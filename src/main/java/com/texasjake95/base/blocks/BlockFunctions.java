package com.texasjake95.base.blocks;

import java.util.List;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;

public class BlockFunctions {
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void getSubBlocks(int par1, CreativeTabs par2CreativeTabs, List par3List, int startingDamage, int maxDamage)
	{
		for (int a = startingDamage; a <= maxDamage; a++)
		{
			par3List.add(new ItemStack(par1, 1, a));
		}
	}
}
