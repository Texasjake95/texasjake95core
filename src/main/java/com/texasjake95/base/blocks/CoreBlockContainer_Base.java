package com.texasjake95.base.blocks;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public abstract class CoreBlockContainer_Base extends BlockContainer {
	
	protected CoreBlockContainer_Base(int par1, Material par3Material)
	{
		super(par1, par3Material);
	}
	
	@Override
	public abstract void registerIcons(IconRegister par1IconRegister);
	
	@Override
	public abstract TileEntity createTileEntity(World world, int metadata);
}
