package com.texasjake95.base.config;

import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

import com.texasjake95.base.helpers.CoreHelper.ItemRegister;
import com.texasjake95.commons.file.config.ConfigFile;
import com.texasjake95.core.CoreConfigHandler;

import cpw.mods.fml.common.Loader;

public abstract class ConfigWriterTemplate extends ConfigWriter {
	
	private boolean autoAssign;
	private Map<String, Integer> blockregister;
	private ConfigFile config;
	private Map<String, Integer> itemregister;
	
	public ConfigWriterTemplate() throws IOException
	{
		CoreConfigHandler.getInstance().addConfig(this);
		this.config = super.CreateConfig(Loader.instance().getConfigDir().getCanonicalPath(), this.modName());
		this.autoAssign = super.autoAssign(this.config, this.modName());
		this.blockregister = new TreeMap<String, Integer>();
		this.itemregister = new TreeMap<String, Integer>();
		this.config.load();
	}
	
	public abstract String modName();
	
	public boolean BooleanConfig(String BooleanName, boolean def)
	{
		return super.BooleanConfig(BooleanName, this.config, def);
	}
	
	public boolean BooleanConfig(String BooleanName, boolean def, int cat, String Category)
	{
		return super.BooleanConfig(BooleanName, this.config, def, cat, Category);
	}
	
	public ConfigFile config()
	{
		return this.config;
	}
	
	public void endProps()
	{
		super.endProps(this.config);
	}
	
	public int getBlockID(String BlockName, int def)
	{
		return super.getBlockID(BlockName, this.config, def, this.autoAssign, this.blockregister);
	}
	
	public int getCreativeTabBlockID(String BlockName)
	{
		// TODO Auto-generated method stub
		return super.getCreativeTabBlockID(BlockName, this.config, this.autoAssign);
	}
	
	public int getCreativeTabItemID(String ItemName)
	{
		// TODO Auto-generated method stub
		return super.getCreativeTabItemID(ItemName, this.config, this.autoAssign);
	}
	
	public int getItemID(String ItemName, int def)
	{
		return super.getItemID(ItemName, this.config, def, this.autoAssign, this.itemregister);
	}
	
	public abstract String modID();
	
	public void registerAll()
	{
		ItemRegister.RegisterAll(this.modID(), this.itemregister, this.blockregister);
	}
}