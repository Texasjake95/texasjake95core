package com.texasjake95.base.config;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraft.world.chunk.storage.AnvilChunkLoader;
import net.minecraft.world.chunk.storage.IChunkLoader;
import net.minecraft.world.storage.ISaveHandler;

public class WorldConfig {
	
	public static WorldConfig wconfig;
	
	public static File getWorldBaseSaveLocation(World world)
	{
		File savedir = getWorldSaveLocation(world);
		if (savedir == null)
		{
			return null;
		}
		else if (savedir.getName().contains("DIM"))
		{
			return savedir.getParentFile();
		}
		else
		{
			return savedir;
		}
	}
	
	@Override
	public boolean equals(Object o)
	{
		if (!(o instanceof WorldConfig))
			return false;
		return this.world.getWorldInfo().getWorldName().equals(((WorldConfig) o).world.getWorldInfo().getWorldName());
	}
	
	public static File getWorldFile(World world)
	{
		return new File(WorldConfig.getWorldBaseSaveLocation(world), "texasjake95/World.dat");
	}
	
	public static File getWorldSaveLocation(World world)
	{
		try
		{
			ISaveHandler worldsaver = world.getSaveHandler();
			IChunkLoader loader = worldsaver.getChunkLoader(world.provider);
			if (loader instanceof AnvilChunkLoader)
			{
				return ((AnvilChunkLoader) loader).chunkSaveLocation;
			}
			return null;
		}
		catch (IllegalAccessError e)
		{
			return ((WorldServer) world).getChunkSaveLocation();
		}
		catch (Exception e)
		{
			return null;
		}
	}
	
	private boolean isDirty;
	public NBTTagCompound saveCompound;
	private File saveFile;
	public World world;
	
	public void removeKey(String keyName)
	{
		if (this.saveCompound.hasKey(keyName))
			this.saveCompound.removeTag(keyName);
	}
	
	public static void initWorldConfig(World world)
	{
		if (wconfig != null)
		{
			if (wconfig.world.getWorldInfo().getWorldName().equals(world.getWorldInfo().getWorldName()))
				return;
		}
		wconfig = new WorldConfig(world);
	}
	
	public WorldConfig(World world)
	{
		this.world = world;
		this.saveFile = getWorldFile(world);
		if (!this.saveFile.getParentFile().exists())
		{
			this.saveFile.getParentFile().mkdirs();
		}
		this.load();
	}
	
	public boolean getBoolean(String keyName)
	{
		if (!this.saveCompound.hasKey(keyName))
		{
			this.setBoolean(keyName, false);
		}
		return this.saveCompound.getBoolean(keyName);
	}
	
	public byte getByte(String keyName)
	{
		if (!this.saveCompound.hasKey(keyName))
		{
			this.setByte(keyName, (byte) 0);
		}
		return this.saveCompound.getByte(keyName);
	}
	
	public double getDouble(String keyName)
	{
		if (!this.saveCompound.hasKey(keyName))
		{
			this.setDouble(keyName, 0);
		}
		return this.saveCompound.getDouble(keyName);
	}
	
	public float getFloat(String keyName)
	{
		if (!this.saveCompound.hasKey(keyName))
		{
			this.setFloat(keyName, 0);
		}
		return this.saveCompound.getFloat(keyName);
	}
	
	public int getInteger(String keyName)
	{
		if (!this.saveCompound.hasKey(keyName))
		{
			this.setInteger(keyName, 0);
		}
		return this.saveCompound.getInteger(keyName);
	}
	
	public long getLong(String keyName)
	{
		if (!this.saveCompound.hasKey(keyName))
		{
			this.setLong(keyName, 0);
		}
		return this.saveCompound.getLong(keyName);
	}
	
	public short getShort(String keyName)
	{
		if (!this.saveCompound.hasKey(keyName))
		{
			this.setShort(keyName, (short) 0);
		}
		return this.saveCompound.getShort(keyName);
	}
	
	public String getString(String keyName)
	{
		if (!this.saveCompound.hasKey(keyName))
		{
			this.setString(keyName, "");
		}
		return this.saveCompound.getString(keyName);
	}
	
	private void load()
	{
		this.saveCompound = new NBTTagCompound();
		try
		{
			if (!this.saveFile.exists())
			{
				this.saveFile.createNewFile();
			}
			if (this.saveFile.length() > 0)
			{
				DataInputStream din = new DataInputStream(new FileInputStream(this.saveFile));
				this.saveCompound = (NBTTagCompound) NBTBase.readNamedTag(din);
				din.close();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			this.load();
		}
	}
	
	public void save()
	{
		if (!this.isDirty)
		{
			return;
		}
		try
		{
			DataOutputStream dout = new DataOutputStream(new FileOutputStream(this.saveFile));
			NBTBase.writeNamedTag(this.saveCompound, dout);
			dout.close();
			this.isDirty = false;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			this.save();
		}
	}
	
	public void setBoolean(String keyName, boolean keyValue)
	{
		this.saveCompound.setBoolean(keyName, keyValue);
		this.setDirty();
	}
	
	public void setByte(String keyName, byte keyValue)
	{
		this.saveCompound.setByte(keyName, keyValue);
		this.setDirty();
	}
	
	public void setDirty()
	{
		this.isDirty = true;
	}
	
	public void setDouble(String keyName, double keyValue)
	{
		this.saveCompound.setDouble(keyName, keyValue);
		this.setDirty();
	}
	
	public void setFloat(String keyName, float keyValue)
	{
		this.saveCompound.setFloat(keyName, keyValue);
		this.setDirty();
	}
	
	public void setInteger(String keyName, int keyValue)
	{
		this.saveCompound.setInteger(keyName, keyValue);
		this.setDirty();
	}
	
	public void setLong(String keyName, long keyValue)
	{
		this.saveCompound.setLong(keyName, keyValue);
		this.setDirty();
	}
	
	public void setShort(String keyName, short keyValue)
	{
		this.saveCompound.setShort(keyName, keyValue);
		this.setDirty();
	}
	
	public void setString(String keyName, String keyValue)
	{
		this.saveCompound.setString(keyName, keyValue);
		this.setDirty();
	}
}
