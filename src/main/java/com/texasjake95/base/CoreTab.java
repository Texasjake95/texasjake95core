package com.texasjake95.base;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import net.minecraft.creativetab.CreativeTabs;

public class CoreTab extends CreativeTabs {
	
	private int ida;
	
	public CoreTab(String label, int id)
	{
		super(label);
		this.ida = id;
	}
	
	@SideOnly(Side.CLIENT)
	@Override
	public int getTabIconItemIndex()
	{
		return this.ida;
	}
}
