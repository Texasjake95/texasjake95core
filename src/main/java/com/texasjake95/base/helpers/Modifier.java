package com.texasjake95.base.helpers;

public class Modifier {
	
	public final int times;
	public final int chance;
	public final int spread;
	
	public Modifier(int times, int chance, int spread)
	{
		this.times = times;
		this.chance = chance;
		this.spread = spread;
	}
}
