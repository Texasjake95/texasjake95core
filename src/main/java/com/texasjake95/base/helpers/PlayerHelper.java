package com.texasjake95.base.helpers;

import net.minecraftforge.common.ForgeDirection;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class PlayerHelper {
	
	public static boolean doesInventoryContainItem(ItemStack[] stack, Item item)
	{
		for (int i = 0; i < stack.length; i++)
		{
			if (stack[i] != null)
			{
				if (stack[i].itemID == item.itemID)
				{
					return true;
				}
			}
		}
		return false;
	}
	
	public static ForgeDirection getPlayerFacing(EntityPlayer player)
	{
		float yaw = player.rotationYaw;
		float pitch = player.rotationPitch;
		System.out.println("Yaw: " + yaw);
		System.out.println("Pitch: " + pitch);
		if (-45F < pitch && pitch < 45F)
		{
			if (-135F <= yaw && yaw < -45F)
				return ForgeDirection.EAST;
			else if (-45F <= yaw && yaw < 45F)
				return ForgeDirection.SOUTH;
			else if (45F <= yaw && yaw < 135F)
				return ForgeDirection.WEST;
			else if (135F <= yaw || yaw < -135F)
				return ForgeDirection.NORTH;
		}
		else if (-45F >= pitch)
			return ForgeDirection.UP;
		else
			return ForgeDirection.DOWN;
		return ForgeDirection.UNKNOWN;
	}
}
