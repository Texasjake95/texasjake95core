package com.texasjake95.base.helpers;

import com.texasjake95.commons.util.SortableCustomList;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class ComItmStk implements Comparable<ComItmStk> {
	
	private int id;
	private int stackSize;
	private int meta;
	
	public ComItmStk(Block block)
	{
		this(block, 0);
	}
	
	public ComItmStk(Block block, int meta)
	{
		this(block, meta, 1);
	}
	
	public ComItmStk(Block block, int meta, int size)
	{
		this(block != null ? block.blockID : -1, meta, size);
	}
	
	public ComItmStk(Item item)
	{
		this(item, 0);
	}
	
	public ComItmStk(Item item, int meta)
	{
		this(item, meta, 1);
	}
	
	public ComItmStk(Item item, int meta, int size)
	{
		this(item != null ? item.itemID : -1, meta, size);
	}
	
	public ComItmStk(ItemStack stack)
	{
		this(stack.itemID, stack.getItemDamage(), stack.stackSize);
	}
	
	public ComItmStk(int id, int meta, int size)
	{
		this.id = id;
		this.meta = meta;
		this.stackSize = size;
	}
	
	public int getID()
	{
		return this.id;
	}
	
	public int getStackSize()
	{
		return this.stackSize;
	}
	
	public int getMeta()
	{
		return this.meta;
	}
	
	public Block getBlock()
	{
		if (this.id == -1)
		{
			return null;
		}
		return Block.blocksList[this.getID()];
	}
	
	public Item getItem()
	{
		if (this.id == -1)
		{
			return null;
		}
		return Item.itemsList[this.getID()];
	}
	
	@Override
	public int compareTo(ComItmStk comItmStk)
	{
		if (id != comItmStk.id)
			return id - comItmStk.id;
		if (meta != comItmStk.meta)
			return meta - comItmStk.meta;
		return stackSize - comItmStk.stackSize;
	}
	
	@Override
	public boolean equals(Object object)
	{
		if (!(object instanceof ComItmStk))
			return false;
		return this.compareTo((ComItmStk) object) == 0;
	}
	
	public static ComItmStk[] toArray(ItemStack[] stacks)
	{
		SortableCustomList<ComItmStk> comStacks = new SortableCustomList<ComItmStk>();
		for (ItemStack stack : stacks)
			comStacks.add(new ComItmStk(stack));
		return comStacks.sort().toArray(new ComItmStk[0]);
	}
	
	public static ComItmStk[] toArray(Block[] blocks)
	{
		SortableCustomList<ComItmStk> comStacks = new SortableCustomList<ComItmStk>();
		for (Block block : blocks)
			comStacks.add(new ComItmStk(block));
		return comStacks.sort().toArray(new ComItmStk[0]);
	}
	
	public static ComItmStk[] toArray(Item[] items)
	{
		SortableCustomList<ComItmStk> comStacks = new SortableCustomList<ComItmStk>();
		for (Item item : items)
			comStacks.add(new ComItmStk(item));
		return comStacks.sort().toArray(new ComItmStk[0]);
	}
	
	public ItemStack getItemStack()
	{
		return new ItemStack(this.id, this.stackSize, this.meta);
	}
}
