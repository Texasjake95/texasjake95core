package com.texasjake95.base.helpers;

import java.util.Random;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.boss.EntityDragon;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.monster.IMob;
import net.minecraft.entity.passive.IAnimals;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

public class MobHelper {
	
	private static final Random random = new Random();
	
	public static EntityItem doMobDrop(ItemStack par1ItemStack, Entity entity)
	{
		EntityItem var3 = new EntityItem(entity.worldObj, entity.posX + Math.random(), entity.posY + Math.random(), entity.posZ + Math.random(), par1ItemStack);
		var3.delayBeforeCanPickup = 10;
		if (entity.captureDrops)
		{
			entity.capturedDrops.add(var3);
		}
		else
		{
			entity.worldObj.spawnEntityInWorld(var3);
		}
		return var3;
	}
	
	public static boolean isAnimal(EntityLiving entity)
	{
		return entity instanceof IAnimals ? !(entity instanceof IMob) : false;
	}
	
	public static boolean isHostile(EntityLiving entity)
	{
		return entity instanceof IMob ? true : entity instanceof EntityDragon ? true : false;
	}
	
	/**
	 * 
	 * @param player
	 * @param stack
	 * @param entity
	 * @param chance
	 *            must be between 0.0d and 1.0d
	 */
	public static void setMobDrop(EntityPlayer player, ItemStack stack, EntityLiving entity, double chance)
	{
		double rand = Math.random();
		if (rand < chance)
		{
			int amount = random.nextInt(stack.stackSize);
			for (int x = 0; x <= amount; x++)
			{
				doMobDrop(new ItemStack(stack.itemID, 1, stack.getItemDamage()), entity);
			}
		}
	}
}
