package com.texasjake95.base.helpers;

import com.texasjake95.core.CoreItemHandler;

import net.minecraft.item.ItemStack;

public class TXItemStack {
	
	public String name;
	public int meta;
	public int stackSize;
	
	public TXItemStack(String name)
	{
		this(name, 0);
	}
	
	public TXItemStack(String name, int meta)
	{
		this(name, meta, 0);
	}
	
	public TXItemStack(String name, int meta, int stackSize)
	{
		this.name = name;
		this.meta = meta;
		this.stackSize = stackSize;
	}
	
	public ItemStack toItemStack()
	{
		if (CoreItemHandler.getInstace().isItemLoaded(this.name))
			return new ItemStack(CoreItemHandler.getInstace().getItem(this.name).itemID, this.stackSize, this.meta);
		else if (CoreItemHandler.getInstace().isBlockLoaded(this.name))
			return new ItemStack(CoreItemHandler.getInstace().getBlock(this.name).blockID, this.stackSize, this.meta);
		return null;
	}
}
