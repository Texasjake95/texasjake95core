package com.texasjake95.base.helpers;

public class Range {
	
	public final int min;
	public final int max;
	
	public Range(int max)
	{
		this(0, max);
	}
	
	public Range(int min, int max)
	{
		this.min = min;
		this.max = max;
	}
}
