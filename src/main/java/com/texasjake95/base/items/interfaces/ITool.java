package com.texasjake95.base.items.interfaces;

public interface ITool {
	
	public String[] getToolClasses();
	
	public int getPower(String toolClass);
}
