package com.texasjake95.base.items;

import com.texasjake95.base.helpers.LoggerHelper;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.Item;

public abstract class CoreItem_Base extends Item {
	
	public LoggerHelper SH;
	
	public CoreItem_Base(int ID)
	{
		super(ID);
	}
	
	@Override
	public abstract void registerIcons(IconRegister par1IconRegister);
}
