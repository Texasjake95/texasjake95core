package com.texasjake95.base.items;

import java.util.List;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;

public abstract class CoreMetaItem_Base extends CoreItem_Base {
	
	public CoreMetaItem_Base(int ID)
	{
		super(ID);
	}
	
	@Override
	public abstract Icon getIconFromDamage(int i);
	
	@Override
	public abstract String getUnlocalizedName(ItemStack itemstack);
	
	@Override
	@SuppressWarnings("rawtypes")
	public abstract void getSubItems(int par1, CreativeTabs par2CreativeTabs, List par3List);
	
	public abstract int setDamageFromMetadata(int i, int metadata);
}
