package com.texasjake95.commons.file;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class FileOutput {
	
	FileWriter fw;
	PrintWriter pw;
	
	public FileOutput(File file) throws IOException
	{
		fw = new FileWriter(file);
		pw = new PrintWriter(fw);
	}
	
	public void println(String string)
	{
		this.pw.println(string);
	}
	
	public void close() throws IOException
	{
		this.pw.close();
		this.fw.close();
	}
}
