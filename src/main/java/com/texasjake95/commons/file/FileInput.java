package com.texasjake95.commons.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class FileInput {
	
	FileReader fr;
	BufferedReader br;
	
	public FileInput(File file)
	{
		try
		{
			fr = new FileReader(file);
			br = new BufferedReader(fr);
		}
		catch (FileNotFoundException e)
		{
		}
	}
	
	public String readLine() throws IOException
	{
		return this.br.readLine();
	}
	
	public ArrayList<String> getFileLines()
	{
		ArrayList<String> lines = new ArrayList<String>();
		String line;
		try
		{
			line = this.readLine();
			while (line != null)
			{
				lines.add(line);
				line = this.readLine();
			}
		}
		catch (IOException e)
		{
		}
		return lines;
	}
	
	public void close() throws IOException
	{
		this.br.close();
		this.fr.close();
	}
}
