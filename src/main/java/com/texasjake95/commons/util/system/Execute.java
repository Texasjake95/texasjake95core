package com.texasjake95.commons.util.system;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Execute {
	
	public static void execute(String command, String... parms)
	{
		String line;
		StringBuilder Exec = new StringBuilder();
		if (getSystemExec() != "")
			Exec.append(String.format("%s %s", getSystemExec(), command));
		else
			Exec.append(command);
		for (String parm : parms)
		{
			Exec.append(" " + parm);
		}
		try
		{
			Process p = Runtime.getRuntime().exec(Exec.toString());
			BufferedReader bri = new BufferedReader(new InputStreamReader(p.getInputStream()));
			BufferedReader bre = new BufferedReader(new InputStreamReader(p.getErrorStream()));
			while ((line = bri.readLine()) != null)
			{
				System.out.println(line);
			}
			bri.close();
			while ((line = bre.readLine()) != null)
			{
				System.out.println(line);
			}
			bre.close();
			p.waitFor();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
	}
	
	public static String getSystemExec()
	{
		String exec = null;
		switch (OS.getOS())
		{
			case Mac:
				exec = "";
				break;
			case Solaris:
				break;
			case Unix:
				break;
			case Windows:
				exec = "cmd /c start";
				break;
			default:
				break;
		}
		return exec;
	}
}
