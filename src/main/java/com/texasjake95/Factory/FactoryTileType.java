package com.texasjake95.Factory;

public enum FactoryTileType
{
	SAW("Saw Blade Table", "Saw_Table.png", 0, TileSawBlade.class);
	
	public static TileFactoryBase makeEntity(int metadata)
	{
		// Compatibility
		int chesttype = validateMeta(metadata);
		if (chesttype == metadata)
		{
			try
			{
				// Test for commit
				TileFactoryBase te = values()[chesttype].clazz.newInstance();
				return te;
			}
			catch (InstantiationException e)
			{
				// unpossible
				e.printStackTrace();
			}
			catch (IllegalAccessException e)
			{
				// unpossible
				e.printStackTrace();
			}
		}
		return null;
	}
	
	public static int validateMeta(int i)
	{
		if (i < values().length)
		{
			return i;
		}
		else
		{
			return 0;
		}
	}
	
	public Class<? extends TileFactoryBase> clazz;
	public String friendlyName;
	private String modelTexture;
	private int textureRow;
	
	FactoryTileType(String friendlyName, String modelTexture, int textureRow, Class<? extends TileFactoryBase> clazz)
	{
		this.friendlyName = friendlyName;
		this.modelTexture = "/texasjake95/Factory/misc/" + modelTexture;
		this.textureRow = textureRow;
		this.clazz = clazz;
	}
	
	public String getModelTexture()
	{
		return this.modelTexture;
	}
	
	public int getTextureRow()
	{
		return this.textureRow;
	}
};
