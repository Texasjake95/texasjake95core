package com.texasjake95.core;

import java.util.HashMap;
import java.util.Map.Entry;

import com.texasjake95.base.helpers.CoreHelper;
import com.texasjake95.base.items.interfaces.ITool;

import cpw.mods.fml.common.registry.GameRegistry;

import net.minecraftforge.oredict.OreDictionary;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class CoreItemHandler implements IItemRegistry {
	
	private static CoreItemHandler instance;
	private final HashMap<String, Item> itemMap = new HashMap<String, Item>();
	private final HashMap<String, Block> blockMap = new HashMap<String, Block>();
	private final HashMap<String, IItemHandler> itemHandlers = new HashMap<String, IItemHandler>();
	private final HashMap<String, IBlockHandler> blockHandlers = new HashMap<String, IBlockHandler>();
	private boolean itemInit = false;
	private boolean blockInit = false;
	
	private CoreItemHandler()
	{
	}
	
	public static CoreItemHandler getInstace()
	{
		if (instance == null)
		{
			instance = new CoreItemHandler();
		}
		return instance;
	}
	
	public void addItemHandler(IItemHandler handler)
	{
		this.itemHandlers.put(handler.itemName(), handler);
	}
	
	public void addBlockHandler(IBlockHandler handler)
	{
		this.blockHandlers.put(handler.blockName(), handler);
	}
	
	public void addItem(String name, Item item)
	{
		this.itemMap.put(name, item);
	}
	
	public void initItems()
	{
		if (itemInit)
			return;
		itemInit = true;
		Item item = null;
		for (Entry<String, IItemHandler> entry : this.itemHandlers.entrySet())
		{
			IItemHandler handler = entry.getValue();
			if (handler.shouldInit())
			{
				item = handler.initItem();
				this.addItem(handler.itemName(), item);
				// GameRegistry.registerItem(item, entry.getKey(),
				// handler.getModName());
				GameRegistry.registerItem(item, entry.getKey());
				if (item instanceof IOreRegister)
					for (int meta : ((IOreRegister) item).getMeta())
						OreDictionary.registerOre(((IOreRegister) item).getOreName(meta), new ItemStack(item.itemID, 1, meta));
				if (item instanceof ITool)
					CoreHelper.RegisterHelper.registerTool(item);
			}
		}
	}
	
	public void initBlocks()
	{
		if (blockInit)
			return;
		blockInit = true;
		Block block = null;
		for (Entry<String, IBlockHandler> entry : this.blockHandlers.entrySet())
		{
			IBlockHandler handler = entry.getValue();
			if (handler.shouldInit())
			{
				block = handler.initBlock();
				this.addBlock(handler.blockName(), block);
				// GameRegistry.registerBlock(block,
				// handler.getItemBlockClass(), entry.getKey(),
				// handler.getModName());
				GameRegistry.registerBlock(block, handler.getItemBlockClass(), entry.getKey());
				if (block instanceof IOreRegister)
					for (int meta : ((IOreRegister) block).getMeta())
						OreDictionary.registerOre(((IOreRegister) block).getOreName(meta), new ItemStack(block.blockID, 1, meta));
			}
		}
	}
	
	public void addBlock(String name, Block block)
	{
		this.blockMap.put(name, block);
	}
	
	public Item getItem(String name)
	{
		return this.itemMap.get(name);
	}
	
	public Block getBlock(String name)
	{
		return this.blockMap.get(name);
	}
	
	public boolean isBlockLoaded(String name)
	{
		return this.blockMap.get(name) != null;
	}
	
	public boolean isItemLoaded(String name)
	{
		return this.itemMap.get(name) != null;
	}
}
