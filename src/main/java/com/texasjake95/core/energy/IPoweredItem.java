package com.texasjake95.core.energy;

import net.minecraft.item.ItemStack;

/**
 * This is used to make an item be powered Internal classes will handle all of
 * the filling and drainings
 * 
 * @author Texasjake95
 * 
 */
public interface IPoweredItem {
	
	/**
	 * @param location
	 * @return the maximum amount of power that can be drained per tick
	 */
	int maxDrain(ItemStack stack, PowerLocation location);
	
	/**
	 * @param islocation
	 * @return the minimum amount of power that can be drained per tick
	 */
	int minDrain(ItemStack stack, PowerLocation location);
	
	/**
	 * @param location
	 * @return the maximum amount of power that can be filled per tick
	 */
	int maxFill(ItemStack stack, PowerLocation location);
	
	/**
	 * @param location
	 * @return the minimum amount of power that can be filled per tick
	 */
	int minFill(ItemStack stack, PowerLocation location);
	
	/**
	 * 
	 * @param stack
	 * @return the amount if power this capacitor can hold
	 */
	int getCapacity(ItemStack stack);
	
	/**
	 * 
	 * @param itemstack
	 * @return if the item can provide power to a machine when placed inside
	 *         (basically can it be drained?)
	 */
	boolean canProvidePower(ItemStack itemstack, PowerLocation location);
	
	/**
	 * 
	 * @param itemStack
	 * @return if the item should use the default tool tip
	 */
	boolean useDefaultToolTip(ItemStack itemStack);
}
