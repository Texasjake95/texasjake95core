package com.texasjake95.core.energy;

import net.minecraftforge.common.ForgeDirection;

/**
 * This interface allows TileEntities to handle power
 * 
 * NOTE: This is much like the fluid system
 * 
 * @author Texasjake95
 * 
 */
public interface IPowerHandler {
	
	/**
	 * Fills power into internal capacitors, distribution is left entirely to
	 * the IPowerHandler.
	 * 
	 * @param from
	 *            Orientation the power is put in to.
	 * @param power
	 *            the amount of power to be inserted
	 * @param doFill
	 *            If false, fill will only be simulated.
	 * @return Amount of power that was (or would have been, if simulated)
	 *         inserted.
	 */
	int gainPower(ForgeDirection from, int power, boolean doFill);
	
	/**
	 * Drains power out of internal capacitors, distribution is left entirely to
	 * the IPowerHandler.
	 * 
	 * 
	 * @param from
	 *            Orientation the power is drained to.
	 * @param maxDrain
	 *            Maximum amount of power to drain.
	 * @param doDrain
	 *            If false, drain will only be simulated.
	 * @return amount of power that was (or would have been, if simulated)
	 *         drained.
	 */
	int drainPower(ForgeDirection from, int power, boolean doDrain);
	
	/**
	 * Returns true if the given power can be inserted into the given direction.
	 * 
	 * More formally, this should return true if power is able to enter from the
	 * given direction.
	 */
	boolean canFillPower(ForgeDirection from);
	
	/**
	 * Returns true if the given power can be extracted from the given
	 * direction.
	 * 
	 * More formally, this should return true if power is able to leave from the
	 * given direction.
	 */
	boolean canDrainPower(ForgeDirection from);
	
	/**
	 * 
	 * @param from
	 *            Orientation in question.
	 * @return if the this can handle power from this side
	 */
	boolean canHandlePower(ForgeDirection from);
	
	PowerCapacitorInfo getCapacitor();
}
