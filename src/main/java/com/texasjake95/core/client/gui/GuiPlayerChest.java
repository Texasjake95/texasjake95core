package com.texasjake95.core.client.gui;

import org.lwjgl.opengl.GL11;

import com.texasjake95.core.inventory.ContainerPlayerChest;
import com.texasjake95.core.tile.TileEntityPlayerChest;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;

@SideOnly(Side.CLIENT)
public class GuiPlayerChest extends GuiContainer {
	
	private static final ResourceLocation field_110421_t = new ResourceLocation("textures/gui/container/generic_54.png");
	/**
	 * window height is calculated with this values, the more rows, the heigher
	 */
	private int inventoryRows;
	private TileEntityPlayerChest chest;
	
	public GuiPlayerChest(InventoryPlayer playerInv, TileEntityPlayerChest chest)
	{
		super(new ContainerPlayerChest(playerInv, chest));
		this.chest = chest;
		this.allowUserInput = false;
		short short1 = 222;
		int i = short1 - 108;
		this.inventoryRows = 5;
		this.ySize = i + this.inventoryRows * 18;
	}
	
	/**
	 * Draw the foreground layer for the GuiContainer (everything in front of
	 * the items)
	 */
	protected void drawGuiContainerForegroundLayer(int par1, int par2)
	{
		this.fontRenderer.drawString(this.chest.isInvNameLocalized() ? this.chest.getInvName() : I18n.getString(this.chest.getInvName()), 8, this.ySize - 96 + 2, 4210752);
	}
	
	/**
	 * Draw the background layer for the GuiContainer (everything behind the
	 * items)
	 */
	protected void drawGuiContainerBackgroundLayer(float par1, int par2, int par3)
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.mc.getTextureManager().bindTexture(field_110421_t);
		int k = (this.width - this.xSize) / 2;
		int l = (this.height - this.ySize) / 2;
		this.drawTexturedModalRect(k, l, 0, 0, this.xSize, this.inventoryRows * 18 + 17);
		this.drawTexturedModalRect(k, l + this.inventoryRows * 18 + 17, 0, 126, this.xSize, 96);
	}
}
