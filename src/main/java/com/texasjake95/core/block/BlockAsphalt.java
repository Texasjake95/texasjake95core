package com.texasjake95.core.block;

import java.util.List;

import com.texasjake95.base.blocks.CoreBlock_Base;

import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.Icon;
import net.minecraft.world.World;

public class BlockAsphalt extends CoreBlock_Base {
	
	public static final String[] blockNames = { "blockAsphalt", "blockAsphalt1", "blockAsphalt2" };
	private Icon[] iconAsphalt;
	
	public BlockAsphalt(int par1, Material par3Material)
	{
		super(par1, Material.ground);
		this.setStepSound(soundStoneFootstep);
		this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.15F, 1.0F);
		setLightOpacity(255);
	}
	
	@Override
	public void onEntityCollidedWithBlock(World world, int i, int j, int k, Entity entity)
	{
		if (world.isRemote)
		{
			return;
		}
		else
		{
			speedPlayerUp(entity);
			return;
		}
	}
	
	@Override
	public boolean renderAsNormalBlock()
	{
		return false;
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public void addCollisionBoxesToList(World par1World, int par2, int par3, int par4, AxisAlignedBB par5AxisAlignedBB, List par6ArrayList, Entity entity)
	{
		setBlockBoundsBasedOnState(par1World, par2, par3, par4);
		super.addCollisionBoxesToList(par1World, par2, par3, par4, par5AxisAlignedBB, par6ArrayList, entity);
	}
	
	private void speedPlayerUp(Entity entity)
	{
		entity.motionX *= 1.3;
		entity.motionZ *= 1.3;
	}
	
	@Override
	public void registerIcons(IconRegister par1IconRegister)
	{
		this.iconAsphalt = new Icon[blockNames.length];
		for (int i = 0; i < blockNames.length; i++)
			this.iconAsphalt[i] = par1IconRegister.registerIcon(blockNames[i]);
	}
}
