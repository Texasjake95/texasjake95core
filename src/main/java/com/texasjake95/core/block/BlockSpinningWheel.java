package com.texasjake95.core.block;

import com.texasjake95.core.Texasjake95Core;
import com.texasjake95.core.tile.TileEntityGrinder;
import com.texasjake95.core.tile.TileEntitySpinningWheel;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class BlockSpinningWheel extends BlockContainer {
	
	public BlockSpinningWheel(int par1)
	{
		super(par1, Material.wood);
	}
	
	@Override
	public TileEntity createNewTileEntity(World world)
	{
		return null;
	}
	
	@Override
	public TileEntity createTileEntity(World world, int meta)
	{
		switch (meta)
		{
			case 1:
				return new TileEntityGrinder();
			default:
				return new TileEntitySpinningWheel();
		}
	}
	
	@Override
	public int damageDropped(int par1)
	{
		return par1;
	}
	
	public boolean onBlockActivated(World par1World, int par2, int par3, int par4, EntityPlayer par5EntityPlayer, int par6, float par7, float par8, float par9)
	{
		int meta = par1World.getBlockMetadata(par2, par3, par4);
		if (par1World.isRemote)
		{
		}
		else
		{
			switch (meta)
			{
				case 1:
					this.GrinderGUI(par1World, par2, par3, par4, par5EntityPlayer);
					break;
				default:
					this.SpinningWheelGUI(par1World, par2, par3, par4, par5EntityPlayer);
					break;
			}
		}
		return true;
	}
	
	private void GrinderGUI(World world, int x, int y, int z, EntityPlayer player)
	{
		TileEntityGrinder wheel = (TileEntityGrinder) world.getBlockTileEntity(x, y, z);
		if (wheel != null)
		{
			player.openGui(Texasjake95Core.instance, 8, world, x, y, z);
		}
	}
	
	private void SpinningWheelGUI(World world, int x, int y, int z, EntityPlayer player)
	{
		TileEntitySpinningWheel wheel = (TileEntitySpinningWheel) world.getBlockTileEntity(x, y, z);
		if (wheel != null)
		{
			player.openGui(Texasjake95Core.instance, 7, world, x, y, z);
		}
	}
}
