package com.texasjake95.core.block;

import com.texasjake95.core.tile.TileEntityFluidGen;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class BlockFluidGen extends BlockContainer {
	
	public BlockFluidGen(int par1)
	{
		super(par1, Material.iron);
	}
	
	@Override
	public TileEntity createNewTileEntity(World world)
	{
		return new TileEntityFluidGen();
	}
	
	@Override
	public boolean onBlockActivated(World par1World, int par2, int par3, int par4, EntityPlayer par5EntityPlayer, int par6, float par7, float par8, float par9)
	{
		if (!par1World.isRemote)
		{
			TileEntity tile = par1World.getBlockTileEntity(par2, par3, par4);
			if ((tile instanceof TileEntityFluidGen))
			{
				TileEntityFluidGen fluid = (TileEntityFluidGen) tile;
				boolean sneak = par5EntityPlayer.isSneaking();
				ItemStack heldItem = par5EntityPlayer.getHeldItem();
				if (heldItem == null & sneak)
				{
					if (fluid.inv[0] == null)
					{
					}
					else
					{
						ItemStack stack = fluid.inv[0];
						fluid.inv[0] = null;
						EntityItem item = new EntityItem(par1World, par2 + 1, par3, par4, stack);
						par1World.spawnEntityInWorld(item);
					}
				}
				else if (fluid.isItemValidForSlot(0, heldItem))
				{
					if (fluid.inv[0] == null)
					{
						fluid.inv[0] = heldItem;
						par5EntityPlayer.inventory.mainInventory[par5EntityPlayer.inventory.currentItem] = null;
					}
					else
					{
					}
				}
			}
		}
		return true;
	}
}
