package com.texasjake95.core.block;

import com.texasjake95.core.Texasjake95Core;
import com.texasjake95.core.tile.TileEntityPlayerChest;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class BlockPlayerChest extends BlockContainer {
	
	public BlockPlayerChest(int par1, Material par2Material)
	{
		super(par1, par2Material);
	}
	
	@Override
	public TileEntity createNewTileEntity(World world)
	{
		return null;
	}
	
	@Override
	public TileEntity createTileEntity(World world, int meta)
	{
		return new TileEntityPlayerChest();
	}
	
	public void onBlockPlacedBy(World par1World, int par2, int par3, int par4, EntityLivingBase par5EntityLivingBase, ItemStack par6ItemStack)
	{
		if (par5EntityLivingBase instanceof EntityPlayer)
		{
			EntityPlayer player = (EntityPlayer) par5EntityLivingBase;
			TileEntity tile = par1World.getBlockTileEntity(par2, par3, par4);
			if (tile instanceof TileEntityPlayerChest)
			{
				TileEntityPlayerChest chest = (TileEntityPlayerChest) tile;
				chest.setOwnerName(player.username);
			}
		}
	}
	
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer par5EntityPlayer, int par6, float par7, float par8, float par9)
	{
		TileEntity tile = world.getBlockTileEntity(x, y, z);
		if (world.isRemote || !(tile instanceof TileEntityPlayerChest))
		{
		}
		else
		{
			TileEntityPlayerChest chest = (TileEntityPlayerChest) tile;
			if (chest.getPlayer() != null && chest.isUseableByPlayer(par5EntityPlayer))
			{
				par5EntityPlayer.openGui(Texasjake95Core.instance, 9, world, x, y, z);
			}
		}
		return true;
	}
}
