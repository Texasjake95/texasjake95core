package com.texasjake95.core;

import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import net.minecraft.entity.Entity;
import net.minecraft.entity.effect.EntityWeatherEffect;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;

public class EntityCustomLightningBolt extends EntityWeatherEffect {
	
	private EntityPlayer player;
	private float damage;
	private int lightningState;
	public long boltVertex;
	private int boltLivingTime;
	
	public EntityCustomLightningBolt(EntityPlayer player, World par1World, double par2, double par4, double par6, float damage)
	{
		super(par1World);
		this.damage = damage;
		this.player = player;
		this.setLocationAndAngles(par2, par4, par6, 0.0F, 0.0F);
		this.lightningState = 1;
		this.boltVertex = this.rand.nextLong();
		this.boltLivingTime = 1;
	}
	
	/**
	 * Called to update the entity's position/logic.
	 */
	public void onUpdate()
	{
		super.onUpdate();
		if (this.lightningState == 1)
		{
			this.worldObj.playSoundEffect(this.posX, this.posY, this.posZ, "ambient.weather.thunder", 10000.0F, 0.8F + this.rand.nextFloat() * 0.2F);
			this.worldObj.playSoundEffect(this.posX, this.posY, this.posZ, "random.explode", 2.0F, 0.5F + this.rand.nextFloat() * 0.2F);
		}
		--this.lightningState;
		if (this.lightningState < 0)
		{
			if (this.boltLivingTime == 0)
			{
				this.setDead();
			}
			else if (this.lightningState < -this.rand.nextInt(10))
			{
				--this.boltLivingTime;
				this.lightningState = 1;
				this.boltVertex = this.rand.nextLong();
			}
		}
		if (this.lightningState >= 0)
		{
			if (this.worldObj.isRemote)
			{
				this.worldObj.lastLightningBolt = 2;
			}
			else
			{
				double d0 = .5D;
				@SuppressWarnings("unchecked")
				List<Entity> list = this.worldObj.getEntitiesWithinAABBExcludingEntity(this, AxisAlignedBB.getAABBPool().getAABB(this.posX - d0, this.posY - d0, this.posZ - d0, this.posX + d0, this.posY + d0, this.posZ + d0));
				for (int l = 0; l < list.size(); ++l)
				{
					Entity entity = (Entity) list.get(l);
					if (entity instanceof EntityPlayer && ((EntityPlayer) entity).username.equals(this.player.username))
					{
					}
					else
						entity.attackEntityFrom(Texasjake95Core.lightning, this.damage);
				}
			}
		}
	}
	
	protected void entityInit()
	{
	}
	
	/**
	 * (abstract) Protected helper method to read subclass entity data from NBT.
	 */
	protected void readEntityFromNBT(NBTTagCompound par1NBTTagCompound)
	{
	}
	
	/**
	 * (abstract) Protected helper method to write subclass entity data to NBT.
	 */
	protected void writeEntityToNBT(NBTTagCompound par1NBTTagCompound)
	{
	}
	
	@SideOnly(Side.CLIENT)
	/**
	 * Checks using a Vec3d to determine if this entity is within range of that vector to be rendered. Args: vec3D
	 */
	public boolean isInRangeToRenderVec3D(Vec3 par1Vec3)
	{
		return this.lightningState >= 0;
	}
}
