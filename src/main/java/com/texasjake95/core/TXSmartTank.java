package com.texasjake95.core;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;

import net.minecraft.nbt.NBTTagCompound;

public class TXSmartTank extends TXTank {
	
	TankState state = TankState.NONE;
	
	public TXSmartTank(int capacity)
	{
		super(capacity);
	}
	
	public TXSmartTank(FluidStack stack, int capacity)
	{
		super(stack, capacity);
	}
	
	public TXSmartTank(Fluid fluid, int amount, int capacity)
	{
		super(fluid, amount, capacity);
	}
	
	public void writeToPacket(DataOutputStream dos)
	{
		super.writeToPacket(dos);
		try
		{
			dos.writeByte((byte) this.state.ordinal());
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public void readFromPacket(DataInputStream dis)
	{
		super.readFromPacket(dis);
		try
		{
			this.state = TankState.getState(dis.readByte());
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public FluidTank readFromNBT(NBTTagCompound nbt)
	{
		super.readFromNBT(nbt);
		state = TankState.getState(nbt.getByte("tankState"));
		return this;
	}
	
	public NBTTagCompound writeToNBT(NBTTagCompound nbt)
	{
		super.writeToNBT(nbt);
		nbt.setByte("tankState", (byte) state.ordinal());
		return nbt;
	}
	
	public TankState getState()
	{
		return this.state;
	}
	
	public void setState(TankState state)
	{
		this.state = state;
	}
	
	public enum TankState
	{
		NONE, INPUT, OUTPUT;
		
		public static TankState getState(int id)
		{
			if (id > values().length)
				id = values().length - 1;
			return values()[id];
		}
	}
}
