package com.texasjake95.core;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

public class RenderEntityBlizzard extends Render {
	
	private static final ResourceLocation entityBlizzardTexture = new ResourceLocation("textures/entity/experience_orb.png");
	
	@Override
	public void doRender(Entity entity, double d0, double d1, double d2, float f, float f1)
	{
		this.renderBlizzard((EntityBlizzard) entity, d0, d1, d2, f, f1);
	}
	
	private void renderBlizzard(EntityBlizzard entityBlizzard, double d0, double d1, double d2, float f, float f1)
	{
		GL11.glPushMatrix();
		GL11.glTranslatef((float) d0, (float) d1, (float) d2);
		this.bindEntityTexture(entityBlizzard);
		float f2 = (float) (3 % 4 * 16 + 0) / 64.0F;
		float f3 = (float) (3 % 4 * 16 + 16) / 64.0F;
		float f4 = (float) (3 / 4 * 16 + 0) / 64.0F;
		float f5 = (float) (3 / 4 * 16 + 16) / 64.0F;
		float f6 = 1.0F;
		float f7 = 0.5F;
		float f8 = 0.25F;
		int j = entityBlizzard.getBrightnessForRender(f1);
		int k = j % 65536;
		int l = j / 65536;
		OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, (float) k / 1.0F, (float) l / 1.0F);
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		GL11.glRotatef(180.0F - this.renderManager.playerViewY, 0.0F, 1.0F, 0.0F);
		GL11.glRotatef(-this.renderManager.playerViewX, 1.0F, 0.0F, 0.0F);
		float f11 = 0.3F;
		GL11.glScalef(f11, f11, f11);
		Tessellator tessellator = Tessellator.instance;
		tessellator.startDrawingQuads();
		tessellator.setColorRGBA(300, 300, 300, 3030);
		tessellator.setNormal(0.0F, 1.0F, 0.0F);
		tessellator.addVertexWithUV((double) (0.0F - f7), (double) (0.0F - f8), 0.0D, (double) f2, (double) f5);
		tessellator.addVertexWithUV((double) (f6 - f7), (double) (0.0F - f8), 0.0D, (double) f3, (double) f5);
		tessellator.addVertexWithUV((double) (f6 - f7), (double) (1.0F - f8), 0.0D, (double) f3, (double) f4);
		tessellator.addVertexWithUV((double) (0.0F - f7), (double) (1.0F - f8), 0.0D, (double) f2, (double) f4);
		tessellator.draw();
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glDisable(GL12.GL_RESCALE_NORMAL);
		GL11.glPopMatrix();
	}
	
	protected ResourceLocation getBlizzardTexture(EntityBlizzard entityBlizzard)
	{
		return entityBlizzardTexture;
	}
	
	@Override
	protected ResourceLocation getEntityTexture(Entity entity)
	{
		return this.getBlizzardTexture((EntityBlizzard) entity);
	}
}
