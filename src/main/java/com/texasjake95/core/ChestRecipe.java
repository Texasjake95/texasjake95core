package com.texasjake95.core;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.texasjake95.core.Items.BagInventory;
import com.texasjake95.core.Items.BagInventoryLarge;
import com.texasjake95.core.Items.PortableCraftingTable;

import net.minecraft.block.Block;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.world.World;

public class ChestRecipe implements IRecipe {
	
	/** Is the ItemStack that you get when craft the recipe. */
	private final ItemStack recipeOutput;
	/** Is a List of ItemStack that composes the recipe. */
	public final List<ItemStack> recipeItems;
	
	public ChestRecipe(ItemStack par1ItemStack, List<ItemStack> par2List)
	{
		this.recipeOutput = par1ItemStack;
		this.recipeItems = par2List;
	}
	
	public ItemStack getRecipeOutput()
	{
		return this.recipeOutput;
	}
	
	/**
	 * Used to check if a recipe matches current crafting inventory
	 */
	public boolean matches(InventoryCrafting par1InventoryCrafting, World par2World)
	{
		ArrayList<ItemStack> arraylist = new ArrayList<ItemStack>(this.recipeItems);
		for (int i = 0; i < 3; ++i)
		{
			for (int j = 0; j < 3; ++j)
			{
				ItemStack itemstack = par1InventoryCrafting.getStackInRowAndColumn(j, i);
				if (itemstack != null)
				{
					boolean flag = false;
					Iterator<ItemStack> iterator = arraylist.iterator();
					while (iterator.hasNext())
					{
						ItemStack itemstack1 = iterator.next();
						if (itemstack.itemID == itemstack1.itemID && (itemstack1.getItemDamage() == 32767 || itemstack.getItemDamage() == itemstack1.getItemDamage()))
						{
							flag = true;
							arraylist.remove(itemstack1);
							break;
						}
					}
					if (!flag)
					{
						return false;
					}
				}
			}
		}
		return arraylist.isEmpty();
	}
	
	/**
	 * Returns an Item that is the result of this recipe
	 */
	public ItemStack getCraftingResult(InventoryCrafting par1InventoryCrafting)
	{
		BagInventory bag1 = null;
		BagInventory bag2 = null;
		for (int i = 0; i < 3; ++i)
		{
			for (int j = 0; j < 3; ++j)
			{
				ItemStack itemstack = par1InventoryCrafting.getStackInRowAndColumn(j, i);
				if (itemstack.getItem() instanceof PortableCraftingTable && itemstack.getItemDamage() == 1)
					if (itemstack != null)
					{
						if (bag1 == null)
						{
							bag1 = new BagInventory(itemstack);
						}
						else
						{
							bag2 = new BagInventory(itemstack);
							break;
						}
					}
			}
		}
		return BagInventoryLarge.combineNBTs(new BagInventoryLarge(this.recipeOutput.copy()), bag1, bag2);
	}
	
	/**
	 * Returns the size of the recipe area
	 */
	public int getRecipeSize()
	{
		return this.recipeItems.size();
	}
	
	@SuppressWarnings("unchecked")
	public static void addChestRecipe(ItemStack output, Object... recipe)
	{
		ArrayList<ItemStack> inputFinal = new ArrayList<ItemStack>();
		Object[] inputs = recipe;
		int inputLength = recipe.length;
		for (int i = 0; i < inputLength; ++i)
		{
			Object input = inputs[i];
			if (input instanceof ItemStack)
			{
				inputFinal.add(((ItemStack) input).copy());
			}
			else if (input instanceof Item)
			{
				inputFinal.add(new ItemStack((Item) input));
			}
			else
			{
				if (!(input instanceof Block))
				{
					throw new RuntimeException("Invalid shapeless damage recipe!");
				}
				inputFinal.add(new ItemStack((Block) input));
			}
		}
		CraftingManager.getInstance().getRecipeList().add(new ChestRecipe(output, inputFinal));
		// FMLInterModComms.sendMessage(Reference.MOD_ID,
		// InterModComms.ADD_RECIPE, NBTHelper.encodeRecipeAsNBT(output,
		// Arrays.asList(recipe)));
	}
}
