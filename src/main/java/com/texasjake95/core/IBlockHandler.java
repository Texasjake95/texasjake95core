package com.texasjake95.core;

import net.minecraft.block.Block;
import net.minecraft.item.ItemBlock;

public interface IBlockHandler {
	
	public String blockName();
	
	public boolean shouldInit();
	
	public Block initBlock();
	
	public Class<? extends ItemBlock> getItemBlockClass();
	
	public IBlockHandler setModName(String modName);
	
	public String getModName();
	
	/**
	 * Not in use yet
	 */
	@Deprecated
	public int getPriority();
	
	/**
	 * Not in use yet
	 */
	@Deprecated
	public IBlockHandler setPriority(int priority);
}
