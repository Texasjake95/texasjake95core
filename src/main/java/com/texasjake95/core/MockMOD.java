package com.texasjake95.core;

import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;

// @Mod(modid = "MockMod", name = "MockMod", version = "1.0.0")
public class MockMOD {
	
	@Instance("TXMockMod")
	public static MockMOD instance;
	
	@EventHandler
	public void load(FMLInitializationEvent event)
	{
	}
	
	@EventHandler
	public void modsLoaded(FMLPostInitializationEvent event)
	{
	}
	
	@EventHandler
	public void preload(FMLPreInitializationEvent event)
	{
	}
}
