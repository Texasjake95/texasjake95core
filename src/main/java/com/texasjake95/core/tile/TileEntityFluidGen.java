package com.texasjake95.core.tile;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import buildcraft.api.power.IPowerEmitter;
import buildcraft.api.power.IPowerReceptor;
import buildcraft.api.power.PowerHandler;
import buildcraft.api.power.PowerHandler.PowerReceiver;
import buildcraft.api.power.PowerHandler.Type;
import buildcraft.core.TileBuffer;
import buildcraft.core.proxy.CoreProxy;

import com.texasjake95.core.energy.IPowerHandler;
import com.texasjake95.core.energy.PowerCapacitorInfo;
import com.texasjake95.core.network.packet.PacketTileEntityUpdate;
import com.texasjake95.core.network.packet.PacketTypeHandler;

import net.minecraftforge.common.ForgeDirection;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidContainerRegistry;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fluids.FluidTankInfo;
import net.minecraftforge.fluids.IFluidHandler;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.packet.Packet;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class TileEntityFluidGen extends TileEntityCore implements IInventory, IFluidHandler, IPowerHandler, IPowerEmitter, IPowerReceptor {
	
	private FluidTank tank = new FluidTank(10000);
	private PowerStorage power = new PowerStorage(10000);
	private PowerHandler buildCraft = new PowerHandler(this, Type.ENGINE);
	public ItemStack[] inv = new ItemStack[1];
	private IFluidHandler[] tanks = new IFluidHandler[6];
	private IPowerHandler[] powers = new IPowerHandler[6];
	public float energy;
	private TileBuffer[] tileCache;
	
	public TileEntityFluidGen()
	{
		super();
	}
	
	@Override
	public void updateEntity()
	{
		if (!this.worldObj.isRemote)
		{
			this.addEnergy(10000);
			this.sendPower();
			for (ForgeDirection direction : ForgeDirection.values())
			{
				if (direction != ForgeDirection.UNKNOWN)
				{
					TileEntity tile = this.worldObj.getBlockTileEntity(this.xCoord + direction.offsetX, this.yCoord + direction.offsetY, this.zCoord + direction.offsetZ);
					if (tile instanceof IFluidHandler)
					{
						this.tanks[direction.ordinal()] = (IFluidHandler) tile;
					}
					else if (tile instanceof IPowerHandler)
					{
						this.powers[direction.ordinal()] = (IPowerHandler) tile;
					}
				}
			}
			if (this.inv[0] != null)
				if (this.tank.getFluid() == null || FluidContainerRegistry.getFluidForFilledItem(this.inv[0]).getFluid() == this.tank.getFluid().getFluid())
				{
					if (this.tank.getCapacity() != this.tank.getFluidAmount())
					{
						this.tank.setFluid(new FluidStack(FluidContainerRegistry.getFluidForFilledItem(this.inv[0]).getFluid(), this.tank.getCapacity()));
					}
				}
				else
				{
					this.tank.setFluid(new FluidStack(FluidContainerRegistry.getFluidForFilledItem(this.inv[0]).getFluid(), this.tank.getCapacity()));
				}
			if (this.power.getPower() < this.power.getCapacity())
			{
				System.out.println("Filling power");
				this.power.fill(this.power.getCapacity(), true);
			}
			for (int i = 0; i < tanks.length; i++)
			{
				IFluidHandler tileTank = tanks[i];
				IPowerHandler powerTank = powers[i];
				if (this.tank.getFluid() != null)
					if (tileTank != null && tileTank.canFill(ForgeDirection.getOrientation(i).getOpposite(), this.tank.getFluid().getFluid()))
					{
						tileTank.fill(ForgeDirection.getOrientation(i).getOpposite(), this.tank.getFluid(), true);
					}
				if (powerTank != null && powerTank.canFillPower(ForgeDirection.getOrientation(i).getOpposite()))
				{
					powerTank.gainPower(ForgeDirection.getOrientation(i).getOpposite(), this.power.getCapacity(), true);
				}
			}
		}
	}
	
	@Override
	public Packet getDescriptionPacket()
	{
		return PacketTypeHandler.populatePacket(new PacketTileEntityUpdate(this, "Texasjake95Core"));
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbtTagCompound)
	{
		super.readFromNBT(nbtTagCompound);
		this.tank.readFromNBT(nbtTagCompound);
		NBTTagList nbttaglist = nbtTagCompound.getTagList("Items");
		for (int i = 0; i < nbttaglist.tagCount(); ++i)
		{
			NBTTagCompound nbttagcompound1 = (NBTTagCompound) nbttaglist.tagAt(i);
			byte b0 = nbttagcompound1.getByte("Slot");
			if (b0 >= 0 && b0 < this.inv.length)
			{
				this.inv[b0] = ItemStack.loadItemStackFromNBT(nbttagcompound1);
			}
		}
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbtTagCompound)
	{
		super.writeToNBT(nbtTagCompound);
		this.tank.writeToNBT(nbtTagCompound);
		NBTTagList nbttaglist = new NBTTagList();
		for (int i = 0; i < this.inv.length; ++i)
		{
			if (this.inv[i] != null)
			{
				NBTTagCompound nbttagcompound1 = new NBTTagCompound();
				nbttagcompound1.setByte("Slot", (byte) i);
				this.inv[i].writeToNBT(nbttagcompound1);
				nbttaglist.appendTag(nbttagcompound1);
			}
		}
		nbtTagCompound.setTag("Items", nbttaglist);
	}
	
	@Override
	public void writeToPacket(DataOutputStream dos, PacketTypeHandler packetType) throws IOException
	{
		super.writeToPacket(dos, packetType);
		for (int i = 0; i < 1; i++)
		{
			ItemStack stack = this.inv[i];
			dos.writeByte(i);
			if (stack == null)
			{
				dos.writeBoolean(false);
			}
			else
			{
				dos.writeBoolean(true);
				dos.writeInt(stack.itemID);
				dos.writeInt(stack.stackSize);
				dos.writeInt(stack.getItemDamage());
			}
		}
	}
	
	@Override
	public void readFromPacket(DataInputStream dis, PacketTypeHandler packetType) throws IOException
	{
		super.readFromPacket(dis, packetType);
		int slot;
		for (int i = 0; i < 1; i++)
		{
			slot = dis.readByte();
			if (dis.readBoolean())
			{
				this.inv[slot] = new ItemStack(dis.readInt(), dis.readInt(), dis.readInt());
			}
			else
			{
				this.inv[slot] = null;
			}
		}
	}
	
	@Override
	public int getSizeInventory()
	{
		return 1;
	}
	
	@Override
	public ItemStack getStackInSlot(int i)
	{
		return this.inv[i];
	}
	
	@Override
	public ItemStack decrStackSize(int i, int j)
	{
		return this.inv[i].splitStack(j);
	}
	
	@Override
	public ItemStack getStackInSlotOnClosing(int i)
	{
		return this.inv[i];
	}
	
	@Override
	public void setInventorySlotContents(int i, ItemStack itemstack)
	{
		this.inv[i] = itemstack;
	}
	
	@Override
	public String getInvName()
	{
		return "Creavtive Fluid Generation";
	}
	
	@Override
	public boolean isInvNameLocalized()
	{
		return false;
	}
	
	@Override
	public int getInventoryStackLimit()
	{
		return 1;
	}
	
	@Override
	public boolean isUseableByPlayer(EntityPlayer entityplayer)
	{
		return true;
	}
	
	@Override
	public void openChest()
	{
	}
	
	@Override
	public void closeChest()
	{
	}
	
	@Override
	public boolean isItemValidForSlot(int i, ItemStack itemstack)
	{
		return FluidContainerRegistry.isContainer(itemstack);
	}
	
	@Override
	public int fill(ForgeDirection from, FluidStack resource, boolean doFill)
	{
		return this.tank.fill(resource, doFill);
	}
	
	@Override
	public FluidStack drain(ForgeDirection from, int maxDrain, boolean doDrain)
	{
		return this.tank.drain(maxDrain, doDrain);
	}
	
	@Override
	public FluidStack drain(ForgeDirection from, FluidStack resource, boolean doDrain)
	{
		return this.tank.drain(resource.amount, doDrain);
	}
	
	@Override
	public boolean canFill(ForgeDirection from, Fluid fluid)
	{
		return false;
	}
	
	@Override
	public boolean canDrain(ForgeDirection from, Fluid fluid)
	{
		return true;
	}
	
	@Override
	public FluidTankInfo[] getTankInfo(ForgeDirection from)
	{
		return new FluidTankInfo[] { this.tank.getInfo() };
	}
	
	@Override
	public int gainPower(ForgeDirection from, int power, boolean doFill)
	{
		return 0;
	}
	
	@Override
	public int drainPower(ForgeDirection from, int power, boolean doDrain)
	{
		return 0;
	}
	
	@Override
	public boolean canFillPower(ForgeDirection from)
	{
		return false;
	}
	
	@Override
	public boolean canDrainPower(ForgeDirection from)
	{
		return false;
	}
	
	@Override
	public boolean canHandlePower(ForgeDirection from)
	{
		return false;
	}
	
	@Override
	public PowerCapacitorInfo getCapacitor()
	{
		return this.power.getInfo();
	}
	
	@Override
	public boolean canEmitPowerFrom(ForgeDirection side)
	{
		return true;
	}
	
	@Override
	public PowerReceiver getPowerReceiver(ForgeDirection side)
	{
		return this.buildCraft.getPowerReceiver();
	}
	
	private void sendPower()
	{
		for (ForgeDirection d : ForgeDirection.values())
		{
			if (d != ForgeDirection.UNKNOWN)
			{
				TileEntity tile = getTileBuffer(d).getTile();
				if (tile instanceof IPowerReceptor)
				{
					PowerReceiver receptor = ((IPowerReceptor) tile).getPowerReceiver(d.getOpposite());
					float extracted = 10000;
					if (extracted > 0)
					{
						if (receptor != null)
						{
							float needed = receptor.receiveEnergy(PowerHandler.Type.ENGINE, extracted, d.getOpposite());
							extractEnergy(receptor.getMinEnergyReceived(), needed, true); // Comment
																							// out
																							// for
																							// constant
																							// power
							// currentOutput = extractEnergy(0, needed, true);
							// //
							// Uncomment for constant power
						}
					}
				}
			}
		}
	}
	
	public TileBuffer getTileBuffer(ForgeDirection side)
	{
		if (tileCache == null)
			tileCache = TileBuffer.makeBuffer(worldObj, xCoord, yCoord, zCoord, false);
		return tileCache[side.ordinal()];
	}
	
	public float extractEnergy(float min, float max, boolean doExtract)
	{
		if (energy < min)
			return 0;
		float actualMax;
		if (max > 10000)
			actualMax = 10000;
		else
			actualMax = max;
		if (actualMax < min)
			return 0;
		float extracted;
		if (energy >= actualMax)
		{
			extracted = actualMax;
			if (doExtract)
				energy -= actualMax;
		}
		else
		{
			extracted = energy;
			if (doExtract)
				energy = 0;
		}
		return extracted;
	}
	
	@Override
	public void doWork(PowerHandler workProvider)
	{
		if (CoreProxy.proxy.isRenderWorld(worldObj))
			return;
		addEnergy(10000);
	}
	
	public void addEnergy(float addition)
	{
		energy += addition;
		if (energy > 10000)
			energy = 10000;
	}
	
	@Override
	public World getWorld()
	{
		return this.worldObj;
	}
}
