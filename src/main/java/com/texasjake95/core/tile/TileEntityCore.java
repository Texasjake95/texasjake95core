package com.texasjake95.core.tile;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.texasjake95.core.network.packet.PacketTypeHandler;

import net.minecraftforge.common.ForgeDirection;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;

public class TileEntityCore extends TileEntity {
	
	protected ForgeDirection facing;
	protected String customName;
	protected String owner;
	
	public TileEntityCore()
	{
		facing = ForgeDirection.SOUTH;
		customName = "";
		owner = "";
	}
	
	public void setFacing(ForgeDirection direction)
	{
		if (direction != ForgeDirection.UNKNOWN)
		{
			facing = direction;
		}
	}
	
	public ForgeDirection getFacing()
	{
		return this.facing;
	}
	
	public void setCustomName(String name)
	{
		this.customName = name;
	}
	
	public String getCustomName()
	{
		return this.customName;
	}
	
	public void setOwnerName(String name)
	{
		this.owner = name;
	}
	
	public String getOwnerName()
	{
		return this.owner;
	}
	
	public boolean hasCustomName()
	{
		return customName != null && customName.length() > 0;
	}
	
	public int getBlockID()
	{
		return this.worldObj.getBlockId(this.xCoord, this.yCoord, this.zCoord);
	}
	
	public int getBlockMeta()
	{
		return this.worldObj.getBlockMetadata(this.xCoord, this.yCoord, this.zCoord);
	}
	
	public boolean isSameBlock(int id)
	{
		return this.isSameBlock(id, 0);
	}
	
	public boolean isSameBlock(int id, int meta)
	{
		return this.getBlockID() == id && this.getBlockMeta() == meta;
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbtTagCompound)
	{
		super.readFromNBT(nbtTagCompound);
		if (nbtTagCompound.hasKey("direction"))
		{
			facing = ForgeDirection.getOrientation(nbtTagCompound.getByte("direction"));
		}
		if (nbtTagCompound.hasKey("customName"))
		{
			customName = nbtTagCompound.getString("customName");
		}
		if (nbtTagCompound.hasKey("owner"))
		{
			owner = nbtTagCompound.getString("owner");
		}
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbtTagCompound)
	{
		super.writeToNBT(nbtTagCompound);
		nbtTagCompound.setByte("direction", (byte) facing.ordinal());
		nbtTagCompound.setString("owner", owner);
		if (this.hasCustomName())
		{
			nbtTagCompound.setString("customName", customName);
		}
	}
	
	public void writeToPacket(DataOutputStream dos, PacketTypeHandler packetType) throws IOException
	{
		if (dos != null)
		{
			dos.writeInt(this.xCoord);
			dos.writeInt(this.yCoord);
			dos.writeInt(this.zCoord);
			dos.writeByte((byte) this.facing.ordinal());
			dos.writeUTF(this.owner);
		}
	}
	
	public void readFromPacket(DataInputStream dis, PacketTypeHandler packetType) throws IOException
	{
		if (dis != null)
		{
			this.facing = ForgeDirection.getOrientation(dis.readByte());
			this.owner = dis.readUTF();
		}
	}
	
	public boolean rotateBlock(ForgeDirection axis)
	{
		this.setFacing(this.getNextRotation());
		return true;
	}
	
	private ForgeDirection getNextRotation()
	{
		switch (this.getFacing())
		{
			case UP:
				return ForgeDirection.DOWN;
			case DOWN:
				return ForgeDirection.NORTH;
			case NORTH:
				return ForgeDirection.SOUTH;
			case SOUTH:
				return ForgeDirection.EAST;
			case EAST:
				return ForgeDirection.WEST;
			case WEST:
				return ForgeDirection.UP;
			default:
				return ForgeDirection.UNKNOWN;
		}
	}
}
