package com.texasjake95.core.tile;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.texasjake95.core.energy.ICustomPoweredItem;
import com.texasjake95.core.energy.IPowerHandler;
import com.texasjake95.core.energy.IPoweredItem;
import com.texasjake95.core.energy.PowerCapacitorInfo;
import com.texasjake95.core.energy.PowerHelper;
import com.texasjake95.core.energy.PowerLocation;
import com.texasjake95.core.network.packet.PacketGrinderUpdate;
import com.texasjake95.core.network.packet.PacketTypeHandler;
import com.texasjake95.core.recipe.GrinderRecipe;

import net.minecraftforge.common.ForgeDirection;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.packet.Packet;

public class TileEntityGrinder extends TileEntityCore implements ISidedInventory, IPowerHandler {
	
	public ItemStack[] inv = new ItemStack[3];
	public int spinTime = 0;
	public int recipeSpinTime = 0;
	private int recipeID = -1;
	private PowerStorage capacitor = new PowerStorage(2000, 10, 2);
	
	@Override
	public void updateEntity()
	{
		if (this.inv[0] != null)
			recipeID = GrinderRecipe.getRecipeID(this.inv[0]);
		if (!this.worldObj.isRemote)
			if (this.inv[1] != null && this.inv[1].getItem() instanceof IPoweredItem && ((IPoweredItem) this.inv[1].getItem()).canProvidePower(this.inv[1], PowerLocation.MACHINE))
			{
				int needed = this.capacitor.getCapacity() - this.capacitor.getPower();
				if (PowerHelper.isAbleToExchangePower((IPoweredItem) this.inv[1].getItem(), this.inv[1], this, true, PowerLocation.MACHINE))
				{
					int fill = PowerHelper.removePower((IPoweredItem) this.inv[1].getItem(), this.inv[1], needed, false, PowerLocation.MACHINE);
					int temp = this.capacitor.fill(fill, false);
					System.out.println("Draining " + temp);
					PowerHelper.removePower((IPoweredItem) this.inv[1].getItem(), this.inv[1], this.capacitor.fill(temp, true), true, PowerLocation.MACHINE);
				}
			}
		boolean isSpinning = this.spinTime > 0;
		boolean invChanged = false;
		if (this.spinTime > 0)
		{
			--this.spinTime;
		}
		if (!this.worldObj.isRemote)
		{
			if (this.spinTime == 0 && this.canRun())
			{
				this.spinTime = 1000;
			}
			if (this.isRunning() && this.canRun())
			{
				++this.recipeSpinTime;
				if (this.recipeSpinTime == 50)
				{
					this.recipeSpinTime = 0;
					this.spinItem();
					invChanged = true;
				}
				if (this.recipeSpinTime >= 50)
				{
					this.recipeSpinTime = 0;
				}
			}
			else
			{
				this.recipeSpinTime = 0;
			}
			if (isSpinning != this.spinTime > 0)
			{
				invChanged = true;
			}
		}
		if (invChanged)
		{
			this.onInventoryChanged();
		}
	}
	
	private void spinItem()
	{
		if (this.canRun())
		{
			GrinderRecipe recipe = GrinderRecipe.getRecipe(this.getRecipeID());
			ItemStack itemstack = recipe.getOutput(inv[0]);
			if (this.inv[2] == null)
			{
				this.inv[2] = itemstack.copy();
			}
			else if (this.inv[2].isItemEqual(itemstack))
			{
				inv[2].stackSize += itemstack.stackSize;
			}
			this.inv[0].stackSize -= recipe.getInputDecrement();
			if (this.inv[0].stackSize <= 0)
			{
				this.inv[0] = null;
			}
			this.capacitor.drain(10, true);
		}
	}
	
	@Override
	public int getSizeInventory()
	{
		return 3;
	}
	
	@Override
	public ItemStack getStackInSlot(int i)
	{
		return inv[i];
	}
	
	@Override
	public ItemStack decrStackSize(int i, int j)
	{
		if (this.inv[i] != null)
		{
			ItemStack itemstack;
			if (this.inv[i].stackSize <= j)
			{
				itemstack = this.inv[i];
				this.inv[i] = null;
				return itemstack;
			}
			else
			{
				itemstack = this.inv[i].splitStack(j);
				if (this.inv[i].stackSize == 0)
				{
					this.inv[i] = null;
				}
				return itemstack;
			}
		}
		else
		{
			return null;
		}
	}
	
	@Override
	public ItemStack getStackInSlotOnClosing(int i)
	{
		return this.getStackInSlot(i);
	}
	
	@Override
	public void setInventorySlotContents(int i, ItemStack itemstack)
	{
		this.inv[i] = itemstack;
	}
	
	@Override
	public String getInvName()
	{
		return null;
	}
	
	@Override
	public boolean isInvNameLocalized()
	{
		return false;
	}
	
	@Override
	public int getInventoryStackLimit()
	{
		return 64;
	}
	
	@Override
	public boolean isUseableByPlayer(EntityPlayer entityplayer)
	{
		return true;
	}
	
	@Override
	public void openChest()
	{
	}
	
	@Override
	public void closeChest()
	{
	}
	
	@Override
	public boolean isItemValidForSlot(int i, ItemStack itemstack)
	{
		if (i == 2)
			return false;
		else if (i == 1 && itemstack.getItem() instanceof IPoweredItem)
			return true;
		else if (i == 0)
			return true;
		return false;
	}
	
	@Override
	public int[] getAccessibleSlotsFromSide(int var1)
	{
		switch (ForgeDirection.getOrientation(var1))
		{
			case UP:
				return new int[] { 0 };
			case DOWN:
				return new int[] { 1, 2 };
			default:
				return new int[] { 1 };
		}
	}
	
	@Override
	public boolean canInsertItem(int i, ItemStack itemstack, int j)
	{
		for (int slot : this.getAccessibleSlotsFromSide(j))
		{
			if (slot != 2 && i != 2 && slot == i)
				if (this.isItemValidForSlot(i, itemstack))
					return true;
		}
		return false;
	}
	
	@Override
	public boolean canExtractItem(int i, ItemStack itemstack, int j)
	{
		for (int slot : this.getAccessibleSlotsFromSide(j))
		{
			if (slot == 2 && i == 2)
				return true;
			if (slot == 1 && i == 1 && itemstack.getItem() instanceof IPoweredItem)
			{
				if (((IPoweredItem) itemstack.getItem()).canProvidePower(itemstack, PowerLocation.MACHINE))
				{
					int power = itemstack.getItem() instanceof ICustomPoweredItem ? ((ICustomPoweredItem) itemstack.getItem()).getPower(itemstack) : 0;
					if (power > 0)
						return false;
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean canRun()
	{
		if (this.inv[0] == null || this.getRecipeID() == -1 || this.capacitor.getPower() < 10)
		{
			return false;
		}
		else
		{
			GrinderRecipe recipe = GrinderRecipe.getRecipe(this.getRecipeID());
			if (recipe == null)
				return false;
			ItemStack itemstack = recipe.getOutput(this.inv[0]);
			if (itemstack == null)
				return false;
			if (this.inv[2] == null)
				return true;
			if (!this.inv[2].isItemEqual(itemstack))
				return false;
			int result = inv[2].stackSize + itemstack.stackSize;
			return (result <= getInventoryStackLimit() && result <= itemstack.getMaxStackSize());
		}
	}
	
	public boolean isRunning()
	{
		return this.spinTime > 0;
	}
	
	@Override
	public Packet getDescriptionPacket()
	{
		return PacketTypeHandler.populatePacket(new PacketGrinderUpdate(this));
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbtTagCompound)
	{
		super.writeToNBT(nbtTagCompound);
		nbtTagCompound.setInteger("spinTime", spinTime);
		nbtTagCompound.setInteger("spinRecipeTime", recipeSpinTime);
		nbtTagCompound.setInteger("recipeID", recipeID);
		NBTTagList nbttaglist = new NBTTagList();
		for (int i = 0; i < this.inv.length; ++i)
		{
			if (this.inv[i] != null)
			{
				NBTTagCompound nbttagcompound1 = new NBTTagCompound();
				nbttagcompound1.setByte("Slot", (byte) i);
				this.inv[i].writeToNBT(nbttagcompound1);
				nbttaglist.appendTag(nbttagcompound1);
			}
		}
		nbtTagCompound.setTag("Items", nbttaglist);
		capacitor.writeToNBT(nbtTagCompound);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbtTagCompound)
	{
		super.readFromNBT(nbtTagCompound);
		spinTime = nbtTagCompound.getInteger("spinTime");
		recipeSpinTime = nbtTagCompound.getInteger("spinRecipeTime");
		recipeID = nbtTagCompound.getInteger("recipeID");
		NBTTagList nbttaglist = nbtTagCompound.getTagList("Items");
		for (int i = 0; i < nbttaglist.tagCount(); ++i)
		{
			NBTTagCompound nbttagcompound1 = (NBTTagCompound) nbttaglist.tagAt(i);
			byte b0 = nbttagcompound1.getByte("Slot");
			if (b0 >= 0 && b0 < this.inv.length)
			{
				this.inv[b0] = ItemStack.loadItemStackFromNBT(nbttagcompound1);
			}
		}
		capacitor.readFromNBT(nbtTagCompound);
	}
	
	public int getRecipeID()
	{
		return recipeID;
	}
	
	public void setRecipeID(int recipeID)
	{
		this.recipeID = recipeID;
	}
	
	@Override
	public int gainPower(ForgeDirection from, int power, boolean doFill)
	{
		return capacitor.fill(power, doFill);
	}
	
	@Override
	public int drainPower(ForgeDirection from, int power, boolean doDrain)
	{
		return capacitor.drain(power, doDrain);
	}
	
	@Override
	public boolean canFillPower(ForgeDirection from)
	{
		return capacitor.getCapacity() - capacitor.getPower() > capacitor.minTransferRate();
	}
	
	@Override
	public boolean canDrainPower(ForgeDirection from)
	{
		return capacitor.getPower() > capacitor.minTransferRate();
	}
	
	@Override
	public boolean canHandlePower(ForgeDirection from)
	{
		return true;
	}
	
	@Override
	public PowerCapacitorInfo getCapacitor()
	{
		return this.capacitor.getInfo();
	}
	
	@Override
	public void writeToPacket(DataOutputStream dos, PacketTypeHandler packetType) throws IOException
	{
		if (dos != null)
		{
			super.writeToPacket(dos, packetType);
			dos.writeInt(recipeID);
			for (int i = 0; i < 3; i++)
			{
				ItemStack stack = inv[i];
				dos.writeByte(i);
				if (stack == null)
				{
					dos.writeBoolean(false);
				}
				else
				{
					dos.writeBoolean(true);
					dos.writeInt(stack.itemID);
					dos.writeInt(stack.stackSize);
					dos.writeInt(stack.getItemDamage());
				}
			}
			dos.writeInt(spinTime);
			dos.writeInt(recipeSpinTime);
		}
	}
	
	@Override
	public void readFromPacket(DataInputStream dis, PacketTypeHandler packetType) throws IOException
	{
		if (dis != null)
		{
			super.readFromPacket(dis, packetType);
			recipeID = dis.readInt();
			int slot;
			for (int i = 0; i < 3; i++)
			{
				slot = dis.readByte();
				if (dis.readBoolean())
				{
					this.inv[slot] = new ItemStack(dis.readInt(), dis.readInt(), dis.readInt());
				}
				else
				{
					this.inv[slot] = null;
				}
			}
			spinTime = dis.readInt();
			recipeSpinTime = dis.readInt();
		}
	}
}
