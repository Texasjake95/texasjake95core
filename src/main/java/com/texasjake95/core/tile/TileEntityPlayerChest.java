package com.texasjake95.core.tile;

import java.util.ArrayList;
import java.util.HashMap;

import com.texasjake95.core.Texasjake95Core;
import com.texasjake95.core.network.packet.PacketTileEntityUpdate;
import com.texasjake95.core.network.packet.PacketTypeHandler;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.network.packet.Packet;

public class TileEntityPlayerChest extends TileEntityCore implements ISidedInventory {
	
	public HashMap<Integer, ArrayList<ItemStack>> slotMap = new HashMap<Integer, ArrayList<ItemStack>>();
	
	public TileEntityPlayerChest()
	{
	}
	
	@Override
	public int getSizeInventory()
	{
		EntityPlayer player = this.getPlayer();
		if (player != null)
		{
			return player.inventory.getSizeInventory();
		}
		return 0;
	}
	
	@Override
	public ItemStack getStackInSlot(int i)
	{
		EntityPlayer player = this.getPlayer();
		if (player != null)
		{
			return player.inventory.getStackInSlot(i);
		}
		return null;
	}
	
	@Override
	public ItemStack decrStackSize(int i, int j)
	{
		EntityPlayer player = this.getPlayer();
		if (player != null)
		{
			return player.inventory.decrStackSize(i, j);
		}
		return null;
	}
	
	@Override
	public ItemStack getStackInSlotOnClosing(int i)
	{
		EntityPlayer player = this.getPlayer();
		if (player != null)
		{
			return player.inventory.getStackInSlotOnClosing(i);
		}
		return null;
	}
	
	@Override
	public void setInventorySlotContents(int i, ItemStack itemstack)
	{
		EntityPlayer player = this.getPlayer();
		if (player != null)
		{
			player.inventory.setInventorySlotContents(i, itemstack);
		}
	}
	
	@Override
	public String getInvName()
	{
		EntityPlayer player = this.getPlayer();
		if (player != null)
		{
			return player.inventory.getInvName();
		}
		return null;
	}
	
	@Override
	public boolean isInvNameLocalized()
	{
		EntityPlayer player = this.getPlayer();
		if (player != null)
		{
			return player.inventory.isInvNameLocalized();
		}
		return false;
	}
	
	@Override
	public int getInventoryStackLimit()
	{
		EntityPlayer player = this.getPlayer();
		if (player != null)
		{
			return player.inventory.getInventoryStackLimit();
		}
		return 0;
	}
	
	@Override
	public boolean isUseableByPlayer(EntityPlayer entityplayer)
	{
		EntityPlayer player = this.getPlayer();
		if (player != null)
		{
			return (entityplayer.username.compareTo(this.owner) == 0);/* true; */
		}
		return false;
	}
	
	@Override
	public void openChest()
	{
		EntityPlayer player = this.getPlayer();
		if (player != null)
		{
			player.inventory.openChest();
		}
	}
	
	@Override
	public void closeChest()
	{
		EntityPlayer player = this.getPlayer();
		if (player != null)
		{
			player.inventory.closeChest();
		}
	}
	
	@Override
	public Packet getDescriptionPacket()
	{
		return PacketTypeHandler.populatePacket(new PacketTileEntityUpdate(this, "Texasjake95Core"));
	}
	
	@Override
	public boolean isItemValidForSlot(int i, ItemStack itemstack)
	{
		EntityPlayer player = this.getPlayer();
		if (player != null)
		{
			// if (isStackInSlotMap(i, itemstack))
			return true;
		}
		return false;
	}
	
	private boolean isStackInSlotMap(int i, ItemStack itemstack)
	{
		ArrayList<ItemStack> stacks = this.slotMap.get(i);
		if (stacks != null)
			if (stacks.contains(itemstack))
				return true;
		return false;
	}
	
	public EntityPlayer getPlayer()
	{
		return Texasjake95Core.proxy.getPlayer(this.owner);
	}
	
	@Override
	public int[] getAccessibleSlotsFromSide(int var1)
	{
		if (this.getPlayer() != null)
		{
			int[] slots = new int[this.getSizeInventory()];
			int i;
			for (i = 0; i < this.getSizeInventory(); i++)
				slots[i] = i;
			return slots;
		}
		return new int[] {};
	}
	
	@Override
	public boolean canInsertItem(int i, ItemStack itemstack, int j)
	{
		for (int slot : this.getAccessibleSlotsFromSide(j))
		{
			System.out.println("Insert: " + slot);
			if (slot == i)
				return true;
		}
		System.out.println("Cannot insert");
		return false;
	}
	
	@Override
	public boolean canExtractItem(int i, ItemStack itemstack, int j)
	{
		for (int slot : this.getAccessibleSlotsFromSide(j))
		{
			System.out.println("Extract: " + slot);
			if (slot == i)
				return true;
		}
		System.out.println("Cannot Extract");
		return false;
	}
}
