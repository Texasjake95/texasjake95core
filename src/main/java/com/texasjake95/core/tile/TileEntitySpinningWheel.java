package com.texasjake95.core.tile;

import com.texasjake95.core.network.packet.PacketTypeHandler;
import com.texasjake95.core.network.packet.PacketWheelUpdate;
import com.texasjake95.core.recipe.SpinningWheelRecipe;

import net.minecraftforge.common.ForgeDirection;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.packet.Packet;

public class TileEntitySpinningWheel extends TileEntityCore implements IInventory, ISidedInventory {
	
	public ItemStack[] inv = new ItemStack[2];
	public int spinTime = 0;
	public int recipeSpinTime = 0;
	private int recipeID = -1;
	
	@Override
	public void updateEntity()
	{
		if (this.inv[0] != null)
			recipeID = SpinningWheelRecipe.getRecipeID(this.inv[0]);
		boolean isSpinning = this.spinTime > 0;
		boolean invChanged = false;
		if (this.spinTime > 0)
		{
			--this.spinTime;
		}
		if (!this.worldObj.isRemote)
		{
			if (this.spinTime == 0 && this.canRun())
			{
				this.spinTime = 1000;
			}
			if (this.isRunning() && this.canRun())
			{
				++this.recipeSpinTime;
				if (this.recipeSpinTime == SpinningWheelRecipe.getRecipe(this.getRecipeID()).getTime())
				{
					this.recipeSpinTime = 0;
					this.spinItem();
					invChanged = true;
				}
				if (this.recipeSpinTime >= SpinningWheelRecipe.getRecipe(this.getRecipeID()).getTime())
				{
					this.recipeSpinTime = 0;
				}
			}
			else
			{
				this.recipeSpinTime = 0;
			}
			if (isSpinning != this.spinTime > 0)
			{
				invChanged = true;
			}
		}
		if (invChanged)
		{
			this.onInventoryChanged();
		}
	}
	
	private void spinItem()
	{
		if (this.canRun())
		{
			SpinningWheelRecipe recipe = SpinningWheelRecipe.getRecipe(this.getRecipeID());
			ItemStack itemstack = recipe.getOutput(inv[0]);
			if (this.inv[1] == null)
			{
				this.inv[1] = itemstack.copy();
			}
			else if (this.inv[1].isItemEqual(itemstack))
			{
				inv[1].stackSize += itemstack.stackSize;
			}
			this.inv[0].stackSize -= recipe.getInputDecrement();
			if (this.inv[0].stackSize <= 0)
			{
				this.inv[0] = null;
			}
		}
	}
	
	@Override
	public int getSizeInventory()
	{
		return 2;
	}
	
	@Override
	public ItemStack getStackInSlot(int i)
	{
		return inv[i];
	}
	
	@Override
	public ItemStack decrStackSize(int i, int j)
	{
		if (this.inv[i] != null)
		{
			ItemStack itemstack;
			if (this.inv[i].stackSize <= j)
			{
				itemstack = this.inv[i];
				this.inv[i] = null;
				return itemstack;
			}
			else
			{
				itemstack = this.inv[i].splitStack(j);
				if (this.inv[i].stackSize == 0)
				{
					this.inv[i] = null;
				}
				return itemstack;
			}
		}
		else
		{
			return null;
		}
	}
	
	@Override
	public ItemStack getStackInSlotOnClosing(int i)
	{
		return this.getStackInSlot(i);
	}
	
	@Override
	public void setInventorySlotContents(int i, ItemStack itemstack)
	{
		this.inv[i] = itemstack;
	}
	
	@Override
	public String getInvName()
	{
		return null;
	}
	
	@Override
	public boolean isInvNameLocalized()
	{
		return false;
	}
	
	@Override
	public int getInventoryStackLimit()
	{
		return 64;
	}
	
	@Override
	public boolean isUseableByPlayer(EntityPlayer entityplayer)
	{
		return true;
	}
	
	@Override
	public void openChest()
	{
	}
	
	@Override
	public void closeChest()
	{
	}
	
	@Override
	public boolean isItemValidForSlot(int i, ItemStack itemstack)
	{
		if (i == 1)
			return false;
		else
		{
			return true;
		}
	}
	
	@Override
	public int[] getAccessibleSlotsFromSide(int var1)
	{
		switch (ForgeDirection.getOrientation(var1))
		{
			case UP:
				return new int[] { 0 };
			case DOWN:
				return new int[] { 1 };
			default:
				return new int[] {};
		}
	}
	
	@Override
	public boolean canInsertItem(int i, ItemStack itemstack, int j)
	{
		for (int slot : this.getAccessibleSlotsFromSide(j))
		{
			if (slot == 0 && i == 0)
				if (this.isItemValidForSlot(i, itemstack))
					return true;
		}
		return false;
	}
	
	@Override
	public boolean canExtractItem(int i, ItemStack itemstack, int j)
	{
		for (int slot : this.getAccessibleSlotsFromSide(j))
		{
			if (slot == 1 && i == 1)
				return true;
		}
		return false;
	}
	
	public boolean canRun()
	{
		if (this.inv[0] == null || this.getRecipeID() == -1)
		{
			return false;
		}
		else
		{
			SpinningWheelRecipe recipe = SpinningWheelRecipe.getRecipe(this.getRecipeID());
			if (recipe == null)
				return false;
			ItemStack itemstack = recipe.getOutput(this.inv[0]);
			if (itemstack == null)
				return false;
			if (this.inv[1] == null)
				return true;
			if (!this.inv[1].isItemEqual(itemstack))
				return false;
			int result = inv[1].stackSize + itemstack.stackSize;
			return (result <= getInventoryStackLimit() && result <= itemstack.getMaxStackSize());
		}
	}
	
	public boolean isRunning()
	{
		return this.spinTime > 0;
	}
	
	@Override
	public Packet getDescriptionPacket()
	{
		return PacketTypeHandler.populatePacket(new PacketWheelUpdate(this));
	}
	
	@Override
	public void writeToNBT(NBTTagCompound nbtTagCompound)
	{
		super.writeToNBT(nbtTagCompound);
		nbtTagCompound.setInteger("spinTime", spinTime);
		nbtTagCompound.setInteger("spinRecipeTime", recipeSpinTime);
		nbtTagCompound.setInteger("recipeID", recipeID);
		NBTTagList nbttaglist = new NBTTagList();
		for (int i = 0; i < this.inv.length; ++i)
		{
			if (this.inv[i] != null)
			{
				NBTTagCompound nbttagcompound1 = new NBTTagCompound();
				nbttagcompound1.setByte("Slot", (byte) i);
				this.inv[i].writeToNBT(nbttagcompound1);
				nbttaglist.appendTag(nbttagcompound1);
			}
		}
		nbtTagCompound.setTag("Items", nbttaglist);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbtTagCompound)
	{
		super.readFromNBT(nbtTagCompound);
		spinTime = nbtTagCompound.getInteger("spinTime");
		recipeSpinTime = nbtTagCompound.getInteger("spinRecipeTime");
		recipeID = nbtTagCompound.getInteger("recipeID");
		NBTTagList nbttaglist = nbtTagCompound.getTagList("Items");
		for (int i = 0; i < nbttaglist.tagCount(); ++i)
		{
			NBTTagCompound nbttagcompound1 = (NBTTagCompound) nbttaglist.tagAt(i);
			byte b0 = nbttagcompound1.getByte("Slot");
			if (b0 >= 0 && b0 < this.inv.length)
			{
				this.inv[b0] = ItemStack.loadItemStackFromNBT(nbttagcompound1);
			}
		}
	}
	
	public int getRecipeID()
	{
		return recipeID;
	}
	
	public void setRecipeID(int recipeID)
	{
		this.recipeID = recipeID;
	}
}
