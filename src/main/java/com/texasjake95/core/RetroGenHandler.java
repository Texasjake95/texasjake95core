package com.texasjake95.core;

import java.util.ArrayList;
import java.util.Random;

import com.texasjake95.base.config.BaseConfig;

import cpw.mods.fml.common.IWorldGenerator;

import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.world.ChunkDataEvent;

import net.minecraft.world.ChunkCoordIntPair;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;

public class RetroGenHandler {
	
	private ArrayList<IWorldGenerator> generators;
	private BaseConfig config;
	
	public RetroGenHandler(BaseConfig config)
	{
		this.config = config;
		generators = WorldGeneratorHandler.getGenerators(config.modName());
	}
	
	@ForgeSubscribe
	public void chunkDataLoad(ChunkDataEvent.Load event)
	{
		if (!event.getData().getBoolean(String.format("texasjake95.%s.retrogen", config.modName())) && config.retroGen)
		{
			Chunk chunk = event.getChunk();
			World world = chunk.worldObj;
			long worldSeed = world.getSeed();
			ChunkCoordIntPair coords = chunk.getChunkCoordIntPair();
			Random random = new Random(worldSeed);
			long xSeed = random.nextLong() >> 2 + 1L;
			long zSeed = random.nextLong() >> 2 + 1L;
			long chunkSeed = (xSeed * coords.chunkXPos + zSeed * coords.chunkZPos) ^ worldSeed;
			int chunkoffset = 1;
			// force adjacent chunks to load
			Chunk chunk01 = world.getChunkFromChunkCoords(coords.chunkXPos, coords.chunkZPos + chunkoffset);
			Chunk chunk10 = world.getChunkFromChunkCoords(coords.chunkXPos + chunkoffset, coords.chunkZPos);
			Chunk chunk11 = world.getChunkFromChunkCoords(coords.chunkXPos + chunkoffset, coords.chunkZPos + chunkoffset);
			if (generators != null && !generators.isEmpty())
			{
				System.out.println(String.format("Retro Gening at %s, %s", coords.chunkXPos, coords.chunkZPos));
				System.out.println(String.format("Retro Gening at %s, %s", coords.chunkXPos * 16 + 8, coords.chunkZPos * 16 + 8));
				for (IWorldGenerator gen : generators)
				{
					random.setSeed(chunkSeed);
					gen.generate(random, chunk.xPosition, chunk.zPosition, world, world.provider.createChunkGenerator(), world.provider.createChunkGenerator());
				}
			}
		}
	}
	
	@ForgeSubscribe
	public void chunkDataSave(ChunkDataEvent.Save event)
	{
		if (config.retroGen)
			event.getData().setBoolean(String.format("texasjake95.%s.retrogen", config.modName()), true);
		System.out.println(String.format("Saving chunk at %s, %s", event.getChunk().xPosition, event.getChunk().zPosition));
	}
}
