package com.texasjake95.core.network;

import com.texasjake95.core.network.packet.PacketTX;
import com.texasjake95.core.network.packet.PacketTypeHandler;

import cpw.mods.fml.common.network.IPacketHandler;
import cpw.mods.fml.common.network.Player;

import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet250CustomPayload;

public class TXPacketHandler implements IPacketHandler {
	
	@Override
	public void onPacketData(INetworkManager manager, Packet250CustomPayload packet, Player player)
	{
		PacketTX packettx = PacketTypeHandler.buildPacket(packet.data);
		packettx.execute(manager, player);
	}
}
