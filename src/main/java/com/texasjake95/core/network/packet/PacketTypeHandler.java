package com.texasjake95.core.network.packet;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;

import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.Packet250CustomPayload;

public enum PacketTypeHandler
{
	KEY(PacketKeyPressed.class), Tile(PacketTileEntityUpdate.class), Wheel(PacketWheelUpdate.class), Grinder(PacketGrinderUpdate.class), FluidTransportRender(PacketTileFluidTransportRender.class);
	
	public static PacketTX buildPacket(byte[] data)
	{
		ByteArrayInputStream bis = new ByteArrayInputStream(data);
		int selector = bis.read();
		DataInputStream dis = new DataInputStream(bis);
		PacketTX packet = null;
		try
		{
			packet = values()[selector].clazz.newInstance();
		}
		catch (Exception e)
		{
			e.printStackTrace(System.err);
		}
		packet.readPopulate(dis);
		return packet;
	}
	
	public static PacketTX buildPacket(PacketTypeHandler type)
	{
		PacketTX packet = null;
		try
		{
			packet = values()[type.ordinal()].clazz.newInstance();
		}
		catch (Exception e)
		{
			e.printStackTrace(System.err);
		}
		return packet;
	}
	
	public static Packet populatePacket(PacketTX packettx)
	{
		byte[] data = packettx.populate();
		Packet250CustomPayload packet250 = new Packet250CustomPayload();
		packet250.channel = packettx.channelName;
		packet250.data = data;
		packet250.length = data.length;
		packet250.isChunkDataPacket = packettx.isChunkDataPacket;
		return packet250;
	}
	
	private Class<? extends PacketTX> clazz;
	
	PacketTypeHandler(Class<? extends PacketTX> clazz)
	{
		this.clazz = clazz;
	}
}
