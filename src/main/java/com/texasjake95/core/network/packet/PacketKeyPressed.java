package com.texasjake95.core.network.packet;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.texasjake95.core.Items.interfaces.ICoreKeyBound;

import cpw.mods.fml.common.network.Player;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.INetworkManager;

/**
 * PacketKeyPressed
 * 
 * Packet specifically for notifying the server of client key pressed events
 * 
 * @author pahimar
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 * 
 */
public class PacketKeyPressed extends PacketTX {
	
	public String key;
	
	public PacketKeyPressed()
	{
		super(PacketTypeHandler.KEY, false, "Texasjake95Core");
	}
	
	public PacketKeyPressed(String key)
	{
		super(PacketTypeHandler.KEY, false, "Texasjake95Core");
		this.key = key;
	}
	
	@Override
	public void execute(INetworkManager manager, Player player)
	{
		EntityPlayer thePlayer = (EntityPlayer) player;
		if (thePlayer.getCurrentEquippedItem() != null && thePlayer.getCurrentEquippedItem().getItem() instanceof ICoreKeyBound)
		{
			((ICoreKeyBound) thePlayer.getCurrentEquippedItem().getItem()).doKeyBindingAction(thePlayer, thePlayer.getCurrentEquippedItem(), this.key);
		}
	}
	
	@Override
	public void readData(DataInputStream data) throws IOException
	{
		this.key = data.readUTF();
	}
	
	public void setKey(String key)
	{
		this.key = key;
	}
	
	@Override
	public void writeData(DataOutputStream data) throws IOException
	{
		data.writeUTF(this.key);
	}
}