package com.texasjake95.core.network.packet;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.texasjake95.core.tile.TileEntitySpinningWheel;

import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.network.Player;

import net.minecraft.item.ItemStack;
import net.minecraft.network.INetworkManager;

public class PacketWheelUpdate extends PacketTileEntityUpdate {
	
	private TileEntitySpinningWheel wheel;
	private int spinTime, recipeSpinTime, recipeID;
	private ItemStack[] inv = new ItemStack[2];
	
	public PacketWheelUpdate()
	{
		super(PacketTypeHandler.Wheel, "Texasjake95Core");
	}
	
	public PacketWheelUpdate(TileEntitySpinningWheel tileEntitySpinningWheel)
	{
		super(PacketTypeHandler.Wheel, tileEntitySpinningWheel, "Texasjake95Core");
		this.wheel = tileEntitySpinningWheel;
		this.inv = tileEntitySpinningWheel.inv;
		this.recipeID = tileEntitySpinningWheel.getRecipeID();
		this.spinTime = tileEntitySpinningWheel.spinTime;
		this.recipeSpinTime = tileEntitySpinningWheel.recipeSpinTime;
	}
	
	@Override
	public void writeData(DataOutputStream data) throws IOException
	{
		super.writeData(data);
		data.writeInt(recipeID);
		for (int i = 0; i < 2; i++)
		{
			ItemStack stack = wheel.inv[i];
			data.writeByte(i);
			if (stack == null)
			{
				data.writeBoolean(false);
			}
			else
			{
				data.writeBoolean(true);
				data.writeInt(stack.itemID);
				data.writeInt(stack.stackSize);
				data.writeInt(stack.getItemDamage());
			}
		}
		data.writeInt(spinTime);
		data.writeInt(recipeSpinTime);
	}
	
	@Override
	public void readData(DataInputStream data) throws IOException
	{
		super.readData(data);
		recipeID = data.readInt();
		int slot;
		for (int i = 0; i < 2; i++)
		{
			slot = data.readByte();
			if (data.readBoolean())
			{
				this.inv[slot] = new ItemStack(data.readInt(), data.readInt(), data.readInt());
			}
			else
			{
				this.inv[slot] = null;
			}
		}
		spinTime = data.readInt();
		recipeSpinTime = data.readInt();
	}
	
	@Override
	public void execute(INetworkManager manager, Player player)
	{
		super.execute(manager, player);
		TileEntitySpinningWheel tileEntity = (TileEntitySpinningWheel) FMLClientHandler.instance().getClient().theWorld.getBlockTileEntity(x, y, z);
		if (tileEntity != null)
		{
			int i;
			for (i = 0; i < 2; i++)
				tileEntity.setInventorySlotContents(i, this.inv[i]);
		}
		tileEntity.setRecipeID(this.recipeID);
		tileEntity.spinTime = this.spinTime;
		tileEntity.recipeSpinTime = this.spinTime;
	}
}
