package com.texasjake95.core;

import com.texasjake95.core.tile.adfg;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class BLOCKAD extends BlockContainer {
	
	protected BLOCKAD(int par1)
	{
		super(par1, Material.wood);
	}
	
	@Override
	public TileEntity createNewTileEntity(World world)
	{
		return new adfg();
	}
}
