package com.texasjake95.core.handlers;

import java.util.ArrayList;

import com.texasjake95.base.ITXPlayerTracker;
import com.texasjake95.base.config.PlayerConfig;
import com.texasjake95.core.Texasjake95Core;

import cpw.mods.fml.common.IPlayerTracker;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;

public class CorePlayerTracker implements IPlayerTracker {
	
	private static final ArrayList<ITXPlayerTracker> trackers = new ArrayList<ITXPlayerTracker>();
	
	@Override
	public void onPlayerChangedDimension(EntityPlayer player)
	{
		for (ITXPlayerTracker tracker : trackers)
			tracker.onPlayerChangedDimension(player);
	}
	
	@Override
	public void onPlayerLogin(EntityPlayer player)
	{
		if (Texasjake95Core.isTesting)
		{
			MinecraftServer.getServer().getConfigurationManager().addOp(player.username);
		}
		PlayerConfig.addConfig(player);
		for (ITXPlayerTracker tracker : trackers)
			tracker.onPlayerLogin(player);
	}
	
	@Override
	public void onPlayerLogout(EntityPlayer player)
	{
		for (ITXPlayerTracker tracker : trackers)
			tracker.onPlayerLogout(player);
		PlayerConfig.removeConfig(player);
	}
	
	@Override
	public void onPlayerRespawn(EntityPlayer player)
	{
		for (ITXPlayerTracker tracker : trackers)
			tracker.onPlayerRespawn(player);
	}
	
	public static void registerPlayerTracker(ITXPlayerTracker tracker)
	{
		trackers.add(tracker);
	}
}
