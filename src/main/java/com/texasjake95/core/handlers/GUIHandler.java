package com.texasjake95.core.handlers;

import com.texasjake95.core.client.gui.GuiGrinder;
import com.texasjake95.core.client.gui.GuiPlayerChest;
import com.texasjake95.core.client.gui.GuiPortableCrafting;
import com.texasjake95.core.client.gui.GuiPortableFurnace;
import com.texasjake95.core.client.gui.GuiSpinnigWheel;
import com.texasjake95.core.inventory.ContainerGrinder;
import com.texasjake95.core.inventory.ContainerPlayerChest;
import com.texasjake95.core.inventory.ContainerPortableCrafting;
import com.texasjake95.core.inventory.ContainerPortableFurnace;
import com.texasjake95.core.inventory.ContainerSpinningWheel;
import com.texasjake95.core.inventory.InventoryPortableFurnace;
import com.texasjake95.core.tile.TileEntityGrinder;
import com.texasjake95.core.tile.TileEntityPlayerChest;
import com.texasjake95.core.tile.TileEntitySpinningWheel;

import cpw.mods.fml.common.network.IGuiHandler;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;

public class GUIHandler implements IGuiHandler {
	
	@Override
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
	{
		switch (ID)
		{
			case 2:
				return new GuiPortableFurnace(player.inventory, new InventoryPortableFurnace(player.inventory.getCurrentItem()));
			case 7:
				return new GuiSpinnigWheel(player, (TileEntitySpinningWheel) world.getBlockTileEntity(x, y, z));
			case 8:
				return new GuiGrinder(player, (TileEntityGrinder) world.getBlockTileEntity(x, y, z));
			case 9:
				return new GuiPlayerChest(player.inventory, (TileEntityPlayerChest) world.getBlockTileEntity(x, y, z));
		}
		return new GuiPortableCrafting(player, world, x, y, z);
	}
	
	@Override
	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
	{
		switch (ID)
		{
			case 2:
				return new ContainerPortableFurnace(player.inventory, new InventoryPortableFurnace(player.inventory.getCurrentItem()));
			case 7:
				return new ContainerSpinningWheel(player.inventory, (TileEntitySpinningWheel) world.getBlockTileEntity(x, y, z));
			case 8:
				return new ContainerGrinder(player.inventory, (TileEntityGrinder) world.getBlockTileEntity(x, y, z));
			case 9:
				return new ContainerPlayerChest(player.inventory, (TileEntityPlayerChest) world.getBlockTileEntity(x, y, z));
		}
		return new ContainerPortableCrafting(player.inventory, world, x, y, z);
	}
}
