package com.texasjake95.core;

import java.util.Random;

import com.texasjake95.base.helpers.WorldGenHelper;

import cpw.mods.fml.common.IWorldGenerator;

import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.feature.WorldGenerator;

public class RetroGenTest implements IWorldGenerator {
	
	private WorldGenerator test;
	private int IDK = 0;
	
	public RetroGenTest()
	{
		test = WorldGenHelper.Surface(Texasjake95Core.test.blockID, 5, IDK, IDK);
	}
	
	@Override
	public void generate(Random random, int chunkX, int chunkZ, World world, IChunkProvider chunkGenerator, IChunkProvider chunkProvider)
	{
		for (int i = 0; i < 5; i++)
		{
			test.generate(world, random, chunkX * 16, 12 + random.nextInt(70), chunkZ * 16);
		}
	}
}
