package com.texasjake95.core.recipe;

import java.util.ArrayList;

import net.minecraftforge.oredict.OreDictionary;

import net.minecraft.item.ItemStack;

public class SpinningWheelRecipe {
	
	private static int nextID = 0;
	private static ArrayList<SpinningWheelRecipe> recipeMap = new ArrayList<SpinningWheelRecipe>();
	private final int id;
	private final int time;
	private ItemStack input;
	private ItemStack output;
	
	public SpinningWheelRecipe(ItemStack input, ItemStack output, int id, int time)
	{
		this.input = input;
		this.output = output;
		this.id = id;
		this.time = time;
	}
	
	public ItemStack getOutput(ItemStack input)
	{
		return doesItemStackMatch(input, true) ? this.output : null;
	}
	
	public int getInputDecrement()
	{
		return this.input.stackSize;
	}
	
	public boolean doesItemStackMatch(ItemStack stack, boolean sizeMatters)
	{
		if (input.itemID != stack.itemID || (input.getItemDamage() != stack.getItemDamage() && input.getItemDamage() != OreDictionary.WILDCARD_VALUE))
			return false;
		return sizeMatters ? stack.stackSize >= this.input.stackSize : true;
	}
	
	public static void addRecipe(ItemStack input, ItemStack output, int time)
	{
		recipeMap.add(new SpinningWheelRecipe(input, output, nextID, time));
		// FMLInterModComms.sendMessage(Reference.MOD_ID,
		// InterModComms.ADD_RECIPE, NBTHelper.encodeRecipeAsNBT(output,
		// Arrays.asList(input)));
		nextID++;
	}
	
	public static SpinningWheelRecipe getRecipe(ItemStack input)
	{
		for (SpinningWheelRecipe recipe : recipeMap)
		{
			if (recipe.doesItemStackMatch(input, false))
				return recipe;
		}
		return null;
	}
	
	public static SpinningWheelRecipe getRecipe(int id)
	{
		return recipeMap.get(id);
	}
	
	public static int getRecipeID(ItemStack itemStack)
	{
		if (getRecipe(itemStack) != null)
			return getRecipe(itemStack).id;
		return -1;
	}
	
	public int getTime()
	{
		return this.time;
	}
}
