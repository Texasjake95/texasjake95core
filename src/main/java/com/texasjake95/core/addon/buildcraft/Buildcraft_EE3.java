package com.texasjake95.core.addon.buildcraft;

import com.texasjake95.core.addon.ee3.IEE3;

public class Buildcraft_EE3 implements IEE3 {
	
	private boolean activate = true;
	
	@Override
	public void activate(boolean b)
	{
		this.activate = b;
	}
	
	@Override
	public boolean getActivate()
	{
		return this.activate;
	}
	
	@Override
	public String ModName()
	{
		return "Texasjake95Core - Buildcraft";
	}
	
	@Override
	public void preLoad()
	{
	}
	
	@Override
	public void load()
	{
		// this.addEMC();
	}
	
	private void addEMC()
	{
		// for (AssemblyRecipe recipe : AssemblyRecipe.assemblyRecipes)
		// {
		// List<CustomWrappedStack> input = new ArrayList<CustomWrappedStack>();
		// for (ItemStack stack : recipe.input)
		// input.add(new CustomWrappedStack(stack));
		// input.add(new CustomWrappedStack(new EnergyStack("mj", (int)
		// recipe.energy)));
		// FMLInterModComms.sendMessage(Reference.MOD_ID,
		// InterModComms.ADD_RECIPE, NBTHelper.encodeRecipeAsNBT(recipe.output,
		// input));
		// }
		// for (Recipe recipe : RefineryRecipes.getRecipes())
		// {
		// List<CustomWrappedStack> input = new ArrayList<CustomWrappedStack>();
		// input.add(new CustomWrappedStack(recipe.ingredient1));
		// input.add(new CustomWrappedStack(recipe.ingredient2));
		// input.add(new CustomWrappedStack(new EnergyStack("mj", (int)
		// recipe.energy)));
		// FMLInterModComms.sendMessage(Reference.MOD_ID,
		// InterModComms.ADD_RECIPE, NBTHelper.encodeRecipeAsNBT(recipe.result,
		// input));
		// }
		// for (Fuel fuel : IronEngineFuel.fuels.values())
		// {
		// List<CustomWrappedStack> input = new ArrayList<CustomWrappedStack>();
		// input.add(new
		// CustomWrappedStack(FluidContainerRegistry.fillFluidContainer(new
		// FluidStack(fuel.liquid, FluidContainerRegistry.BUCKET_VOLUME),
		// FluidContainerRegistry.EMPTY_BUCKET)));
		// int size = (int) (fuel.totalBurningTime * fuel.powerPerCycle);
		// FMLInterModComms.sendMessage(Reference.MOD_ID,
		// InterModComms.ADD_RECIPE, NBTHelper.encodeRecipeAsNBT(new
		// EnergyStack("mj", size), input));
		// }
	}
	
	@Override
	public void postLoad()
	{
	}
	
	@Override
	public void onServerLoad()
	{
	}
	
	@Override
	public void onServerLoaded()
	{
	}
}
