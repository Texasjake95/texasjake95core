package com.texasjake95.core.config;

import java.io.IOException;

import com.texasjake95.base.config.BaseConfig;
import com.texasjake95.commons.file.config.ConfigFile;
import com.texasjake95.core.CoreRefernece;

public class CoreConfig extends BaseConfig {
	
	private static CoreConfig instance;
	public boolean autoSwitch = false;
	
	public CoreConfig() throws IOException
	{
		super(new CoreConfigWriter(), false);
	}
	
	public static CoreConfig getInstance()
	{
		if (instance == null)
			try
			{
				instance = new CoreConfig();
			}
			catch (IOException e)
			{
			}
		return instance;
	}
	
	public void endProps()
	{
		configw.endProps();
	}
	
	public void initProps()
	{
		super.initProps();
		config.load();
		autoSwitch = Boolean.parseBoolean(config.getCatagory(ConfigFile.GENERAL).addPropertyWithComment("Auto Switch Tool", false, "Set this to true for the equipped tool to be switched to the most effective tool on the hot bar").value);
		Boolean.parseBoolean(config.getCatagory(ConfigFile.GENERAL).addPropertyWithComment("Auto Switch Tool", false, "Set this to true for the equipped tool to be switched to the most effective tool on the hot bar").value);
		config.save();
	}
	
	@Override
	public String modName()
	{
		return CoreRefernece.CoreName;
	}
}
