package com.texasjake95.core.api;

import com.texasjake95.api.RegisteredBlocks;
import com.texasjake95.api.RegisteredItems;

import net.minecraft.block.Block;
import net.minecraft.item.Item;

public class CoreApiItems {
	
	public static final Item Craft = getItem("Portable Crafting Table");
	
	@SuppressWarnings("unused")
	private static final Block getBlock(String tag)
	{
		return RegisteredBlocks.getBlock("Core", tag);
	}
	
	private static final Item getItem(String tag)
	{
		return RegisteredItems.getItem("Core", tag);
	}
}
