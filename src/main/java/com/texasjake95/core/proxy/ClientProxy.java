package com.texasjake95.core.proxy;

import com.texasjake95.core.EntityBlizzard;
import com.texasjake95.core.EntityCustomLightningBolt;
import com.texasjake95.core.RenderCustomLightningBolt;
import com.texasjake95.core.RenderEntityBlizzard;
import com.texasjake95.core.Texasjake95Core;
import com.texasjake95.core.block.RenderIndex;
import com.texasjake95.core.tile.TileEntityFluidTransport;

import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.registry.TickRegistry;
import cpw.mods.fml.relauncher.Side;

import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.common.MinecraftForge;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;

public class ClientProxy extends CommonProxy {
	
	public EntityPlayer getPlayer(String name)
	{
		if (Minecraft.getMinecraft().isIntegratedServerRunning())
		{
			EntityPlayer player = Minecraft.getMinecraft().getIntegratedServer().getConfigurationManager().getPlayerForUsername(name);
			if (player == null)
				System.out.println("Player is null: " + name + "on IS");
			return player;
		}
		if (Minecraft.getMinecraft().thePlayer == null)
			System.out.println("Player is null: " + name + "on Client");
		return Minecraft.getMinecraft().thePlayer;
	}
	
	@Override
	public void registerEntityRenders()
	{
		RenderingRegistry.registerEntityRenderingHandler(EntityCustomLightningBolt.class, new RenderCustomLightningBolt());
		RenderingRegistry.registerEntityRenderingHandler(EntityBlizzard.class, new RenderEntityBlizzard());
	}
	
	@Override
	public void registerTileEntities()
	{
		super.registerTileEntities();
		RenderIndex.fluidTransort = RenderingRegistry.getNextAvailableRenderId();
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityFluidTransport.class, new TileEntityFluidTransportRenderer());
		MinecraftForgeClient.registerItemRenderer(Texasjake95Core.fluidTransport.blockID, new ItemFluidTransportRender());
	}
	
	public void registerEventHandlers()
	{
		super.registerEventHandlers();
		MinecraftForge.EVENT_BUS.register(new ClientEventHandler());
	}
	
	public void registerTickHanders(Object mod)
	{
		super.registerTickHanders(mod);
		TickRegistry.registerTickHandler(new ClientTickHandler(), Side.CLIENT);
	}
}
