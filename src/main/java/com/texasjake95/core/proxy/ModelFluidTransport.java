package com.texasjake95.core.proxy;

public class ModelFluidTransport extends ModelCore {
	
	public ModelFluidTransport()
	{
		super("/assets/texasjake95core/model/entity/FluidTransport.obj");
	}
	
	public void renderInHand()
	{
		this.renderOnly("Front", "PipeFront", "Corner");
	}
}
