package com.texasjake95.core.proxy;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.client.FMLClientHandler;

import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

public class ItemFluidTransportRender extends ItemRenderCore {
	
	public ItemFluidTransportRender()
	{
		super(new ModelFluidTransport());
	}
	
	@Override
	public boolean handleRenderType(ItemStack item, ItemRenderType type)
	{
		return true;
	}
	
	@Override
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item, ItemRendererHelper helper)
	{
		return true;
	}
	
	@Override
	public void renderItem(ItemRenderType type, ItemStack item, Object... data)
	{
		switch (type)
		{
			case ENTITY:
			{
				renderModel(-0.5F, -0.38F, 0.5F, 1.0F);
				return;
			}
			case EQUIPPED:
			{
				renderModel(0.0F, 0.0F, 1.0F, 1.0F);
				return;
			}
			case EQUIPPED_FIRST_PERSON:
			{
				renderModel(0.0F, 0.0F, 1.0F, 1.0F);
				return;
			}
			case INVENTORY:
			{
				renderModel(-1.0F, -0.9F, 0.0F, 1.0F);
				return;
			}
			default:
				return;
		}
	}
	
	private void renderModel(float x, float y, float z, float scale)
	{
		ModelFluidTransport fluidModel = (ModelFluidTransport) this.model;
		GL11.glPushMatrix();
		GL11.glDisable(GL11.GL_LIGHTING);
		// Scale, Translate, Rotate
		GL11.glScalef(scale, scale, scale);
		GL11.glTranslatef(x, y, z);
		GL11.glRotatef(-90F, 1, 0, 0);
		// Bind texture
		FMLClientHandler.instance().getClient().renderEngine.bindTexture(new ResourceLocation("texasjake95core", "/textures/model/FluidTransport.png"));
		// Render
		fluidModel.renderInHand();
		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glPopMatrix();
	}
}
