package com.texasjake95.core.proxy;

import java.util.ArrayList;
import java.util.EnumSet;

import com.texasjake95.core.Texasjake95Core;
import com.texasjake95.core.ToolHandlerRegistry;
import com.texasjake95.core.config.CoreConfig;

import cpw.mods.fml.common.ITickHandler;
import cpw.mods.fml.common.TickType;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

// TODO Create Item Handler for to help tools decide if they are appropriate
// i.e. determine durability % able to harvest block
// blackList etc
// Create Map for above!!!!!
public class ClientTickHandler implements ITickHandler {
	
	private int currentItem = -1;
	private boolean shouldSwitchBack = false;
	private int tickCount = 0;
	
	@Override
	public void tickStart(EnumSet<TickType> type, Object... tickData)
	{
		if (type.contains(TickType.PLAYER))
			doPlayerTick(true, tickData);
	}
	
	@Override
	public void tickEnd(EnumSet<TickType> type, Object... tickData)
	{
		if (type.contains(TickType.PLAYER))
			doPlayerTick(false, tickData);
	}
	
	@Override
	public EnumSet<TickType> ticks()
	{
		return EnumSet.of(TickType.PLAYER);
	}
	
	private void doPlayerTick(boolean tickStart, Object... tickData)
	{
		doAutoSwitchStart(tickStart, tickData);
	}
	
	private void doAutoSwitchStart(boolean tickStart, Object... tickData)
	{
		if (tickStart)
		{
			if (tickData[0] instanceof EntityPlayer)
			{
				EntityPlayer player = (EntityPlayer) tickData[0];
				if (player.capabilities.isCreativeMode)
					return;
				World world = player.worldObj;
				if (CoreConfig.getInstance().autoSwitch)
					if (Minecraft.getMinecraft().inGameHasFocus)
						if (GameSettings.isKeyDown(Minecraft.getMinecraft().gameSettings.keyBindAttack))
						{
							ToolHandlerRegistry tools = ToolHandlerRegistry.getInstance();
							tickCount = 0;
							MovingObjectPosition mop = Minecraft.getMinecraft().objectMouseOver;
							if (mop != null)
							{
								if (world.isAirBlock(mop.blockX, mop.blockY, mop.blockZ))
									return;
								int blockID = world.getBlockId(mop.blockX, mop.blockY, mop.blockZ);
								int blockMeta = world.getBlockMetadata(mop.blockX, mop.blockY, mop.blockZ);
								int hotBarSize = InventoryPlayer.getHotbarSize();
								float bestFloat = 0;
								int bestSlot = 0;
								currentItem = currentItem > -1 ? currentItem : player.inventory.currentItem;
								ArrayList<Integer> bestSlots = new ArrayList<Integer>();
								int fakeCurrentItem = player.inventory.currentItem;
								for (int i = fakeCurrentItem; i < hotBarSize; i++)
								{
									if (tools.canAutoSwtichTo(player.inventory.getStackInSlot(i)))
										if (tools.canHarvest(blockID, blockMeta, player.inventory.getStackInSlot(i)))
										{
											player.inventory.currentItem = i;
											float strVsBlock = player.getCurrentPlayerStrVsBlock(Block.blocksList[blockID], true, blockMeta);
											if (bestFloat < strVsBlock)
											{
												bestFloat = strVsBlock;
												bestSlot = i;
												if (Texasjake95Core.isTesting)
													System.out.println("Best Item at " + bestSlot + " with strength of " + bestFloat);
											}
										}
								}
								for (int i = 0; i <= fakeCurrentItem; i++)
								{
									if (tools.canAutoSwtichTo(player.inventory.getStackInSlot(i)))
										if (tools.canHarvest(blockID, blockMeta, player.inventory.getStackInSlot(i)))
										{
											player.inventory.currentItem = i;
											float strVsBlock = player.getCurrentPlayerStrVsBlock(Block.blocksList[blockID], true, blockMeta);
											if (bestFloat < strVsBlock)
											{
												bestFloat = strVsBlock;
												bestSlot = i;
												if (Texasjake95Core.isTesting)
													System.out.println("Best Item at " + bestSlot + " with strength of " + bestFloat);
											}
										}
								}
								for (int i = fakeCurrentItem; i < hotBarSize; i++)
								{
									if (tools.canAutoSwtichTo(player.inventory.getStackInSlot(i)))
									{
										player.inventory.currentItem = i;
										float strVsBlock = player.getCurrentPlayerStrVsBlock(Block.blocksList[blockID], true, blockMeta);
										if (bestFloat == strVsBlock)
										{
											if (!bestSlots.contains(i))
												bestSlots.add(i);
										}
									}
								}
								for (int i = 0; i <= fakeCurrentItem; i++)
								{
									if (tools.canAutoSwtichTo(player.inventory.getStackInSlot(i)))
									{
										player.inventory.currentItem = i;
										float strVsBlock = player.getCurrentPlayerStrVsBlock(Block.blocksList[blockID], true, blockMeta);
										if (bestFloat == strVsBlock)
										{
											if (!bestSlots.contains(i))
												bestSlots.add(i);
										}
									}
								}
								if (bestFloat <= 1.0F)
								{
									for (int i = fakeCurrentItem; i < hotBarSize; i++)
									{
										if (tools.canAutoSwtichTo(player.inventory.getStackInSlot(i)))
										{
											player.inventory.currentItem = i;
											if (player.inventory.getCurrentItem() == null || !tools.isDamageable(player.inventory.getCurrentItem()))
											{
												bestFloat = -1.0F;
												bestSlot = i;
												break;
											}
										}
									}
									if (bestFloat != -1.0F)
										for (int i = 0; i <= fakeCurrentItem; i++)
										{
											if (tools.canAutoSwtichTo(player.inventory.getStackInSlot(i)))
											{
												player.inventory.currentItem = i;
												if (player.inventory.getCurrentItem() == null || !tools.isDamageable(player.inventory.getCurrentItem()))
												{
													bestSlot = i;
													break;
												}
											}
										}
								}
								double meta = 1.0F;
								if (bestFloat != -1.0F)
									for (int slot : bestSlots)
									{
										if (player.inventory.getStackInSlot(slot) != null)
										{
											meta = meta > tools.getDurability(player.inventory.getStackInSlot(slot)) ? tools.getDurability(player.inventory.getStackInSlot(slot)) : meta;
											if (Texasjake95Core.isTesting)
												System.out.println(meta);
											if (meta == tools.getDurability(player.inventory.getStackInSlot(slot)))
											{
												bestSlot = slot;
												if (Texasjake95Core.isTesting)
													System.out.println(bestSlot + ":" + meta);
											}
										}
									}
								player.inventory.currentItem = bestSlot;
								if (Texasjake95Core.isTesting)
									System.out.println(blockID + ":" + blockMeta);
							}
						}
						else
						{
							tickCount++;
							if (tickCount >= 5)
							{
								shouldSwitchBack = true;
							}
						}
			}
		}
		else
		{
			if (tickData[0] instanceof EntityPlayer)
			{
				EntityPlayer player = (EntityPlayer) tickData[0];
				if (player.capabilities.isCreativeMode)
					return;
				if (CoreConfig.getInstance().autoSwitch)
					if (!GameSettings.isKeyDown(Minecraft.getMinecraft().gameSettings.keyBindAttack) && currentItem > -1 && shouldSwitchBack)
					{
						player.inventory.currentItem = currentItem;
						currentItem = -1;
						shouldSwitchBack = false;
					}
			}
		}
	}
	
	@Override
	public String getLabel()
	{
		return "Texasjake95Core - TickHandler";
	}
}
