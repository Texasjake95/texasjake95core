package com.texasjake95.core;

public class CoreRefernece {
	
	public static final String CoreDependencies = "";
	public static final String CoreID = "Texasjake95Core";
	public static final String CoreName = "Texasjake95 Core";
	public static final String Major = "*COREMAJOR*";
	public static final String Minor = "*COREMINOR*";
	public static final String Revision = "*COREREVISION*";
	public static final String CoreVersion = Major + "." + Minor + "." + Revision;
}
