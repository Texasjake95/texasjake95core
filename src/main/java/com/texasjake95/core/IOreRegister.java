package com.texasjake95.core;

public interface IOreRegister {
	
	public int[] getMeta();
	
	public String getOreName(int meta);
}
