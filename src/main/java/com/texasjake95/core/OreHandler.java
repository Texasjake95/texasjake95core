package com.texasjake95.core;

import java.util.ArrayList;
import java.util.TreeMap;

import com.texasjake95.base.helpers.NBTHelper;
import com.texasjake95.core.energy.IPoweredItem;
import com.texasjake95.core.energy.PowerHelper;

import net.minecraftforge.event.Event;
import net.minecraftforge.event.EventPriority;
import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.oredict.OreDictionary.OreRegisterEvent;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;

public class OreHandler {
	
	public static TreeMap<String, ArrayList<ItemStack>> allOres = new TreeMap<String, ArrayList<ItemStack>>();
	private static boolean init = false;
	
	public static void populateList()
	{
		if (!init)
		{
			init = true;
			ArrayList<ItemStack> allItems = new ArrayList<ItemStack>();
			for (Item item : Item.itemsList)
			{
				if (item != null)
				{
					item.getSubItems(item.itemID, CreativeTabs.tabAllSearch, allItems);
				}
			}
			int id = 0;
			String oreName = "";
			for (ItemStack stack : allItems)
			{
				id = OreDictionary.getOreID(stack);
				if (id != -1)
				{
					oreName = OreDictionary.getOreName(id);
					ArrayList<ItemStack> ores = allOres.get(oreName);
					if (ores == null)
						ores = new ArrayList<ItemStack>();
					ores.add(stack);
					allOres.put(oreName, ores);
				}
			}
		}
	}
	
	@ForgeSubscribe(priority = EventPriority.LOWEST)
	public void catchOres(OreRegisterEvent event)
	{
		// if (!event.isCanceled())
		// {
		// String name = event.Name;
		// ItemStack ore = event.Ore;
		// System.out.println(name + ":" + String.format("%d:%d:%d",
		// ore.itemID,ore.stackSize,ore.getItemDamage()));
		// if (allOres.containsKey(name))
		// {
		// ArrayList<ItemStack> ores = allOres.get(name);
		// ores.add(ore);
		// allOres.put(name, ores);
		// }
		// else
		// {
		// ArrayList<ItemStack> ores = new ArrayList<ItemStack>();
		// ores.add(ore);
		// allOres.put(name, ores);
		// }
		// }
	}
	
	@ForgeSubscribe
	public void test2(BlockEvent.BreakEvent event)
	{
	}
	
	@ForgeSubscribe
	public void test3(PlayerInteractEvent event)
	{
//		if (event.action == PlayerInteractEvent.Action.RIGHT_CLICK_BLOCK || event.action == PlayerInteractEvent.Action.RIGHT_CLICK_AIR)
//		{
//			EntityPlayer player = event.entityPlayer;
//			if (player != null)
//			{
//				ItemStack stack = player.getCurrentEquippedItem();
//				if (stack != null)
//				{
//					if (stack.getItem() instanceof ItemBlock)
//					{
//						event.setCanceled(true);
//						event.useItem = Event.Result.DENY;
//						event.useBlock = Event.Result.DENY;
//					}
//				}
//			}
//		}
	}
	
	@ForgeSubscribe
	public void test(ItemTooltipEvent event)
	{
		if (event.itemStack.itemID == CoreItemHandler.getInstace().getItem("Generic Armor").itemID)
		{
			event.toolTip.add("Durablity: " + NBTHelper.getInt(event.itemStack, "Damage"));
			event.toolTip.add("Max Durablity: " + ((ItemGenericArmor) event.itemStack.getItem()).getTrueMaxDamage(event.itemStack));
			event.toolTip.add("Durablity %: " + ((float) NBTHelper.getInt(event.itemStack, "Damage") / (float) ((ItemGenericArmor) event.itemStack.getItem()).getTrueMaxDamage(event.itemStack)));
		}
		if (event.itemStack.getItem() instanceof IPoweredItem && ((IPoweredItem) event.itemStack.getItem()).useDefaultToolTip(event.itemStack))
		{
			ItemStack itemStack = event.itemStack;
			IPoweredItem item = (IPoweredItem) event.itemStack.getItem();
			int power = PowerHelper.getPower(itemStack);
			int capacity = item.getCapacity(itemStack);
			double powerPer = PowerHelper.getPowerPer(event.itemStack);
			EnumChatFormatting colour = powerPer > .8F ? EnumChatFormatting.DARK_GREEN : powerPer > .6F ? EnumChatFormatting.GREEN : powerPer > .4F ? EnumChatFormatting.YELLOW : powerPer > .2F ? EnumChatFormatting.GOLD : powerPer > 0F ? EnumChatFormatting.RED : EnumChatFormatting.DARK_RED;
			event.toolTip.add(String.format("%s%d / %d", colour, power, capacity));
		}
	}
}