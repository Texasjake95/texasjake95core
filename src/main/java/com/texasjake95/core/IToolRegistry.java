package com.texasjake95.core;

import net.minecraft.item.ItemStack;

public interface IToolRegistry {
	
	public void registerToolHandler(IToolHandler handler, ItemStack stack, boolean damageMatters);
	
	public void registerToolHandler(IToolHandler handler, int itemID, int itemMeta, boolean damageMatters);
	
	public IToolHandler getHandler(ItemStack stack);
	
	public IToolHandler getHandler(int itemID, int itemMeta);
}
