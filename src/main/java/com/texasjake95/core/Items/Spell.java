package com.texasjake95.core.Items;

import com.texasjake95.base.helpers.NBTHelper;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public abstract class Spell {
	
	private static Spell[] spellList = new Spell[8];
	private int id;
	private String[] names;
	static
	{
		new SpellFire();
		new SpellThunder();
		new SpellBlizard();
	}
	
	public Spell(int id, String[] names)
	{
		this.id = id;
		this.names = names;
		spellList[this.id] = this;
	}
	
	public abstract float getDamage(int lvl);
	
	public abstract void doSpell(World world, EntityPlayer player, ItemStack stack);
	
	public int getLvl(ItemStack stack)
	{
		return stack.getItemDamage() % 3 + 1;
	}
	
	public String getName(ItemStack stack)
	{
		return "_" + this.names[stack.getItemDamage() % 3];
	}
	
	public static Spell getSpell(ItemStack stack)
	{
		return spellList[stack.getItemDamage() / 3];
	}
	
	protected final void setSpellTimer(ItemStack stack, int time)
	{
		NBTHelper.setInteger(stack, ItemBaseKeyBlade.SpellTimer, time);
	}
}
