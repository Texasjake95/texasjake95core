package com.texasjake95.core.Items;

import com.texasjake95.base.helpers.NBTHelper;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;

public class BagInventoryLarge implements IInventory {
	
	private ItemStack[] inv = new ItemStack[36 * 2];
	private ItemStack stack;
	private EntityPlayer player;
	
	public BagInventoryLarge(ItemStack stack)
	{
		this.stack = stack;
		readFromNBT(stack);
	}
	
	@Override
	public void closeChest()
	{
		writeToNBT(stack);
		if (player != null)
			this.player.inventory.setInventorySlotContents(this.player.inventory.currentItem, this.stack);
	}
	
	public void readFromNBT(ItemStack stack)
	{
		NBTTagCompound tags = NBTHelper.getCompound(stack);
		NBTTagList nbttaglist = tags.getTagList("Items");
		this.inv = new ItemStack[this.getSizeInventory()];
		for (int i = 0; i < nbttaglist.tagCount(); ++i)
		{
			NBTTagCompound nbttagcompound1 = (NBTTagCompound) nbttaglist.tagAt(i);
			int j = nbttagcompound1.getByte("Slot") & 255;
			if (j >= 0 && j < this.inv.length)
			{
				this.inv[j] = ItemStack.loadItemStackFromNBT(nbttagcompound1);
			}
		}
	}
	
	/**
	 * Writes a tile entity to NBT.
	 */
	public void writeToNBT(ItemStack stack)
	{
		NBTTagCompound tags = NBTHelper.getCompound(stack);
		NBTTagList nbttaglist = new NBTTagList();
		for (int i = 0; i < this.inv.length; ++i)
		{
			if (this.inv[i] != null)
			{
				NBTTagCompound nbttagcompound1 = new NBTTagCompound();
				nbttagcompound1.setByte("Slot", (byte) i);
				this.inv[i].writeToNBT(nbttagcompound1);
				nbttaglist.appendTag(nbttagcompound1);
			}
		}
		tags.setTag("Items", nbttaglist);
	}
	
	@Override
	public ItemStack decrStackSize(int par1, int par2)
	{
		if (this.inv[par1] != null)
		{
			ItemStack var3;
			if (this.inv[par1].stackSize <= par2)
			{
				var3 = this.inv[par1];
				this.inv[par1] = null;
				this.onInventoryChanged();
				return var3;
			}
			else
			{
				var3 = this.inv[par1].splitStack(par2);
				if (this.inv[par1].stackSize == 0)
				{
					this.inv[par1] = null;
				}
				this.onInventoryChanged();
				return var3;
			}
		}
		else
		{
			return null;
		}
	}
	
	@Override
	public int getInventoryStackLimit()
	{
		return 64;
	}
	
	@Override
	public String getInvName()
	{
		return "Texasjake95_Inventory";
	}
	
	@Override
	public int getSizeInventory()
	{
		return this.inv.length - 9;
	}
	
	@Override
	public ItemStack getStackInSlot(int var1)
	{
		return this.inv[var1];
	}
	
	@Override
	public ItemStack getStackInSlotOnClosing(int var1)
	{
		if (this.inv[var1] != null)
		{
			ItemStack var2 = this.inv[var1];
			this.inv[var1] = null;
			return var2;
		}
		else
		{
			return null;
		}
	}
	
	@Override
	public boolean isUseableByPlayer(EntityPlayer var1)
	{
		this.player = var1;
		return true;
	}
	
	@Override
	public void onInventoryChanged()
	{
	}
	
	@Override
	public void openChest()
	{
		this.readFromNBT(this.stack);
	}
	
	@Override
	public void setInventorySlotContents(int var1, ItemStack var2)
	{
		this.inv[var1] = var2;
		if (var2 != null && var2.stackSize > this.getInventoryStackLimit())
		{
			var2.stackSize = this.getInventoryStackLimit();
		}
		this.onInventoryChanged();
	}
	
	@Override
	public boolean isInvNameLocalized()
	{
		return false;
	}
	
	@Override
	public boolean isItemValidForSlot(int i, ItemStack itemstack)
	{
		return true;
	}
	
	public static ItemStack combineNBTs(BagInventoryLarge large, BagInventory bag1, BagInventory bag2)
	{
		ItemStack[] inv = large.inv;
		int i;
		for (i = 0; i < bag1.getSizeInventory(); i++)
		{
			inv[i] = bag1.getStackInSlot(i);
		}
		for (i = 0; i < bag2.getSizeInventory(); i++)
		{
			inv[i + bag1.getSizeInventory()] = bag2.getStackInSlot(i);
		}
		large.inv = inv;
		large.writeToNBT(large.stack);
		return large.stack.copy();
	}
}
