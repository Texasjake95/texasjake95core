package com.texasjake95.core.Items;

import com.texasjake95.base.items.CoreItem_Base;
import com.texasjake95.core.energy.IPowerHandler;

import net.minecraftforge.common.ForgeDirection;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ChatMessageComponent;
import net.minecraft.world.World;

public class ItemTxWrench extends CoreItem_Base {
	
	public ItemTxWrench(int ID)
	{
		super(ID);
		this.setUnlocalizedName("txWrench");
	}
	
	@Override
	public void registerIcons(IconRegister par1IconRegister)
	{
	}
	
	@Override
	public boolean onItemUseFirst(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ)
	{
		int blockId = world.getBlockId(x, y, z);
		TileEntity tile = world.getBlockTileEntity(x, y, z);
		if (tile != null)
		{
			if (tile instanceof IPowerHandler)
			{
				IPowerHandler power = (IPowerHandler) tile;
				if (player.isSneaking())
				{
					player.sendChatToPlayer(new ChatMessageComponent().addText(power.getCapacitor().getPower() + "/" + power.getCapacitor().getCapacity()));
				}
			}
		}
		Block block = Block.blocksList[blockId];
		if (block == null)
			return false;
		if (block.rotateBlock(world, x, y, z, ForgeDirection.getOrientation(side)))
		{
			player.swingItem();
			return !world.isRemote;
		}
		return false;
	}
}
