package com.texasjake95.core.Items;

import java.util.List;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;

public class ItemBlockSpinningWheel extends ItemBlock {
	
	public ItemBlockSpinningWheel(int par1)
	{
		super(par1);
		this.setHasSubtypes(true);
		this.setMaxDamage(0);
	}
	
	@Override
	public int getMetadata(int par1)
	{
		return par1;
	}
	
	@Override
	public String getUnlocalizedName(ItemStack itemstack)
	{
		switch (itemstack.getItemDamage())
		{
			case 1:
				return "Grinder";
			default:
				return "Spinning Wheel";
		}
	}
	
	@Override
	@SuppressWarnings("rawtypes")
	public void getSubItems(int par1, CreativeTabs par2CreativeTabs, List par3List)
	{
	}
	
	public int setDamageFromMetadata(int i, int metadata)
	{
		return metadata;
	}
}
