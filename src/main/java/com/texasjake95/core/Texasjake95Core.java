package com.texasjake95.core;

import com.texasjake95.base.config.PlayerConfig;
import com.texasjake95.base.config.WorldConfig;
import com.texasjake95.base.helpers.ComItmStk;
import com.texasjake95.base.helpers.CoreHelper;
import com.texasjake95.base.helpers.CoreHelper.ItemRegister;
import com.texasjake95.base.helpers.LoggerHelper;
import com.texasjake95.base.items.multi.MultiWeapon;
import com.texasjake95.commons.util.SortableCustomList;
import com.texasjake95.core.Items.ItemBaseKeyBlade;
import com.texasjake95.core.Items.ItemBlockSpinningWheel;
import com.texasjake95.core.Items.ItemDrill;
import com.texasjake95.core.Items.ItemTxWrench;
import com.texasjake95.core.addon.AddonHandler;
import com.texasjake95.core.block.BlockFluidGen;
import com.texasjake95.core.block.BlockFluidTrasport;
import com.texasjake95.core.block.BlockPlayerChest;
import com.texasjake95.core.block.BlockSpinningWheel;
import com.texasjake95.core.config.CoreConfig;
import com.texasjake95.core.handlers.CorePlayerTracker;
import com.texasjake95.core.handlers.GUIHandler;
import com.texasjake95.core.network.TXPacketHandler;
import com.texasjake95.core.proxy.CommonProxy;
import com.texasjake95.core.recipe.GrinderRecipe;
import com.texasjake95.core.recipe.SpinningWheelRecipe;
import com.texasjake95.core.tile.TileEntityFluidGen;
import com.texasjake95.core.tile.TileEntityFluidTransport;
import com.texasjake95.core.tile.TileEntityGrinder;
import com.texasjake95.core.tile.TileEntityPlayerChest;
import com.texasjake95.core.tile.TileEntitySpinningWheel;
import com.texasjake95.core.tile.adfg;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartedEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.event.FMLServerStoppedEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.GameRegistry;

import net.minecraftforge.common.EnumHelper;
import net.minecraftforge.common.IShearable;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidContainerRegistry;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.oredict.OreDictionary;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.EnumArmorMaterial;
import net.minecraft.item.EnumToolMaterial;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;
import net.minecraft.util.Icon;

@Mod(modid = CoreRefernece.CoreID, name = CoreRefernece.CoreName, version = CoreRefernece.CoreVersion, dependencies = CoreRefernece.CoreDependencies)
@NetworkMod(channels = { "Texasjake95Core" }, clientSideRequired = true, serverSideRequired = false, packetHandler = TXPacketHandler.class)
public class Texasjake95Core {
	
	public static final DamageSource lightning = new CustomDamageSource("lightning.damage").setMagicDamage();
	public static final DamageSource blizzard = new CustomDamageSource("blizzard.damage").setMagicDamage();
	public AddonHandler addonHandler;
	@Instance("Texasjake95Core")
	public static Texasjake95Core instance;
	@SidedProxy(clientSide = "com.texasjake95.core.proxy.ClientProxy", serverSide = "com.texasjake95.core.proxy.CommonProxy")
	public static CommonProxy proxy;
	public static boolean isTesting = false;
	Block wheel;
	Item keyBlade;
	Block playerChest;
	Block fluidGen;
	private BLOCKAD adfg;
	public static Fluid fluidTest;
	public static Block fluidTransport;
	public static Icon fluidIconStill;
	public static Icon fluidIconFlow;
	public static boolean serverLoaded = false;
	private static EnumArmorMaterial copper = EnumHelper.addArmorMaterial("TXCopper", 1, new int[] { 3, 8, 6, 3 }, 2);
	private static EnumToolMaterial txDrill = EnumHelper.addToolMaterial("txDrill", EnumToolMaterial.EMERALD.getHarvestLevel() + 1, EnumToolMaterial.EMERALD.getMaxUses(), EnumToolMaterial.EMERALD.getEfficiencyOnProperMaterial() * 2, EnumToolMaterial.EMERALD.getDamageVsEntity() + 2, 0);
	private static EnumToolMaterial txDrillDiamond = EnumHelper.addToolMaterial("txDrillDiamond", EnumToolMaterial.EMERALD.getHarvestLevel() + 3, EnumToolMaterial.EMERALD.getMaxUses(), EnumToolMaterial.EMERALD.getEfficiencyOnProperMaterial() * 4, EnumToolMaterial.EMERALD.getDamageVsEntity() + 4, 0);
	private static EnumToolMaterial[] drillMaterial = new EnumToolMaterial[] { EnumToolMaterial.STONE, EnumToolMaterial.IRON, EnumToolMaterial.EMERALD, txDrill, txDrillDiamond };
	
	public Texasjake95Core()
	{
		addonHandler = AddonHandler.getInstance();
		CoreConfig.getInstance().initProps();
		ItemGenericArmor.addArmor(copper, "diamond_leggings", "textures/models/armor/diamond_layer_2.png", 2, "copper", 0, 0, "cobblestone");
		ItemGenericArmor.addArmor(copper, "diamond_helmet", "textures/models/armor/diamond_layer_1.png", 0, "copper", 0, 0, "cobblestone");
		ItemGenericArmor.addArmor(copper, "diamond_boots", "textures/models/armor/diamond_layer_1.png", 3, "copper", 0, 0, "cobblestone");
		ItemGenericArmor.addArmor(copper, "diamond_chestplate", "textures/models/armor/diamond_layer_1.png", 1, "copper", 0, 0, "cobblestone");
		CoreItemHandler.getInstace().addItemHandler(new ItemHandler("Key Blade", ItemBaseKeyBlade.class, CoreConfig.getInstance().configw));
		CoreItemHandler.getInstace().addBlockHandler(new BlockHandler("TestBlock", Block.class, ItemBlock.class, CoreConfig.getInstance().configw, new Class<?>[] { Material.class }, new Object[] { Material.craftedSnow }));
		CoreItemHandler.getInstace().addItemHandler(new ItemHandler("Generic Armor", ItemGenericArmor.class, CoreConfig.getInstance().configw));
		CoreItemHandler.getInstace().addItemHandler(new ItemHandler("Wrench", ItemTxWrench.class, CoreConfig.getInstance().configw));
		for (int tier = 4; tier > -1; tier--)
		{
			CoreItemHandler.getInstace().addItemHandler(new ItemHandler("Battery Tier " + (tier + 1), ItemPoweredBatteryTest.class, CoreConfig.getInstance().configw, new Class<?>[] { int.class }, new Object[] { tier }));
			CoreItemHandler.getInstace().addItemHandler(new ItemHandler("Drill - Tier " + (tier + 1), ItemDrill.class, CoreConfig.getInstance().configw, new Class<?>[] { EnumToolMaterial.class, int.class }, new Object[] { drillMaterial[tier], tier }));
		}
		MinecraftForge.EVENT_BUS.register(new OreHandler());
	}
	
	public static Block test;
	
	@EventHandler
	public void preload(FMLPreInitializationEvent event)
	{
		LoggerHelper.initLogger();
		CoreItems.init();
		fluidTest = new Fluid("test");
		FluidRegistry.registerFluid(fluidTest);
		Item temp = new Item(25000);
		FluidContainerRegistry.registerFluidContainer(FluidRegistry.getFluidStack("test", FluidContainerRegistry.BUCKET_VOLUME / 2), new ItemStack(temp), new ItemStack(Item.bucketEmpty));
		wheel = new BlockSpinningWheel(4095);
		playerChest = new BlockPlayerChest(4094, Material.wood);
		fluidGen = new BlockFluidGen(4093);
		fluidTransport = new BlockFluidTrasport(4092);
		test = new Block(4001, Material.rock);
		adfg = new BLOCKAD(4000);
		proxy.registerEntityRenders();
		proxy.registerEventHandlers();
		GameRegistry.registerBlock(wheel, ItemBlockSpinningWheel.class, "SpinningWheel");
		GameRegistry.registerBlock(playerChest, "playerChest");
		GameRegistry.registerBlock(fluidGen, "fluidGen");
		GameRegistry.registerBlock(adfg, "adfg");
		GameRegistry.registerBlock(fluidTransport, "fluidTransport");
		proxy.registerTileEntities();
		SpinningWheelRecipe.addRecipe(new ItemStack(Item.wheat, 10, 0), new ItemStack(Item.goldNugget, 1, 0), 1000);
		SpinningWheelRecipe.addRecipe(new ItemStack(Block.cloth, 1, OreDictionary.WILDCARD_VALUE), new ItemStack(Item.silk, 4, 0), 250);
		GrinderRecipe.addRecipe(new ItemStack(Block.oreIron), new ItemStack(Block.oreIron, 2, 0));
		GameRegistry.registerPlayerTracker(new CorePlayerTracker());
		GameRegistry.registerTileEntity(TileEntityGrinder.class, "TXGrinder");
		GameRegistry.registerTileEntity(TileEntitySpinningWheel.class, "TXSpinningWheel");
		GameRegistry.registerTileEntity(TileEntityFluidGen.class, "TXFluidGen");
		GameRegistry.registerTileEntity(TileEntityPlayerChest.class, "TXPlayerChest");
		GameRegistry.registerTileEntity(TileEntityFluidTransport.class, "TXFluidTransoprt");
		GameRegistry.registerTileEntity(adfg.class, "adfg");
		this.addonHandler.preLoadAddons();
		CoreItemHandler.getInstace().initItems();
		CoreItemHandler.getInstace().initBlocks();
		if (CoreItemHandler.getInstace().isItemLoaded("Generic Armor"))
			ArmorHandler.initRecipes();
		OreDictionary.registerOre("cobblestone", CoreItemHandler.getInstace().getBlock("TestBlock"));
		MinecraftForge.setBlockHarvestLevel(Block.workbench, "axe", 0);
		MinecraftForge.setToolClass(Item.shears, "shear", 0);
	}
	
	private static final IToolHandler powerHandler = new PoweredToolHandler();
	public static final boolean AlwaysSet = false;
	
	@EventHandler
	public void load(FMLInitializationEvent event)
	{
		// CoreHelper.RegisterHelper.registerWorldGen(CoreRefernece.CoreName,
		// new RetroGenTest());
		// MinecraftForge.EVENT_BUS.register(new
		// RetroGenHandler(CoreConfig.getInstance()));
		for (int tier = 0; tier < 5; tier++)
		{
			CoreHelper.RegisterHelper.registerTool(CoreItemHandler.getInstace().getItem("Drill - Tier " + (tier + 1)));
			ToolHandlerRegistry.getInstance().registerToolHandler(powerHandler, new ItemStack(CoreItemHandler.getInstace().getItem("Drill - Tier " + (tier + 1))), false);
		}
		proxy.registerTickHanders(this);
		NetworkRegistry.instance().registerGuiHandler(instance, new GUIHandler());
		ChestRecipe.addChestRecipe(new ItemStack(CoreItems.Craft, 1, 3), new ItemStack(CoreItems.Craft, 1, 1), new ItemStack(CoreItems.Craft, 1, 1));
		this.addonHandler.loadAddons();
	}
	
	@EventHandler
	public void postLoad(FMLPostInitializationEvent event)
	{
		SortableCustomList<ComItmStk> temp = new SortableCustomList<ComItmStk>();
		temp.addAll(ComItmStk.toArray(MultiWeapon.blocksEffectiveAgainst));
		temp.cleanUp(new ComItmStk[0]);
		temp.sort();
		CoreConfig.getInstance().endProps();
		ItemRegister.DEBUG();
		// ArrayList<ItemStack> subItems = new ArrayList<ItemStack>();
		// ArrayList<CustomWrappedStack> allItems = new
		// ArrayList<CustomWrappedStack>();
		// for (Item item : Item.itemsList)
		// {
		// if (item != null)
		// {
		// item.getSubItems(item.itemID, CreativeTabs.tabAllSearch, subItems);
		// }
		// for (ItemStack stack : subItems)
		// {
		// allItems.add(new CustomWrappedStack(stack));
		// }
		// subItems.clear();
		// }
		// TestMapUsage.init(allItems);
		// for (Object o : CraftingManager.getInstance().getRecipeList())
		// {
		// if (o instanceof IRecipe)
		// {
		// WrappedRecipe wrapped = new WrappedRecipe(o);
		// if (TestMapUsage.isValid(wrapped.getNode()))
		// TestMapUsage.data.addWrapper(new WrappedRecipe(o));
		// }
		// }
		// for (Item item : Item.itemsList)
		// {
		// if (item != null)
		// {
		// item.getSubItems(item.itemID, CreativeTabs.tabAllSearch, subItems);
		// }
		// for (ItemStack stack : subItems)
		// {
		// ItemStack out = FurnaceRecipes.smelting().getSmeltingResult(stack);
		// if (out != null)
		// {
		// if (TestMapUsage.isValid(out))
		// TestMapUsage.data.addWrapper(new WrappedRecipe(new
		// FurnaceRecipe(stack, out)));
		// }
		// }
		// subItems.clear();
		// }
		// OreHandler.populateList();
		// for (String oreName : OreHandler.allOres.keySet())
		// {
		// System.out.println(oreName);
		// ArrayList<ItemStack> stacks = OreHandler.allOres.get(oreName);
		// for (ItemStack stack : stacks)
		// {
		// OreStack ore = new OreStack(stack);
		// ArrayList<CustomWrappedStack> inputs = new
		// ArrayList<CustomWrappedStack>();
		// CustomWrappedStack output = new CustomWrappedStack(ore);
		// CustomWrappedStack input = new CustomWrappedStack(stack);
		// inputs.add(input);
		// TestMapUsage.data.addWrapper(new WrappedRecipe(output, inputs));
		// System.out.println("\t" + inputs.get(0).toString());
		// }
		// }
		// for (CustomWrappedStack stack :
		// RecipesPotions.getPotionRecipes().keys())
		// {
		// Collection<List<CustomWrappedStack>> stacks =
		// RecipesPotions.getPotionRecipes().get(stack);
		// for (List<CustomWrappedStack> inputs : stacks)
		// if (TestMapUsage.isValid(stack))
		// TestMapUsage.data.addWrapper(new WrappedRecipe(new
		// PotionRecipe(stack, inputs)));
		// }
		// System.out.println(TestMapUsage.data.getWrappers(new
		// CustomWrappedStack(Item.ingotGold)).size());
		// for (WrappedRecipe recipe : TestMapUsage.data.getWrappers(new
		// CustomWrappedStack(Block.cloth)))
		// {
		// System.out.println("Output: " + recipe.getNode().toString());
		// for (CustomWrappedStack stack : recipe.getConnectedNodes())
		// {
		// System.out.println("Input: " + stack.toString());
		// }
		// }
		// System.out.println();
		// for (CustomWrappedStack stack : TestMapUsage.data.getCritNodes())
		// {
		// System.out.println(stack.toString());
		// }
		// System.out.println();
		// System.out.println();
		// System.out.println();
		// for (CustomWrappedStack stack : allItems)
		// {
		// if (TestMapUsage.isValid(stack))
		// {
		// System.out.println("Getting value for: " + stack.toString());
		// FloatValue value = TestMapUsage.data.getValue(stack);
		// if (value != null)
		// System.out.println("\t" + stack.toString() + ": " +
		// value.getValue());
		// }
		// }
		this.addonHandler.postLoadAddons();
	}
	
	@EventHandler
	public void onServerLoad(FMLServerStartedEvent event)
	{
		if (!serverLoaded)
		{
			serverLoaded = true;
			for (Block block : Block.blocksList)
			{
				if (block instanceof IShearable)
				{
					System.out.println("Registering " + block.getLocalizedName());
					MinecraftForge.setBlockHarvestLevel(block, "shear", 0);
				}
			}
		}
		this.addonHandler.onServerLoaded();
	}
	
	@EventHandler
	public void onServerLoading(FMLServerStartingEvent event)
	{
		WorldConfig.initWorldConfig(event.getServer().getEntityWorld());
		this.addonHandler.onServerLoad();
	}
	
	@EventHandler
	public void onServerStopped(FMLServerStoppedEvent event)
	{
		WorldConfig.wconfig.save();
		PlayerConfig.saveAll(true);
	}
	
	public static void addItemHandler()
	{
	}
}
