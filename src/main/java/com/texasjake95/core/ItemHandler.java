package com.texasjake95.core;

import java.lang.reflect.InvocationTargetException;
import java.util.LinkedList;

import com.texasjake95.base.config.ConfigWriterTemplate;

import net.minecraft.item.Item;

public class ItemHandler implements IItemHandler {
	
	LinkedList<Class<?>> parameterTypes = new LinkedList<Class<?>>();
	LinkedList<Object> parameters = new LinkedList<Object>();
	private final Class<? extends Item> itemClass;
	private String itemName;
	private final ConfigWriterTemplate configw;
	private int itemID;
	private int priority;
	private String modName = null;
	
	public ItemHandler(String name, Class<? extends Item> itemClass, ConfigWriterTemplate configw)
	{
		this(name, itemClass, configw, new Class<?>[0], new Object[0]);
	}
	
	public ItemHandler(String name, Class<? extends Item> itemClass, ConfigWriterTemplate configw, Class<?>[] parameterTypes, Object[] parameters)
	{
		this.itemName = name;
		this.itemClass = itemClass;
		this.configw = configw;
		this.parameterTypes.add(int.class);
		for (Class<?> parameterType : parameterTypes)
			this.parameterTypes.add(parameterType);
		for (Object parameter : parameters)
			this.parameters.add(parameter);
		this.configw.config().getCatagory("IDs").addSubCatagoryWithComment("Items", "All Item can be turned off by setting id to -1");
	}
	
	@Override
	public String itemName()
	{
		return itemName;
	}
	
	@Override
	public boolean shouldInit()
	{
		this.itemID = this.configw.getItemID(this.itemName, -1);
		this.parameters.addFirst(this.itemID);
		return this.itemID != -1;
	}
	
	@Override
	public Item initItem()
	{
		Class<?>[] types = this.parameterTypes.toArray(new Class<?>[0]);
		Object[] params = this.parameters.toArray(new Object[0]);
		try
		{
			return this.itemClass.getConstructor(types).newInstance(params);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public int getPriority()
	{
		return this.priority;
	}
	
	@Override
	public IItemHandler setPriority(int priority)
	{
		this.priority = priority;
		return this;
	}
	
	@Override
	public IItemHandler setModName(String modName)
	{
		this.modName = modName;
		return this;
	}
	
	@Override
	public String getModName()
	{
		return this.modName;
	}
}
