package com.texasjake95.core;

import java.util.ArrayList;
import java.util.TreeMap;

import cpw.mods.fml.common.IWorldGenerator;

public class WorldGeneratorHandler {
	
	private static TreeMap<String, ArrayList<IWorldGenerator>> worldGens = new TreeMap<String, ArrayList<IWorldGenerator>>();
	
	public static ArrayList<IWorldGenerator> getGenerators(String modName)
	{
		if (!worldGens.containsKey(modName))
			worldGens.put(modName, new ArrayList<IWorldGenerator>());
		return worldGens.get(modName);
	}
	
	public static void addWorldGen(String modName, IWorldGenerator worldGen)
	{
		ArrayList<IWorldGenerator> modGens = worldGens.get(modName);
		if (modGens == null)
			modGens = new ArrayList<IWorldGenerator>();
		modGens.add(worldGen);
		worldGens.put(modName, modGens);
	}
}
