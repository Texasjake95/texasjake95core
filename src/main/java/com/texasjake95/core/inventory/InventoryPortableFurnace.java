package com.texasjake95.core.inventory;

import com.texasjake95.base.helpers.NBTHelper;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntityFurnace;
import net.minecraft.world.World;

public class InventoryPortableFurnace implements IInventory {
	
	public ItemStack[] inv = new ItemStack[3];
	public int cookTime;
	public int burnTime;
	public int currentItemburnTime;
	private ItemStack stack;
	private EntityPlayer player;
	
	public InventoryPortableFurnace(ItemStack stack)
	{
		this.stack = stack;
		this.readFromNBT(stack);
	}
	
	@Override
	public int getSizeInventory()
	{
		return this.inv.length;
	}
	
	@Override
	public ItemStack getStackInSlot(int i)
	{
		return this.inv[i];
	}
	
	@Override
	public ItemStack decrStackSize(int i, int j)
	{
		if (this.inv[i] != null)
		{
			ItemStack itemstack;
			if (this.inv[i].stackSize <= j)
			{
				itemstack = this.inv[i];
				this.inv[i] = null;
				return itemstack;
			}
			else
			{
				itemstack = this.inv[i].splitStack(j);
				if (this.inv[i].stackSize == 0)
				{
					this.inv[i] = null;
				}
				return itemstack;
			}
		}
		else
		{
			return null;
		}
	}
	
	@Override
	public ItemStack getStackInSlotOnClosing(int i)
	{
		if (this.inv[i] != null)
		{
			ItemStack itemstack = this.inv[i];
			this.inv[i] = null;
			return itemstack;
		}
		else
		{
			return null;
		}
	}
	
	@Override
	public void setInventorySlotContents(int i, ItemStack itemstack)
	{
		this.inv[i] = itemstack;
		if (itemstack != null && itemstack.stackSize > this.getInventoryStackLimit())
		{
			itemstack.stackSize = this.getInventoryStackLimit();
		}
	}
	
	@Override
	public String getInvName()
	{
		return "tx.inv.portable_furnace.name";
	}
	
	@Override
	public boolean isInvNameLocalized()
	{
		return false;
	}
	
	@Override
	public int getInventoryStackLimit()
	{
		return 64;
	}
	
	@Override
	public void onInventoryChanged()
	{
		writeToNBT(stack);
		if (this.player != null)
			this.player.inventory.setInventorySlotContents(this.player.inventory.currentItem, this.stack);
	}
	
	@Override
	public boolean isUseableByPlayer(EntityPlayer entityplayer)
	{
		this.player = entityplayer;
		return true;
	}
	
	@Override
	public void openChest()
	{
		this.readFromNBT(stack);
	}
	
	@Override
	public void closeChest()
	{
		writeToNBT(stack);
		this.player.inventory.setInventorySlotContents(this.player.inventory.currentItem, this.stack);
		this.player = null;
	}
	
	@Override
	public boolean isItemValidForSlot(int i, ItemStack itemstack)
	{
		return i == 2 ? false : i == 1 ? TileEntityFurnace.isItemFuel(itemstack) : true;
	}
	
	@SideOnly(Side.CLIENT)
	public int getCookProgressScaled(int i)
	{
		return this.cookTime * i / 200;
	}
	
	@SideOnly(Side.CLIENT)
	public int getburnTimeRemainingScaled(int i)
	{
		if (this.currentItemburnTime == 0)
		{
			this.currentItemburnTime = 200;
		}
		return this.burnTime * i / this.currentItemburnTime;
	}
	
	public boolean isBurning()
	{
		return this.burnTime > 0;
	}
	
	public boolean canSmelt()
	{
		if (this.inv[0] == null)
		{
			return false;
		}
		else
		{
			ItemStack itemstack = FurnaceRecipes.smelting().getSmeltingResult(this.inv[0]);
			if (itemstack == null)
				return false;
			if (this.inv[2] == null)
				return true;
			if (!this.inv[2].isItemEqual(itemstack))
				return false;
			int result = inv[2].stackSize + itemstack.stackSize;
			return (result <= getInventoryStackLimit() && result <= itemstack.getMaxStackSize());
		}
	}
	
	public void smeltItem()
	{
		if (this.canSmelt())
		{
			ItemStack itemstack = FurnaceRecipes.smelting().getSmeltingResult(this.inv[0]);
			if (this.inv[2] == null)
			{
				this.inv[2] = itemstack.copy();
			}
			else if (this.inv[2].isItemEqual(itemstack))
			{
				inv[2].stackSize += itemstack.stackSize;
			}
			--this.inv[0].stackSize;
			if (this.inv[0].stackSize <= 0)
			{
				this.inv[0] = null;
			}
		}
	}
	
	public void update(World world)
	{
		boolean flag = this.burnTime > 0;
		boolean flag1 = false;
		if (this.burnTime > 0)
		{
			--this.burnTime;
		}
		if (!world.isRemote)
		{
			if (this.burnTime == 0 && this.canSmelt())
			{
				this.currentItemburnTime = this.burnTime = TileEntityFurnace.getItemBurnTime(this.inv[1]);
				if (this.burnTime > 0)
				{
					if (this.inv[1] != null)
					{
						--this.inv[1].stackSize;
						if (this.inv[1].stackSize == 0)
						{
							this.inv[1] = this.inv[1].getItem().getContainerItemStack(this.inv[1]);
						}
					}
				}
			}
			if (this.isBurning() && this.canSmelt())
			{
				++this.cookTime;
				if (this.cookTime == 200)
				{
					this.cookTime = 0;
					this.smeltItem();
				}
			}
			else
			{
				this.cookTime = 0;
			}
			if (flag != this.burnTime > 0)
			{
				flag1 = true;
			}
		}
		if (flag1)
		{
			this.onInventoryChanged();
		}
	}
	
	/**
	 * Reads a tile entity from NBT.
	 */
	public void readFromNBT(ItemStack stack)
	{
		NBTTagCompound tags = NBTHelper.getCompound(stack);
		NBTTagList nbttaglist = tags.getTagList("Items");
		this.inv = new ItemStack[this.getSizeInventory()];
		for (int i = 0; i < nbttaglist.tagCount(); ++i)
		{
			NBTTagCompound nbttagcompound1 = (NBTTagCompound) nbttaglist.tagAt(i);
			byte b0 = nbttagcompound1.getByte("Slot");
			if (b0 >= 0 && b0 < this.inv.length)
			{
				this.inv[b0] = ItemStack.loadItemStackFromNBT(nbttagcompound1);
			}
		}
		this.burnTime = tags.getShort("burnTime");
		this.cookTime = tags.getShort("cookTime");
		this.currentItemburnTime = TileEntityFurnace.getItemBurnTime(this.inv[1]);
	}
	
	/**
	 * Writes a tile entity to NBT.
	 */
	public void writeToNBT(ItemStack stack)
	{
		NBTTagCompound tags = NBTHelper.getCompound(stack);
		tags.setShort("burnTime", (short) this.burnTime);
		tags.setShort("cookTime", (short) this.cookTime);
		NBTTagList nbttaglist = new NBTTagList();
		for (int i = 0; i < this.inv.length; ++i)
		{
			if (this.inv[i] != null)
			{
				NBTTagCompound nbttagcompound1 = new NBTTagCompound();
				nbttagcompound1.setByte("Slot", (byte) i);
				this.inv[i].writeToNBT(nbttagcompound1);
				nbttaglist.appendTag(nbttagcompound1);
			}
		}
		tags.setTag("Items", nbttaglist);
		stack.stackTagCompound = tags;
	}
}
