package com.texasjake95.core.inventory;

import com.texasjake95.core.recipe.SpinningWheelRecipe;
import com.texasjake95.core.tile.TileEntitySpinningWheel;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class ContainerSpinningWheel extends Container {
	
	private TileEntitySpinningWheel wheel;
	
	public ContainerSpinningWheel(IInventory player, TileEntitySpinningWheel wheel)
	{
		this.wheel = wheel;
		this.addSlotToContainer(new Slot(wheel, 0, 56, 17));
		this.addSlotToContainer(new WheelSlot(wheel, 1, 56, 53));
		int i;
		for (i = 0; i < 3; ++i)
		{
			for (int j = 0; j < 9; ++j)
			{
				this.addSlotToContainer(new Slot(player, j + i * 9 + 9, 8 + j * 18, 84 + i * 18));
			}
		}
		for (i = 0; i < 9; ++i)
		{
			this.addSlotToContainer(new Slot(player, i, 8 + i * 18, 142));
		}
	}
	
	@Override
	public boolean canInteractWith(EntityPlayer entityplayer)
	{
		return this.wheel.isUseableByPlayer(entityplayer);
	}
	
	public ItemStack transferStackInSlot(EntityPlayer par1EntityPlayer, int par2)
	{
		ItemStack itemstack = null;
		Slot slot = (Slot) this.inventorySlots.get(par2);
		if (slot != null && slot.getHasStack())
		{
			ItemStack itemstack1 = slot.getStack();
			itemstack = itemstack1.copy();
			if (par2 == 1)
			{
				if (!this.mergeItemStack(itemstack1, 2, 38, true))
				{
					this.wheel.onInventoryChanged();
					return null;
				}
				slot.onSlotChange(itemstack1, itemstack);
			}
			else if (par2 != 0)
			{
				if (SpinningWheelRecipe.getRecipe(itemstack1) != null && SpinningWheelRecipe.getRecipe(itemstack1).getOutput(itemstack1) != null)
				{
					if (!this.mergeItemStack(itemstack1, 0, 1, false))
					{
						this.wheel.onInventoryChanged();
						return null;
					}
				}
				else if (par2 >= 2 && par2 < 29)
				{
					if (!this.mergeItemStack(itemstack1, 29, 38, false))
					{
						this.wheel.onInventoryChanged();
						return null;
					}
				}
				else if (par2 >= 29 && par2 < 38 && !this.mergeItemStack(itemstack1, 2, 29, false))
				{
					this.wheel.onInventoryChanged();
					return null;
				}
			}
			else if (!this.mergeItemStack(itemstack1, 2, 38, false))
			{
				this.wheel.onInventoryChanged();
				return null;
			}
			if (itemstack1.stackSize == 0)
			{
				slot.putStack((ItemStack) null);
			}
			else
			{
				slot.onSlotChanged();
			}
			if (itemstack1.stackSize == itemstack.stackSize)
			{
				this.wheel.onInventoryChanged();
				return null;
			}
			slot.onPickupFromSlot(par1EntityPlayer, itemstack1);
		}
		this.wheel.onInventoryChanged();
		return itemstack;
	}
}
