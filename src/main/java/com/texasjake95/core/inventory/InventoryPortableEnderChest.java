package com.texasjake95.core.inventory;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.InventoryBasic;
import net.minecraft.inventory.InventoryEnderChest;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;

public class InventoryPortableEnderChest extends InventoryBasic {
	
	private InventoryEnderChest inventoryEnderChest;
	
	public InventoryPortableEnderChest(InventoryEnderChest inventoryEnderChest)
	{
		super("container.enderchest", false, 27);
		this.inventoryEnderChest = inventoryEnderChest;
	}
	
	public void loadInventoryFromNBT(NBTTagList par1NBTTagList)
	{
		this.inventoryEnderChest.loadInventoryFromNBT(par1NBTTagList);
		int i;
		for (i = 0; i < this.getSizeInventory(); ++i)
		{
			this.setInventorySlotContents(i, (ItemStack) null);
		}
		for (i = 0; i < par1NBTTagList.tagCount(); ++i)
		{
			NBTTagCompound nbttagcompound = (NBTTagCompound) par1NBTTagList.tagAt(i);
			int j = nbttagcompound.getByte("Slot") & 255;
			if (j >= 0 && j < this.getSizeInventory())
			{
				this.setInventorySlotContents(j, ItemStack.loadItemStackFromNBT(nbttagcompound));
			}
		}
		for (int slot = 0; slot < this.getSizeInventory(); slot++)
			this.setInventorySlotContents(slot, this.inventoryEnderChest.getStackInSlot(slot));
		this.saveInventoryToNBT();
	}
	
	public NBTTagList saveInventoryToNBT()
	{
		for (int slot = 0; slot < this.inventoryEnderChest.getSizeInventory(); slot++)
			this.inventoryEnderChest.setInventorySlotContents(slot, this.getStackInSlot(slot));
		NBTTagList nbttaglist = new NBTTagList("EnderItems");
		for (int i = 0; i < this.getSizeInventory(); ++i)
		{
			ItemStack itemstack = this.getStackInSlot(i);
			if (itemstack != null)
			{
				NBTTagCompound nbttagcompound = new NBTTagCompound();
				nbttagcompound.setByte("Slot", (byte) i);
				itemstack.writeToNBT(nbttagcompound);
				nbttaglist.appendTag(nbttagcompound);
			}
		}
		return this.inventoryEnderChest.saveInventoryToNBT();
	}
	
	public boolean isUseableByPlayer(EntityPlayer par1EntityPlayer)
	{
		return true;
	}
}
