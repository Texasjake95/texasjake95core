package com.texasjake95.core;

import java.util.HashMap;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class ToolHandlerRegistry implements IToolHandler, IToolRegistry {
	
	private static final IToolHandler DEFAULT = new VanillaToolHandler();
	private HashMap<Integer, Boolean> damageMatters = new HashMap<Integer, Boolean>();
	private HashMap<Integer, HashMap<Integer, IToolHandler>> nonDamageableHandler = new HashMap<Integer, HashMap<Integer, IToolHandler>>();
	private HashMap<Integer, IToolHandler> damageableHandler = new HashMap<Integer, IToolHandler>();
	private static ToolHandlerRegistry instance = null;
	
	public static ToolHandlerRegistry getInstance()
	{
		if (instance == null)
			instance = new ToolHandlerRegistry();
		return instance;
	}
	
	private ToolHandlerRegistry()
	{
		registerToolHandler(DEFAULT, new ItemStack(Item.pickaxeDiamond), false);
		registerToolHandler(DEFAULT, new ItemStack(Item.pickaxeGold), false);
		registerToolHandler(DEFAULT, new ItemStack(Item.pickaxeIron), false);
		registerToolHandler(DEFAULT, new ItemStack(Item.pickaxeStone), false);
		registerToolHandler(DEFAULT, new ItemStack(Item.pickaxeWood), false);
		registerToolHandler(DEFAULT, new ItemStack(Item.shovelDiamond), false);
		registerToolHandler(DEFAULT, new ItemStack(Item.shovelGold), false);
		registerToolHandler(DEFAULT, new ItemStack(Item.shovelIron), false);
		registerToolHandler(DEFAULT, new ItemStack(Item.shovelStone), false);
		registerToolHandler(DEFAULT, new ItemStack(Item.shovelWood), false);
		registerToolHandler(DEFAULT, new ItemStack(Item.axeDiamond), false);
		registerToolHandler(DEFAULT, new ItemStack(Item.axeGold), false);
		registerToolHandler(DEFAULT, new ItemStack(Item.axeIron), false);
		registerToolHandler(DEFAULT, new ItemStack(Item.axeStone), false);
		registerToolHandler(DEFAULT, new ItemStack(Item.axeWood), false);
	}
	
	@Override
	public void registerToolHandler(IToolHandler handler, ItemStack stack, boolean damageMatters)
	{
		this.registerToolHandler(handler, stack.itemID, stack.getItemDamage(), damageMatters);
	}
	
	@Override
	public void registerToolHandler(IToolHandler handler, int itemID, int itemMeta, boolean damageMatters)
	{
		if (Texasjake95Core.isTesting)
			System.out.println(String.format("Registering handler for %s:%s damageMatters = %s", itemID, itemMeta, damageMatters));
		this.damageMatters.put(itemID, damageMatters);
		if (damageMatters)
		{
			if (Texasjake95Core.isTesting)
				System.out.println(String.format("Registering handler for %s:%s as a nondamageable", itemID, itemMeta));
			HashMap<Integer, IToolHandler> metaHandlers = nonDamageableHandler.get(itemID);
			if (metaHandlers == null)
				metaHandlers = new HashMap<Integer, IToolHandler>();
			metaHandlers.put(itemMeta, handler);
			nonDamageableHandler.put(itemID, metaHandlers);
		}
		else
		{
			if (Texasjake95Core.isTesting)
				System.out.println(String.format("Registering handler for %s:%s as a damageable", itemID, itemMeta));
			damageableHandler.put(itemID, handler);
		}
	}
	
	@Override
	public IToolHandler getHandler(ItemStack stack)
	{
		return this.getHandler(stack.itemID, stack.getItemDamage());
	}
	
	@Override
	public IToolHandler getHandler(int itemID, int itemMeta)
	{
		if (Texasjake95Core.isTesting)
			System.out.println(itemID + " " + this.damageMatters.containsKey(itemID));
		if (this.damageMatters.containsKey(itemID))
		{
			if (this.damageMatters.get(itemID))
			{
				if (this.nonDamageableHandler.containsKey(itemID) && this.nonDamageableHandler.get(itemID) != null)
				{
					HashMap<Integer, IToolHandler> metaHandlers = nonDamageableHandler.get(itemID);
					if (metaHandlers.containsKey(itemMeta))
						return metaHandlers.get(itemMeta);
					else
					{
						if (Texasjake95Core.isTesting)
							System.out.println("Using vanilla - couldn't find nonDamageable Handler(meta)");
						return DEFAULT;
					}
				}
				else
				{
					if (Texasjake95Core.isTesting)
						System.out.println("Using vanilla - couldn't find nonDamageable Handler(id)");
					return DEFAULT;
				}
			}
			else
			{
				if (this.damageableHandler.containsKey(itemID) && this.damageableHandler.get(itemID) != null)
					return this.damageableHandler.get(itemID);
				else
				{
					if (Texasjake95Core.isTesting)
						System.out.println("Using vanilla - couldn't find damageable Handler");
					return DEFAULT;
				}
			}
		}
		else
		{
			if (Texasjake95Core.isTesting)
				System.out.println("Using vanilla - couldn't find damageMatters");
			return DEFAULT;
		}
	}
	
	@Override
	public boolean canHarvest(int blockID, int blockMeta, ItemStack stack)
	{
		if (stack == null)
		{
			return Block.blocksList[blockID].blockHardness <= 1.0F;
		}
		return getHandler(stack).canHarvest(blockID, blockMeta, stack);
	}
	
	@Override
	public boolean canAutoSwtichTo(ItemStack stack)
	{
		if (stack == null)
			return true;
		return getHandler(stack).canAutoSwtichTo(stack);
	}
	
	@Override
	public boolean isDamageable(ItemStack stack)
	{
		if (stack == null)
			return false;
		return getHandler(stack).isDamageable(stack);
	}
	
	@Override
	public double getDurability(ItemStack stack)
	{
		if (stack == null)
			return 1.0F;
		return getHandler(stack).getDurability(stack);
	}
}
