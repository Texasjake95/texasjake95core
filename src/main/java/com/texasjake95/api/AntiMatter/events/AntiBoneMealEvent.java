package com.texasjake95.api.AntiMatter.events;

import net.minecraftforge.event.Cancelable;
import net.minecraftforge.event.Event;
import net.minecraftforge.event.entity.player.PlayerEvent;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;

@Cancelable
@Event.HasResult
public class AntiBoneMealEvent extends PlayerEvent {
	
	public final World world;
	public final int ID;
	public final int X;
	public final int Y;
	public final int Z;
	
	public AntiBoneMealEvent(EntityPlayer player, World world, int id, int x, int y, int z)
	{
		super(player);
		this.world = world;
		this.ID = id;
		this.X = x;
		this.Y = y;
		this.Z = z;
	}
}