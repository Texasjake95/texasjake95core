package com.texasjake95.api.AntiMatter;

import com.texasjake95.api.AntiMatter.interfaces.IAntiBlock;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public abstract class AntiBlockContainer_API extends BlockContainer implements IAntiBlock {
	
	protected AntiBlockContainer_API(int par1, int par2, Material par3Material)
	{
		super(par1, par3Material);
		// this.RegisterBlock();
	}
	
	@Override
	public TileEntity createNewTileEntity(World var1)
	{
		return null;
	}
	
	@Override
	public void onBlockPlacedBy(World world, int par2, int par3, int par4, EntityLivingBase par5EntityLivingBase, ItemStack par6ItemStack)
	{
		BaseAntiFunctions.antiblockfunctions.onBlockPlacedBy(world, this.blockID, par2, par3, par4, par5EntityLivingBase);
	}
	
	@Override
	public void onNeighborBlockChange(World par1World, int par2, int par3, int par4, int par5)
	{
		BaseAntiFunctions.antiblockfunctions.onNeighborBlockChange(par1World, this.blockID, par2, par3, par4, par5);
	}
	
	@Override
	public abstract int OppositeID(int meta);
	
	@Override
	public abstract int OppositeMeta(int meta);
	
	@Override
	public abstract int MaxBlockMeta();
	
	// @Override
	public void RegisterBlock()
	{
		BaseAntiFunctions.antiblockfunctions.RegisterBlock(this.blockID);
	}
	
	@Override
	public abstract boolean hasAntiRecipe(int meta);
	
	@Override
	public abstract boolean canSwitch(int meta);
}
