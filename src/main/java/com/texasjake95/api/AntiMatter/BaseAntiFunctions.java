package com.texasjake95.api.AntiMatter;

import com.texasjake95.api.AntiMatter.interfaces.IAntiBaseBlock;
import com.texasjake95.api.AntiMatter.interfaces.IAntiBaseItem;

public class BaseAntiFunctions {
	
	/**
	 * This is used by Anti Blocks and contains the functions used by them It is
	 * defined by the Antimatter Mod upon its load
	 */
	public static IAntiBaseBlock antiblockfunctions;
	/**
	 * This is used by Anti Items and contains the functions used by them It is
	 * defined by the Antimatter Mod upon its load
	 */
	public static IAntiBaseItem antiitemfunctions;
}
