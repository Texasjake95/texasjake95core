package com.texasjake95.api.AntiMatter.interfaces;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.world.World;

/**
 * This interface should not be used by modders other than myself, Texasjake95.
 * Please use the BaseAntiFunctions.antiblockfunctions. It will be defined by
 * the AntiMatter Mod upon load.
 * 
 * @author Texasjake95
 */
public interface IAntiBaseBlock {
	
	/**
	 * Please put this in the function onBlockPlacedBy by putting
	 * BaseAntiFunctions.antiblockfunctions.onBlockPlacedBy(par1World,
	 * this.blockID, par2, par3, par4, par5EntityLiving); in the body.
	 * 
	 * @param par1World
	 *            World
	 * @param id
	 *            Block ID
	 * @param par2
	 *            X coordinate
	 * @param par3
	 *            Y coordinate
	 * @param par4
	 *            Z coordinate
	 * @param par5EntityLiving
	 *            Entity Placing Block
	 */
	public void onBlockPlacedBy(World par1World, int id, int par2, int par3, int par4, EntityLivingBase par5EntityLiving);
	
	/**
	 * Please put this in the function onBlockPlacedBy by putting
	 * BaseAntiFunctions.antiblockfunctions.onNeighborBlockChange(par1World,
	 * this.blockID, par2, par3, par4, par5); in the body.
	 * 
	 * @param par1World
	 *            World
	 * @param id
	 *            Block ID
	 * @param par2
	 *            X coordinate
	 * @param par3
	 *            Y coordinate
	 * @param par4
	 *            Z coordinate
	 * @param par5
	 *            Block that changed id
	 */
	public void onNeighborBlockChange(World par1World, int id, int par2, int par3, int par4, int par5);
	
	/**
	 * Please put this in the RegisterBlock function from {@link IAntiBlock} by
	 * putting BaseAntiFunctions.antiblockfunctions.RegisterBlock(this.blockID)
	 * in the body.
	 * 
	 * @param blockid
	 *            The blockID of the Block being registered
	 */
	public void RegisterBlock(int blockid);
}
