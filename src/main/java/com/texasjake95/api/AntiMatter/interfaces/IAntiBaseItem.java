package com.texasjake95.api.AntiMatter.interfaces;

/**
 * This interface should not be used by modders other than myself, Texasjake95.
 * Please use the BaseAntiFunctions.antiitemfunctions. It will be defined by the
 * AntiMatter Mod upon load.
 * 
 * @author Texasjake95
 */
public interface IAntiBaseItem {
	
	/**
	 * Please put this in the RegisterItem function from {@link IAntiItem} by
	 * putting
	 * BaseAntiFunctions.antiitemfunctions.RegisterItem(this.shiftedIndex) in
	 * the body.
	 * 
	 * @param itemID
	 *            The itemID of the Item being registered
	 */
	public void RegisterItem(int itemid);
}
