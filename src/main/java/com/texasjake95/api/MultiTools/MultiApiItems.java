package com.texasjake95.api.MultiTools;

import com.texasjake95.api.RegisteredBlocks;
import com.texasjake95.api.RegisteredItemStacks;
import com.texasjake95.api.RegisteredItems;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class MultiApiItems {
	
	public static final Item PAWood = getItem("Wooden Paxe");
	public static final Item PAStone = getItem("Stone Paxe");
	public static final Item PAIron = getItem("Iron Paxe");
	public static final Item PADiamond = getItem("Diamond Paxe");
	public static final Item PAGold = getItem("Gold Paxe");
	public static final Item PSWood = getItem("Wooden Picvel");
	public static final Item PSStone = getItem("Stone Picvel");
	public static final Item PSIron = getItem("Iron Picvel");
	public static final Item PSDiamond = getItem("Diamond Picvel");
	public static final Item PSGold = getItem("Gold Picvel");
	public static final Item PSAWood = getItem("Wooden Paxel");
	public static final Item PSAStone = getItem("Stone Paxel");
	public static final Item PSAIron = getItem("Iron Paxel");
	public static final Item PSADiamond = getItem("Diamond Paxel");
	public static final Item PSAGold = getItem("Gold Paxel");
	public static final Item PSSwWood = getItem("Wooden Piverd");
	public static final Item PSSwStone = getItem("Stone Piverd");
	public static final Item PSSwIron = getItem("Iron Piverd");
	public static final Item PSSwDiamond = getItem("Diamond Piverd");
	public static final Item PSSwGold = getItem("Gold Piverd");
	public static final Item PSwWood = getItem("Wooden Picord");
	public static final Item PSwStone = getItem("Stone Picord");
	public static final Item PSwIron = getItem("Iron Picord");
	public static final Item PSwDiamond = getItem("Diamond Picord");
	public static final Item PSwGold = getItem("Gold Picord");
	public static final Item PSwAWood = getItem("Wooden Paxerd");
	public static final Item PSwAStone = getItem("Stone Paxerd");
	public static final Item PSwAIron = getItem("Iron Paxerd");
	public static final Item PSwADiamond = getItem("Diamond Paxerd");
	public static final Item PSwAGold = getItem("Gold Paxerd");
	public static final Item SAWood = getItem("Wooden Shaxe");
	public static final Item SAStone = getItem("Stone Shaxe");
	public static final Item SAIron = getItem("Iron Shaxe");
	public static final Item SADiamond = getItem("Diamond Shaxe");
	public static final Item SAGold = getItem("Gold Shaxe");
	public static final Item SSwWood = getItem("Wooden Shord");
	public static final Item SSwStone = getItem("Stone Shord");
	public static final Item SSwIron = getItem("Iron Shord");
	public static final Item SSwDiamond = getItem("Diamond Shord");
	public static final Item SSwGold = getItem("Gold Shord");
	public static final Item SSwAWood = getItem("Wooden Swaxel");
	public static final Item SSwAStone = getItem("Stone Swaxel");
	public static final Item SSwAIron = getItem("Iron Swaxel");
	public static final Item SSwADiamond = getItem("Diamond Swaxel");
	public static final Item SSwAGold = getItem("Gold Swaxel");
	public static final Item SwAWood = getItem("Wooden Swaxe");
	public static final Item SwAStone = getItem("Stone Swaxe");
	public static final Item SwAIron = getItem("Iron Swaxe");
	public static final Item SwADiamond = getItem("Diamond Swaxe");
	public static final Item SwAGold = getItem("Gold Swaxe");
	public static final Item MultiWood = getItem("Wooden Multi Tool");
	public static final Item MultiStone = getItem("Stone Multi Tool");
	public static final Item MultiIron = getItem("Iron Multi Tool");
	public static final Item MultiDiamond = getItem("Diamond Multi Tool");
	public static final Item MultiGold = getItem("Gold Multi Tool");
	
	@SuppressWarnings("unused")
	private static final Block getBlock(String tag)
	{
		return RegisteredBlocks.getBlock(multitools, tag);
	}
	
	private static final Item getItem(String tag)
	{
		return RegisteredItems.getItem(multitools, tag);
	}
	
	public static final ItemStack getItemStack(String tag, int amount, int meta)
	{
		return RegisteredItemStacks.getItemStack(multitools, tag, amount, meta);
	}
	
	private static final String multitools = "MultiTools";
}
