package com.texasjake95.api.Lantern;

import com.texasjake95.api.RegisteredBlocks;
import com.texasjake95.api.RegisteredItemStacks;
import com.texasjake95.api.RegisteredItems;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class LanternApiItems {
	
	public static final Item LanternRing = getItem("Lantern Ring");
	public static final Item Misc = getItem("Ring");
	
	@SuppressWarnings("unused")
	private static final Block getBlock(String tag)
	{
		return RegisteredBlocks.getBlock(lantern, tag);
	}
	
	private static final Item getItem(String tag)
	{
		return RegisteredItems.getItem(lantern, tag);
	}
	
	public static final ItemStack getItemStack(String tag, int amount, int meta)
	{
		return RegisteredItemStacks.getItemStack(lantern, tag, amount, meta);
	}
	
	private static final String lantern = "Lantern";
}
