package com.texasjake95.api;

import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;

import cpw.mods.fml.common.FMLLog;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public final class RegisteredItems {
	
	private static final Map<String, Item> Items = new TreeMap<String, Item>();
	
	/**
	 * This will return null unless it is after the Mod's Init Only if Mod is
	 * created by: Texasjake95
	 * 
	 */
	public static Item getItem(String ModID, String tag)
	{
		Item item = Items.get(ModID + "." + tag);
		if (item == null)
		{
			FMLLog.getLogger().log(Level.WARNING, "[Texasjake95 Core] Unable to get Item " + ModID + "." + tag);
		}
		return item;
	}
	
	public static void registerItem(String ModID, String tag, Item item)
	{
		Item Item = Items.get(ModID + "." + tag);
		ItemStack itemstack = RegisteredItemStacks.getItemStacks().get(ModID + "." + tag);
		if (Item == null && itemstack == null)
		{
			Items.put(ModID + "." + tag, item);
			RegisteredItemStacks.registerItemStack(ModID, tag, new ItemStack(item));
		}
		else
		{
			FMLLog.getLogger().log(Level.SEVERE, "[Texasjake95Core] Item with tag " + ModID + "." + tag + " already exists");
			throw new IllegalArgumentException();
		}
	}
	
	public static void printItemTags()
	{
		System.out.println();
		System.out.println("Printing all registered Items:");
		for (String tag : Items.keySet())
		{
			System.out.println(tag);
		}
		System.out.println();
	}
	
	public static Map<String, Item> getItems()
	{
		return Items;
	}
}
