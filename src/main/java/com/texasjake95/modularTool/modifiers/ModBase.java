package com.texasjake95.modularTool.modifiers;

import com.texasjake95.base.helpers.NBTHelper;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

public class ModBase {
	
	private String nbtName;
	
	public ModBase(String nbtName)
	{
		this.nbtName = nbtName;
	}
	
	public ModBase()
	{
		this.nbtName = "";
	}
	
	public NBTTagCompound getBaseTags(ItemStack stack)
	{
		return NBTHelper.getNBTTags(stack, BASE);
	}
	
	public String getNbtName()
	{
		return nbtName;
	}
	
	public static final String BASE = "TX_MOD_TOOL";
	public static final String EFFBLOCK = "EFFBLOCK";
	public static final String BROKEN = "broken";
}
