package com.texasjake95.modularTool.modifiers;

import com.texasjake95.base.helpers.NBTHelper;

import net.minecraftforge.common.MinecraftForge;

import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;

public class ModTool extends ModBase {
	
	public ModTool(String nbtName)
	{
		super(nbtName);
	}
	
	public String getToolBoolName()
	{
		return this.getNbtName() + ".tool";
	}
	
	public String getToolHarName()
	{
		return this.getNbtName() + ".harvest";
	}
	
	public String getToolEffName()
	{
		return this.getNbtName() + ".efficiency";
	}
	
	public int getToolHarvestLvl(ItemStack stack)
	{
		return NBTHelper.getInt(this.getBaseTags(stack), this.getToolHarName());
	}
	
	public float getToolEfficiency(ItemStack stack)
	{
		return NBTHelper.getFloat(this.getBaseTags(stack), this.getToolEffName());
	}
	
	public void setToolHarvestLvl(ItemStack stack, int lvl)
	{
		NBTHelper.setInteger(this.getBaseTags(stack), this.getToolHarName(), lvl);
	}
	
	public void setToolEfficiency(ItemStack stack, float lvl)
	{
		NBTHelper.setFloat(this.getBaseTags(stack), this.getToolEffName(), lvl);
	}
	
	public boolean canBlockBeHarvested(Block block, int meta, ItemStack stack)
	{
		int harlvl = NBTHelper.getInt(this.getBaseTags(stack), this.getToolHarName());
		int blockhar = MinecraftForge.getBlockHarvestLevel(block, meta, this.getNbtName());
		return isTool(stack) ? harlvl >= blockhar : false;
	}
	
	public boolean isTool(ItemStack stack)
	{
		return NBTHelper.getBoolean(this.getBaseTags(stack), this.getToolBoolName());
	}
	
	public void setIsTool(ItemStack stack, boolean isTool)
	{
		NBTHelper.setBoolean(this.getBaseTags(stack), this.getToolBoolName(), isTool);
	}
	
	public void initTool(ItemStack stack)
	{
		this.setIsTool(stack, false);
		this.setToolEfficiency(stack, 0);
		this.setToolHarvestLvl(stack, 0);
	}
}
