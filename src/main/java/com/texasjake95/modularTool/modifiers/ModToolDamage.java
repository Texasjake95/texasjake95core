package com.texasjake95.modularTool.modifiers;

import com.texasjake95.base.helpers.NBTHelper;

import net.minecraft.item.ItemStack;

public class ModToolDamage extends ModBase {
	
	public ModToolDamage()
	{
		super("damage");
	}
	
	public String getMaxDamageName()
	{
		return this.getNbtName() + ".max";
	}
	
	public String getDamageName()
	{
		return this.getNbtName() + ".current";
	}
	
	public String getBrokenName()
	{
		return this.getNbtName() + ".broken";
	}
	
	public void setMaxDamage(ItemStack stack, int max)
	{
		NBTHelper.setInteger(this.getBaseTags(stack), this.getMaxDamageName(), max);
	}
	
	public void setDamage(ItemStack stack, int max)
	{
		NBTHelper.setInteger(this.getBaseTags(stack), this.getDamageName(), max);
	}
	
	public void setIsBroken(ItemStack stack, boolean isBroken)
	{
		NBTHelper.setBoolean(this.getBaseTags(stack), this.getBrokenName(), isBroken);
	}
	
	public int getMaxDamage(ItemStack stack)
	{
		return NBTHelper.getInt(this.getBaseTags(stack), this.getMaxDamageName());
	}
	
	public int getDamage(ItemStack stack)
	{
		return NBTHelper.getInt(this.getBaseTags(stack), this.getDamageName());
	}
	
	public boolean getIsBroken(ItemStack stack)
	{
		return NBTHelper.getBoolean(this.getBaseTags(stack), this.getBrokenName());
	}
	
	public void initDamage(ItemStack stack)
	{
		this.setMaxDamage(stack, 50);
		this.setDamage(stack, 0);
		this.setIsBroken(stack, false);
	}
}
