package com.texasjake95.modularTool.modifiers.enchant;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.item.ItemStack;

public class ModSilkTouch extends ModEnchant {
	
	public ModSilkTouch()
	{
		super("silkTouch", Enchantment.silkTouch.getMaxLevel(), EnumEnchantType.Tool);
	}
	
	@Override
	public boolean canHaveEnchant(ItemStack stack)
	{
		return !new ModFortune().hasEnchant(stack) && super.canHaveEnchant(stack);
	}
}
