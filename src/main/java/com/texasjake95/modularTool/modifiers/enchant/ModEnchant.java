package com.texasjake95.modularTool.modifiers.enchant;

import com.texasjake95.base.helpers.NBTHelper;
import com.texasjake95.modularTool.modifiers.ModBase;
import com.texasjake95.modularTool.modifiers.ModTool;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

public class ModEnchant extends ModBase {
	
	private int MaxLvl;
	private EnumEnchantType enchantType;
	
	public ModEnchant(String nbtName, int MaxLvl, EnumEnchantType type)
	{
		super(nbtName);
		this.MaxLvl = MaxLvl;
		this.enchantType = type;
	}
	
	public String getEnchLvlName()
	{
		return this.getNbtName() + ".lvl";
	}
	
	public NBTTagCompound getEnchTags(ItemStack stack)
	{
		return NBTHelper.getNBTTags(this.getBaseTags(stack), ENCHANT);
	}
	
	public int getEnchantmentLvl(ItemStack stack)
	{
		return NBTHelper.getInt(this.getEnchTags(stack), this.getEnchLvlName());
	}
	
	public void setEnchantmentLvl(ItemStack stack, int lvl)
	{
		NBTHelper.setInteger(this.getEnchTags(stack), this.getEnchLvlName(), lvl > this.MaxLvl ? this.MaxLvl : lvl);
	}
	
	public boolean hasEnchant(ItemStack stack)
	{
		return getEnchantmentLvl(stack) > 0;
	}
	
	public boolean canHaveEnchant(ItemStack stack)
	{
		switch (this.enchantType)
		{
			case Tool:
				return new ModTool("pickaxe").isTool(stack) || new ModTool("shovel").isTool(stack) || new ModTool("axe").isTool(stack);
			case Sword:
				return false;
			case Both:
				return true;
		}
		return false;
	}
	
	private static final String ENCHANT = "enchants";
	
	public enum EnumEnchantType
	{
		Tool(), Sword(), Both();
	}
}
