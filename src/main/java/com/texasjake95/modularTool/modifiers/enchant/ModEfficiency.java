package com.texasjake95.modularTool.modifiers.enchant;

import net.minecraft.enchantment.Enchantment;

public class ModEfficiency extends ModEnchant {
	
	public ModEfficiency()
	{
		super("efficiency", Enchantment.efficiency.getMaxLevel(), EnumEnchantType.Tool);
	}
}
