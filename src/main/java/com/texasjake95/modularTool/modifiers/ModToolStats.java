package com.texasjake95.modularTool.modifiers;

import java.util.ArrayList;

import com.texasjake95.base.helpers.NBTHelper;
import com.texasjake95.commons.util.IntPair;
import com.texasjake95.modularTool.modifiers.enchant.ModEfficiency;

import net.minecraftforge.common.MinecraftForge;

import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;

public class ModToolStats {
	
	private ItemStack stack;
	
	public ModToolStats(ItemStack stack)
	{
		this.stack = stack;
	}
	
	public boolean isBroken()
	{
		return NBTHelper.getBoolean(new ModBase().getBaseTags(stack), NBT_TOOL_TAG_BROKEN);
	}
	
	public void setBroken(boolean isBroken)
	{
		NBTHelper.setBoolean(new ModBase().getBaseTags(stack), NBT_TOOL_TAG_BROKEN, isBroken);
	}
	
	public int getHarvestLvl(String tool)
	{
		return new ModTool(tool).getToolHarvestLvl(stack);
	}
	
	public void setHarvestLvl(String tool, int lvl)
	{
		new ModTool(tool).setToolHarvestLvl(stack, lvl);
	}
	
	public boolean isTool(String tool)
	{
		return new ModTool(tool).isTool(stack);
	}
	
	public void setIsTool(String tool)
	{
		new ModTool(tool).setIsTool(stack, true);
	}
	
	public boolean canBlockBeHarvested(Block block, String toolType)
	{
		return this.canBlockBeHarvested(block, 0, toolType);
	}
	
	public boolean canBlockBeHarvested(Block block, int meta, String toolType)
	{
		return new ModTool(toolType).canBlockBeHarvested(block, meta, stack);
	}
	
	public ToolType getBestToolType(Block block, int meta)
	{
		ModTool pickMod = new ModTool("pickaxe");
		ModTool shovelMod = new ModTool("shovel");
		ModTool axeMod = new ModTool("axe");
		boolean isPick = isTool("pickaxe");
		boolean isShovel = isTool("shovel");
		boolean isAxe = isTool("axe");
		ToolType bestTool = new ToolType("", 1);
		if (isPick && pickMod.getToolEfficiency(stack) > bestTool.power && pickMod.canBlockBeHarvested(block, meta, stack))
		{
			bestTool = new ToolType(TOOL_TYPES[0], pickMod.getToolEfficiency(stack));
		}
		if (isShovel && shovelMod.getToolEfficiency(stack) > bestTool.power && shovelMod.canBlockBeHarvested(block, meta, stack))
		{
			bestTool = new ToolType(TOOL_TYPES[2], shovelMod.getToolEfficiency(stack));
		}
		if (isAxe && axeMod.getToolEfficiency(stack) > bestTool.power && axeMod.canBlockBeHarvested(block, meta, stack))
		{
			bestTool = new ToolType(TOOL_TYPES[1], axeMod.getToolEfficiency(stack));
		}
		return bestTool;
	}
	
	public float getStrVsBlock(Block block, int meta)
	{
		ToolType bestTool = getBestToolType(block, meta);
		if (this.isBroken())
		{
			return 0.1f;
		}
		if (bestTool.toolType.equalsIgnoreCase(""))
		{
			return 1.0f;
		}
		ModTool BMT = new ModTool(bestTool.toolType);
		if (getBestList(bestTool.toolType).contains(new IntPair(block.blockID, meta)))
		{
			if (BMT.canBlockBeHarvested(block, meta, stack))
			{
				float f = BMT.getToolEfficiency(stack);
				int i = new ModEfficiency().getEnchantmentLvl(stack);
				if (i > 0)
				{
					float f1 = (float) (i * i + 1);
					boolean canHarvest = this.canBlockBeHarvested(block, meta, bestTool.toolType);
					if (!canHarvest && f <= 1.0F)
					{
						f += f1 * 0.08F;
					}
					else
					{
						f += f1;
					}
				}
				return f;
			}
		}
		return 1.0F;
	}
	
	public ItemStack returnItemStack()
	{
		return this.stack.copy();
	}
	
	public static void initStack(ItemStack stack)
	{
		new ModTool("pickaxe").initTool(stack);
		new ModTool("shovel").initTool(stack);
		new ModTool("axe").initTool(stack);
		new ModToolDamage().initDamage(stack);
	}
	
	public static ArrayList<IntPair> getBestList(String tool)
	{
		if (tool.equals("pickaxe"))
			return pickBlocks();
		if (tool.equals("axe"))
			return axeBlocks();
		if (tool.equals("shovel"))
			return shovelBlocks();
		return null;
	}
	
	public static ArrayList<IntPair> shovelBlocks()
	{
		if (shovelList.isEmpty())
		{
			ArrayList<IntPair> valid = new ArrayList<IntPair>();
			ArrayList<ItemStack> subItems = new ArrayList<ItemStack>();
			for (int id = 0; id < Block.blocksList.length; id++)
			{
				Block block = Block.blocksList[id];
				subItems.clear();
				block.getSubBlocks(id, block.getCreativeTabToDisplayOn(), subItems);
				for (ItemStack blockstack : subItems)
				{
					if (blockstack != null)
					{
						if (MinecraftForge.getBlockHarvestLevel(block, blockstack.getItemDamage(), "shovel") > -1)
						{
							valid.add(new IntPair(id, blockstack.getItemDamage()));
						}
					}
				}
			}
			shovelList.addAll(valid);
		}
		return shovelList;
	}
	
	public static ArrayList<IntPair> pickBlocks()
	{
		if (pickList.isEmpty())
		{
			ArrayList<IntPair> valid = new ArrayList<IntPair>();
			ArrayList<ItemStack> subItems = new ArrayList<ItemStack>();
			for (int id = 0; id < Block.blocksList.length; id++)
			{
				Block block = Block.blocksList[id];
				subItems.clear();
				block.getSubBlocks(id, block.getCreativeTabToDisplayOn(), subItems);
				for (ItemStack blockstack : subItems)
				{
					if (blockstack != null)
					{
						if (MinecraftForge.getBlockHarvestLevel(block, blockstack.getItemDamage(), "pickaxe") > -1)
						{
							valid.add(new IntPair(id, blockstack.getItemDamage()));
						}
					}
				}
			}
			pickList.addAll(valid);
		}
		return pickList;
	}
	
	public static ArrayList<IntPair> axeBlocks()
	{
		if (axeList.isEmpty())
		{
			ArrayList<IntPair> valid = new ArrayList<IntPair>();
			ArrayList<ItemStack> subItems = new ArrayList<ItemStack>();
			for (int id = 0; id < Block.blocksList.length; id++)
			{
				Block block = Block.blocksList[id];
				subItems.clear();
				block.getSubBlocks(id, block.getCreativeTabToDisplayOn(), subItems);
				for (ItemStack blockstack : subItems)
				{
					if (blockstack != null)
					{
						if (MinecraftForge.getBlockHarvestLevel(block, blockstack.getItemDamage(), "pickaxe") > -1)
						{
							valid.add(new IntPair(id, blockstack.getItemDamage()));
						}
					}
				}
			}
			axeList.addAll(valid);
		}
		return axeList;
	}
	
	private static final ArrayList<IntPair> shovelList = new ArrayList<IntPair>();
	private static final ArrayList<IntPair> pickList = new ArrayList<IntPair>();
	private static final ArrayList<IntPair> axeList = new ArrayList<IntPair>();
	static final String NBT_TOOL_TAG_BASE = "TX_MOD_TOOL";
	static final String NBT_TOOL_TAG_EFFBLOCK = "EFFBLOCK";
	static final String NBT_TOOL_TAG_BROKEN = "broken";
	private static final String[] TOOL_TYPES = { "pickaxe", "axe", "shovel" };
	
	public class ToolType {
		
		public String toolType;
		public float power;
		
		public ToolType(String type, float power)
		{
			this.toolType = type;
			this.power = power;
		}
	}
}
