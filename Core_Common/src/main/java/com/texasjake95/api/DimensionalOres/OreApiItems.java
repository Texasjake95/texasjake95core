package com.texasjake95.api.DimensionalOres;

import com.texasjake95.api.RegisteredBlocks;
import com.texasjake95.api.RegisteredItemStacks;
import com.texasjake95.api.RegisteredItems;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class OreApiItems {
	
	public static final Block netherBlock = getBlock("Nether Ores");
	/*
	 * Coal 0 Iron 1 Gold 2 Redstone 3 Diamond 4 Lapis 5 Saltpeter 6 Sulfur 7
	 * Uranium 8 Apatite 9 Copper 10 Tin 11 Emerald 12 Silver 13
	 */
	public static final Item Ingots = getItem("Ingots");
	
	/*
	 * Copper 0 Tin 1 Silver 2 Uranium 3
	 */
	private static final Block getBlock(String tag)
	{
		return RegisteredBlocks.getBlock(ores, tag);
	}
	
	private static final Item getItem(String tag)
	{
		return RegisteredItems.getItem(ores, tag);
	}
	
	public static final ItemStack getItemStack(String tag, int amount, int meta)
	{
		return RegisteredItemStacks.getItemStack(ores, tag, amount, meta);
	}
	
	private static final String ores = "Ores";
}
