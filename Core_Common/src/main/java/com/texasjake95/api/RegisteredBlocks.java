package com.texasjake95.api;

import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;

import cpw.mods.fml.common.FMLLog;

import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;

public final class RegisteredBlocks {
	
	private static final Map<String, Block> Blocks = new TreeMap<String, Block>();
	
	/**
	 * This will return null unless it is after the Mod's Init Only if Mod is
	 * created by: Texasjake95
	 * 
	 */
	public static Block getBlock(String ModID, String tag)
	{
		Block block = Blocks.get(ModID + "." + tag);
		if (block == null)
		{
			FMLLog.getLogger().log(Level.WARNING, "[Texasjake95 Core] Unable to get Block " + ModID + "." + tag);
		}
		return block;
	}
	
	public static void registerBlock(String ModID, String tag, Block Block)
	{
		Block block = Blocks.get(ModID + "." + tag);
		ItemStack itemstack = RegisteredItemStacks.getItemStacks().get(ModID + "." + tag);
		if (block == null && itemstack == null)
		{
			Blocks.put(ModID + "." + tag, Block);
			RegisteredItemStacks.registerItemStack(ModID, tag, new ItemStack(Block));
		}
		else
		{
			FMLLog.getLogger().log(Level.SEVERE, "[Texasjake95 Core] Block with tag " + ModID + "." + tag + " already exists");
			throw new IllegalArgumentException();
		}
	}
	
	public static void printBlockTags()
	{
		System.out.println();
		System.out.println("Printing all registered Blocks:");
		for (String tag : Blocks.keySet())
		{
			System.out.println(tag);
		}
		System.out.println();
	}
	
	/**
	 * Returns the entire mapping of Blocks.
	 * 
	 * @return
	 */
	public static Map<String, Block> getBlocks()
	{
		return Blocks;
	}
}
