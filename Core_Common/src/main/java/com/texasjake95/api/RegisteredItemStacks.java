package com.texasjake95.api;

import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;

import cpw.mods.fml.common.FMLLog;

import net.minecraft.item.ItemStack;

public class RegisteredItemStacks {
	
	private static final Map<String, ItemStack> ItemStack = new TreeMap<String, ItemStack>();
	
	/**
	 * This will return null unless it is after the Mod's Init Only if Mod is
	 * created by: Texasjake95
	 * 
	 */
	public static ItemStack getItemStack(String ModID, String tag, int amount, int meta)
	{
		ItemStack item = ItemStack.get(ModID + "." + tag);
		if (item == null)
		{
			FMLLog.getLogger().log(Level.WARNING, "[Texasjake95 Core] Unable to get ItemStack " + ModID + "." + tag);
		}
		else
		{
			item.stackSize = amount;
			item.setItemDamage(meta);
		}
		return item.copy();
	}
	
	public static void registerItemStack(String ModID, String tag, ItemStack itemStack)
	{
		ItemStack itemstack = RegisteredItemStacks.getItemStacks().get(ModID + "." + tag);
		if (itemstack == null)
		{
			ItemStack.put(ModID + "." + tag, itemStack);
		}
		else
		{
			FMLLog.getLogger().log(Level.SEVERE, "[Texasjake95Core] ItemStack with tag " + ModID + "." + tag + " already exists");
			throw new IllegalArgumentException();
		}
	}
	
	public static void printItemStackTags()
	{
		System.out.println();
		System.out.println("Printing all registered ItemStack:");
		for (String tag : ItemStack.keySet())
		{
			System.out.println(tag);
		}
		System.out.println();
	}
	
	public static Map<String, ItemStack> getItemStacks()
	{
		return ItemStack;
	}
}
