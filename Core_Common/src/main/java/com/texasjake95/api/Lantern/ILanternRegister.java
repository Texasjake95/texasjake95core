package com.texasjake95.api.Lantern;

import net.minecraft.item.Item;

public interface ILanternRegister {
	
	public void addConstruct(Item item, int level, int damage, String texture);
	
	public void addConstruct(Item item, int level, int damage, String[] texture);
}
