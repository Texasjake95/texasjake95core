package com.texasjake95.api.AntiMatter.interfaces;

/**
 * This is implemented by a Item that is to be considered an Anti Item
 * 
 * @author Texasjake95
 */
public interface IAntiItem {
	
	/**
	 * Registers the Opposite Item Id (meta sensitive)
	 */
	public int OppositeID(int meta);
	
	/**
	 * Registers the Opposite Item Metadata (meta sensitive)
	 */
	public int OppositeMeta(int meta);
	
	/**
	 * Registers the this Item's Max Metadata (meta sensitive)
	 */
	public int MaxItemMeta();
	
	/**
	 * This needs to be called after super(). This also needs to have
	 * BaseAntiFunctions.antiitemfunctions.RegisterItem(this.shiftedIndex); in
	 * its body.
	 */
	// public void RegisterItem();
	/**
	 * Determines if the block can be switched to its Opposite via crafting
	 */
	public boolean hasAntiRecipe(int meta);
}
