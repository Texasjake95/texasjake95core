package com.texasjake95.api.AntiMatter.interfaces;

/**
 * This is implemented by a Block that is to be considered an Anti Block
 * 
 * @author Texasjake95
 */
public interface IAntiBlock {
	
	/**
	 * Registers the Opposite Block Id (meta sensitive)
	 */
	public int OppositeID(int meta);
	
	/**
	 * Registers the Opposite Block Metadata (meta sensitive)
	 */
	public int OppositeMeta(int meta);
	
	/**
	 * Registers the this Block's Max Metadata (meta sensitive)
	 */
	public int MaxBlockMeta();
	
	/**
	 * This needs to be called after super(). This also needs to have
	 * BaseAntiFunctions.antiblockfunctions.RegisterBlock(this.blockID); in its
	 * body.
	 */
	// public void RegisterBlock();
	/**
	 * Determines if the block can be switched to its Opposite via crafting
	 */
	public boolean hasAntiRecipe(int meta);
	
	/**
	 * Can it be replaced with its Opposite in game (meta sensitive)
	 */
	public boolean canSwitch(int meta);
}
