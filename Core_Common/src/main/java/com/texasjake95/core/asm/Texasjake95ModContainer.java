package com.texasjake95.core.asm;

/*
 * package texasjake95.Core.asm; import java.util.Arrays; import java.util.List;
 * import java.util.logging.Level; import com.google.common.eventbus.EventBus;
 * import com.google.common.eventbus.Subscribe; import
 * cpw.mods.fml.common.DummyModContainer; import
 * cpw.mods.fml.common.LoadController; import cpw.mods.fml.common.Mod.Instance;
 * import cpw.mods.fml.common.ModMetadata; import
 * cpw.mods.fml.common.SidedProxy; import
 * cpw.mods.fml.common.event.FMLInitializationEvent; import
 * cpw.mods.fml.common.event.FMLPostInitializationEvent; import
 * cpw.mods.fml.common.event.FMLPreInitializationEvent; import
 * cpw.mods.fml.common.network.NetworkRegistry; import
 * cpw.mods.fml.common.versioning.ArtifactVersion; import
 * net.minecraftforge.common.Configuration; import texasjake95.Core.CommonProxy;
 * import texasjake95.Core.CoreItems; import texasjake95.Core.GUIHandler; import
 * texasjake95.Core.Addon.AddonHandlerCore; import texasjake95.Core.lib.EMCMaps;
 * import texasjake95.Core.lib.ModInfo; import
 * texasjake95.Core.lib.Helpers.CoreRegistry.ItemRegister; import
 * texasjake95.Core.lib.Helpers.StringHelper; import
 * texasjake95.Core.lib.config.CoreConfig; public class Texasjake95ModContainer
 * extends DummyModContainer { public Texasjake95ModContainer() { super(new
 * ModMetadata()); getMetadata(); }
 * @Override public List<ArtifactVersion> getDependencies() { return
 * super.getDependencies(); }
 * @Override public ModMetadata getMetadata() { ModMetadata meta =
 * super.getMetadata(); meta.modId = ModInfo.CoreID; meta.name =
 * ModInfo.CoreName; meta.version = ModInfo.CoreVersion; meta.authorList =
 * Arrays.asList("Texasjake95"); meta.description =
 * "A coremod used by all of Texasjake95's Mods"; return meta; }
 * @SidedProxy(clientSide = "texasjake95.Core.ClientProxy", serverSide =
 * "texasjake95.Core.CommonProxy") public static CommonProxy proxy;
 * @Instance(ModInfo.CoreID) public static Texasjake95ModContainer instance;
 * public static Configuration config = CoreConfig.config;
 * @Override public boolean registerBus(EventBus bus, LoadController controller)
 * { bus.register(this); return true; }
 * @Subscribe public void preload(FMLPreInitializationEvent event) {
 * StringHelper.Info = Level.INFO; StringHelper.Finer = Level.FINER;
 * StringHelper.Fine = Level.FINE; StringHelper.Finest = Level.FINEST;
 * StringHelper.Severe = Level.SEVERE; StringHelper.Warning = Level.WARNING;
 * StringHelper.initLogger(); EMCMaps.SetToolEMC(); CoreConfig.initProps(event);
 * }
 * @Subscribe public void load(FMLInitializationEvent event) {
 * NetworkRegistry.instance().registerGuiHandler(instance, new GUIHandler());
 * CoreItems.init(); // CoreRegistry.addRecipe(new ItemStack(CoreApiItems.Craft,
 * 1), new // Object[] { "X", 'X', new ItemStack(Block.dirt) });
 * AddonHandlerCore.CheckMods(); }
 * @Subscribe public void modsLoaded(FMLPostInitializationEvent event) {
 * CoreConfig.endProps(); ItemRegister.DEBUG(); } }
 */
