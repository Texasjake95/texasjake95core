package com.texasjake95.core.asm;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;

import com.texasjake95.base.helpers.LoggerHelper;

import cpw.mods.fml.common.asm.transformers.AccessTransformer;

public class Texasjake95CoreAccessTransformer extends AccessTransformer {
	
	private static Texasjake95CoreAccessTransformer instance;
	private static List<String> mapFileList = new LinkedList<String>();
	
	@SuppressWarnings("static-access")
	public static void addAccessTransformer(String name)
	{
		if (instance == null)
		{
			mapFileList.add(name);
		}
		else
		{
			instance.mapFileList.add(name);
		}
	}
	
	public Texasjake95CoreAccessTransformer() throws IOException
	{
		super();
		instance = this;
		mapFileList.add("txCore_at.cfg");
		for (String map : mapFileList)
		{
			this.readMapFile(map);
		}
	}
	
	private void readMapFile(String name)
	{
		LoggerHelper.getCoreLogger("Adding transformer map: " + name, Level.FINE);
		try
		{
			Method e = AccessTransformer.class.getDeclaredMethod("readMapFile", new Class[] { String.class });
			e.setAccessible(true);
			e.invoke(this, new Object[] { name });
		}
		catch (Exception ex)
		{
			throw new RuntimeException(ex);
		}
	}
}
