package com.texasjake95.core;

import com.texasjake95.core.energy.PowerHelper;

import net.minecraftforge.common.ForgeHooks;

import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;

public class PoweredToolHandler implements IToolHandler {
	
	@Override
	public boolean canHarvest(int blockID, int blockMeta, ItemStack stack)
	{
		return ForgeHooks.canToolHarvestBlock(Block.blocksList[blockID], blockMeta, stack) || stack.getItem().canHarvestBlock(Block.blocksList[blockID], stack) || Block.blocksList[blockID].blockMaterial.isToolNotRequired();
	}
	
	@Override
	public boolean canAutoSwtichTo(ItemStack stack)
	{
		return true;
	}
	
	@Override
	public boolean isDamageable(ItemStack stack)
	{
		return stack.isItemStackDamageable();
	}
	
	@Override
	public double getDurability(ItemStack stack)
	{
		double per = (double) PowerHelper.getPowerPer(stack);
		return per;
	}
}
