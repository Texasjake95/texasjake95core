package com.texasjake95.core;

import net.minecraft.item.Item;

public interface IItemHandler {
	
	public String itemName();
	
	public boolean shouldInit();
	
	public Item initItem();
	
	public IItemHandler setModName(String modName);
	
	public String getModName();
	
	/**
	 * Not in use yet
	 */
	@Deprecated
	public int getPriority();
	
	/**
	 * Not in use yet
	 */
	@Deprecated
	public IItemHandler setPriority(int priority);
}
