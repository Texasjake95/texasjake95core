package com.texasjake95.core;

import com.texasjake95.base.config.ConfigWriterTemplate;
import com.texasjake95.core.Items.NoFallBoots;
import com.texasjake95.core.Items.PortableCraftingTable;
import com.texasjake95.core.config.CoreConfig;

import net.minecraftforge.common.EnumHelper;

import net.minecraft.item.EnumArmorMaterial;
import net.minecraft.item.Item;

public class CoreItems {
	
	public static Item Craft;
	// public static Block test;
	private static ConfigWriterTemplate configw = CoreConfig.getInstance().configw;
	public static Item NoFallBoots;
	static EnumArmorMaterial nofalldamage = EnumHelper.addArmorMaterial("nofall", 1000, new int[] { 3, 8, 6, 3 }, 0);
	
	public static void init()
	{
		Craft = new PortableCraftingTable(configw.getItemID("Portable Crafting Table", 0));
		NoFallBoots = (new NoFallBoots(configw.getItemID("Fall Boots", 0), nofalldamage, 0, 3));
		configw.registerAll();
	}
}
