package com.texasjake95.core.Items;

import java.util.List;

import com.texasjake95.core.EntityCustomLightningBolt;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;

public class SpellThunder extends Spell {
	
	public SpellThunder()
	{
		super(2, new String[] { "thunder", "thundara", "thundaga" });
	}
	
	@Override
	public float getDamage(int lvl)
	{
		switch (lvl)
		{
			case 3:
				return 4;
			case 2:
				return 2;
			case 1:
				return 1;
			default:
				return 0;
		}
	}
	
	@Override
	public void doSpell(World world, EntityPlayer player, ItemStack stack)
	{
		System.out.println("Level is: " + this.getLvl(stack));
		@SuppressWarnings("unchecked")
		List<Entity> vaildTargets = world.getEntitiesWithinAABBExcludingEntity(player, AxisAlignedBB.getBoundingBox(player.posX - 7, player.posY - 7, player.posZ - 7, player.posX + 7, player.posY + 7, player.posZ + 7));
		for (Entity validTarget : vaildTargets)
			if (validTarget instanceof EntityLivingBase)
				world.addWeatherEffect(new EntityCustomLightningBolt(player, world, validTarget.posX, validTarget.posY, validTarget.posZ, this.getDamage(this.getLvl(stack))));
		if (!vaildTargets.isEmpty())
			this.setSpellTimer(stack, 40);
	}
}
