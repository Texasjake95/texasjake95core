package com.texasjake95.core.Items;

import com.texasjake95.core.EntityBlizzard;
import com.texasjake95.core.EntityBlizzard.Angle;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class SpellBlizard extends Spell {
	
	public SpellBlizard()
	{
		super(2, new String[] { "blizzard1", "blizzard2", "blizzard3" });
	}
	
	@Override
	public float getDamage(int lvl)
	{
		switch (lvl)
		{
			case 3:
				return 4f;
			case 2:
				return 2f;
			case 1:
				return 1f;
			default:
				return 0f;
		}
	}
	
	@Override
	public void doSpell(World world, EntityPlayer player, ItemStack stack)
	{
		for (Angle angle : Angle.values())
			world.spawnEntityInWorld(new EntityBlizzard(world, player, 2.0F, this.getDamage(this.getLvl(stack)), angle));
		this.setSpellTimer(stack, 15);
	}
}
