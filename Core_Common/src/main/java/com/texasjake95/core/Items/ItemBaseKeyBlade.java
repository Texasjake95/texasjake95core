package com.texasjake95.core.Items;

import java.util.List;

import com.texasjake95.base.helpers.NBTHelper;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumAction;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ItemBaseKeyBlade extends Item {
	
	public static final String SpellTimer = "SpellDelay";
	
	public ItemBaseKeyBlade(int par1)
	{
		super(par1);
		this.setHasSubtypes(true);
	}
	
	@Override
	public void onUpdate(ItemStack stack, World world, Entity entity, int umm, boolean currentItem)
	{
		if (!world.isRemote)
		{
			int time = NBTHelper.getInt(stack, SpellTimer);
			if (currentItem && time > 0)
			{
				time--;
				NBTHelper.setInteger(stack, SpellTimer, time);
			}
		}
	}
	
	public String getUnlocalizedName(ItemStack stack)
	{
		if (stack.getItemDamage() == 24)
			return this.getUnlocalizedName() + "24";
		else if (Spell.getSpell(stack) == null)
			return this.getUnlocalizedName();
		else
			return this.getUnlocalizedName() + Spell.getSpell(stack).getName(stack);
	}
	
	@Override
	public ItemStack onItemRightClick(ItemStack stack, World world, EntityPlayer player)
	{
		if (stack.getItemDamage() != 24)
		{
			if (canCastSpell(stack))
			{
				Spell spell = Spell.getSpell(stack);
				if (spell != null)
					spell.doSpell(world, player, stack);
			}
		}
		else
		{
			player.setItemInUse(stack, this.getMaxItemUseDuration(stack));
		}
		return stack;
	}
	
	/**
	 * Returns True is the item is renderer in full 3D when hold.
	 */
	public boolean isFull3D()
	{
		return true;
	}
	
	public EnumAction getItemUseAction(ItemStack stack)
	{
		if (stack.getItemDamage() != 24)
		{
			return EnumAction.none;
		}
		return EnumAction.block;
	}
	
	public int getMaxItemUseDuration(ItemStack par1ItemStack)
	{
		return 72000;
	}
	
	private boolean canCastSpell(ItemStack stack)
	{
		int time = NBTHelper.getInt(stack, SpellTimer);
		return time <= 0;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void getSubItems(int par1, CreativeTabs par2CreativeTabs, List par3List)
	{
		for (int meta = 0; meta <= 24; meta++)
		{
			par3List.add(new ItemStack(par1, 1, meta));
		}
	}
	
	@Override
	public void registerIcons(IconRegister iconr)
	{
		this.itemIcon = iconr.registerIcon("texasjake95core:kingdomKey");
	}
}
