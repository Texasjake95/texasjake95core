package com.texasjake95.core.Items;

import java.util.List;

import com.texasjake95.base.items.multi.MultiPickShovel;
import com.texasjake95.core.energy.IPoweredItem;
import com.texasjake95.core.energy.PowerHelper;
import com.texasjake95.core.energy.PowerLocation;

import net.minecraftforge.oredict.OreDictionary;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumToolMaterial;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ItemDrill extends MultiPickShovel implements IPoweredItem {
	
	private final int tier;
	private static final int drillDrainRate = 16;
	
	public ItemDrill(int par1, EnumToolMaterial material, int tier)
	{
		super(par1, material);
		this.tier = tier;
		this.setUnlocalizedName("drill");
		this.setMaxStackSize(1);
		this.setHasSubtypes(true);
	}
	
	@Override
	public void onCreated(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
	{
		switch (tier)
		{
			case 0:
				break;
			case 1:
				break;
			case 2:
				break;
			case 3:
				par1ItemStack.addEnchantment(Enchantment.fortune, 1);
				break;
			case 4:
				par1ItemStack.addEnchantment(Enchantment.fortune, Enchantment.fortune.getMaxLevel());
				par1ItemStack.addEnchantment(Enchantment.aquaAffinity, Enchantment.aquaAffinity.getMaxLevel());
				break;
		}
	}
	
	@Override
	public int getMaxDamage(ItemStack stack)
	{
		return (OreDictionary.WILDCARD_VALUE - 1);
	}
	
	@Override
	public boolean onBlockDestroyed(ItemStack par1ItemStack, World par2World, int par3, int par4, int par5, int par6, EntityLivingBase par7EntityLivingBase)
	{
		PowerHelper.removePower(this, par1ItemStack, drillDrainRate, true, PowerLocation.INTERNAL);
		return true;
	}
	
	@Override
	public int maxDrain(ItemStack stack, PowerLocation inMachine)
	{
		return inMachine == PowerLocation.INTERNAL ? drillDrainRate : PowerHelper.itemDrainMaxRate[this.tier];
	}
	
	@Override
	public int minDrain(ItemStack stack, PowerLocation inMachine)
	{
		return inMachine == PowerLocation.INTERNAL ? drillDrainRate : PowerHelper.itemDrainMinRate[this.tier];
	}
	
	@Override
	public int maxFill(ItemStack stack, PowerLocation inMachine)
	{
		return PowerHelper.itemFillMaxRate[this.tier];
	}
	
	@Override
	public int minFill(ItemStack stack, PowerLocation inMachine)
	{
		return PowerHelper.itemFillMinRate[this.tier];
	}
	
	@Override
	public int getCapacity(ItemStack stack)
	{
		return PowerHelper.itemCapacity[this.tier];
	}
	
	@Override
	public boolean canProvidePower(ItemStack itemstack, PowerLocation inMachine)
	{
		switch (inMachine)
		{
			case MACHINE:
				return true;
			case INTERNAL:
				return true;
			default:
				return false;
		}
	}
	
	@Override
	public boolean useDefaultToolTip(ItemStack itemStack)
	{
		return true;
	}
	
	@Override
	public void registerIcons(IconRegister par1IconRegister)
	{
		this.itemIcon = par1IconRegister.registerIcon(this.iconString);
	}
	
	@Override
	public float getStrVsBlock(ItemStack stack, Block block, int meta)
	{
		if (!(PowerHelper.getPower(stack) > drillDrainRate))
			return 1;
		return super.getStrVsBlock(stack, block, meta);
	}
	
	@Override
	public int getItemEnchantability()
	{
		return 0;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void getSubItems(int par1, CreativeTabs par2CreativeTabs, List par3List)
	{
		if (this.tier >= 0 && this.tier < 3)
			PowerHelper.addFullAndEmpty(this, par1, par2CreativeTabs, par3List);
		if (tier == 3)
		{
			ItemStack full = PowerHelper.getFull(this);
			ItemStack empty = PowerHelper.getEmpty(this);
			full.addEnchantment(Enchantment.fortune, 1);
			empty.addEnchantment(Enchantment.fortune, 1);
			par3List.add(empty);
			par3List.add(full);
		}
		if (tier == 4)
		{
			ItemStack full = PowerHelper.getFull(this);
			ItemStack empty = PowerHelper.getEmpty(this);
			full.addEnchantment(Enchantment.fortune, Enchantment.fortune.getMaxLevel());
			full.addEnchantment(Enchantment.aquaAffinity, Enchantment.aquaAffinity.getMaxLevel());
			empty.addEnchantment(Enchantment.fortune, Enchantment.fortune.getMaxLevel());
			empty.addEnchantment(Enchantment.aquaAffinity, Enchantment.aquaAffinity.getMaxLevel());
			par3List.add(empty);
			par3List.add(full);
		}
	}
}
