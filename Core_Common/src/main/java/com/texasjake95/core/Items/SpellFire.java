package com.texasjake95.core.Items;

import com.texasjake95.core.EntityFire;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class SpellFire extends Spell {
	
	public SpellFire()
	{
		super(0, new String[] { "fire", "fira", "firaga" });
	}
	
	@Override
	public float getDamage(int lvl)
	{
		switch (lvl)
		{
			case 3:
				return 4f;
			case 2:
				return 2f;
			case 1:
				return 1f;
			default:
				return 0f;
		}
	}
	
	@Override
	public void doSpell(World world, EntityPlayer player, ItemStack stack)
	{
		world.spawnEntityInWorld(new EntityFire(world, player, 2.0F, this.getLvl(stack), this.getDamage(this.getLvl(stack))));
		this.setSpellTimer(stack, 10);
	}
}
