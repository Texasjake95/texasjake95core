package com.texasjake95.core.Items;

import java.util.List;

import com.texasjake95.base.items.CoreItem_Base;
import com.texasjake95.core.CoreRefernece;
import com.texasjake95.core.Texasjake95Core;
import com.texasjake95.core.inventory.ContainerPortableFurnace;
import com.texasjake95.core.inventory.InventoryPortableFurnace;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.InventoryEnderChest;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class PortableCraftingTable extends CoreItem_Base {
	
	public PortableCraftingTable(int ID)
	{
		super(ID);
		this.setHasSubtypes(true);
		this.setMaxDamage(0);
		this.setMaxStackSize(1);
		this.setCreativeTab(CreativeTabs.tabMaterials);
	}
	
	@Override
	public void onUpdate(ItemStack stack, World world, Entity entity, int slot, boolean isCurrent)
	{
		switch (stack.getItemDamage())
		{
			case 2:
				doFurnace(entity, stack, world);
				break;
			default:
				break;
		}
	}
	
	private void doFurnace(Entity entity, ItemStack stack, World world)
	{
		if (entity instanceof EntityPlayer)
		{
			EntityPlayer living = (EntityPlayer) entity;
			if (living.openContainer instanceof ContainerPortableFurnace)
			{
				InventoryPortableFurnace furnace = ((ContainerPortableFurnace) living.openContainer).furnace;
				furnace.readFromNBT(stack);
				furnace.update(world);
				furnace.writeToNBT(stack);
			}
			else
			{
				InventoryPortableFurnace furnace = new InventoryPortableFurnace(stack);
				furnace.readFromNBT(stack);
				furnace.update(world);
				furnace.writeToNBT(stack);
			}
		}
		else
		{
			InventoryPortableFurnace furnace = new InventoryPortableFurnace(stack);
			furnace.readFromNBT(stack);
			furnace.update(world);
			furnace.writeToNBT(stack);
		}
	}
	
	@SuppressWarnings("unused")
	private void doPotion(Entity entity, ItemStack stack, World world)
	{
	}
	
	@Override
	public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
	{
		switch (par1ItemStack.getItemDamage())
		{
			case 1:
				par3EntityPlayer.displayGUIChest(new BagInventory(par1ItemStack));
				break;
			case 2:
				par3EntityPlayer.openGui(Texasjake95Core.instance, 2, par2World, (int) par3EntityPlayer.posX, (int) par3EntityPlayer.posY, (int) par3EntityPlayer.posZ);
				break;
			case 3:
				par3EntityPlayer.displayGUIChest(new BagInventoryLarge(par1ItemStack));
				break;
			case 4:
				InventoryEnderChest chest = par3EntityPlayer.getInventoryEnderChest();
				par3EntityPlayer.displayGUIChest(chest);
				break;
			default:
				par3EntityPlayer.openGui(Texasjake95Core.instance, 1, par2World, (int) par3EntityPlayer.posX, (int) par3EntityPlayer.posY, (int) par3EntityPlayer.posZ);
				break;
		}
		return par1ItemStack;
	}
	
	@Override
	public boolean onItemUse(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, World par3World, int par4, int par5, int par6, int par7, float par8, float par9, float par10)
	{
		switch (par1ItemStack.getItemDamage())
		{
			case 1:
				par2EntityPlayer.displayGUIChest(new BagInventory(par1ItemStack));
				break;
			case 2:
				par2EntityPlayer.openGui(Texasjake95Core.instance, 2, par3World, (int) par2EntityPlayer.posX, (int) par2EntityPlayer.posY, (int) par2EntityPlayer.posZ);
				break;
			case 3:
				par2EntityPlayer.displayGUIChest(new BagInventoryLarge(par1ItemStack));
				break;
			case 4:
				InventoryEnderChest chest = par2EntityPlayer.getInventoryEnderChest();
				par2EntityPlayer.displayGUIChest(chest);
				break;
			default:
				par2EntityPlayer.openGui(Texasjake95Core.instance, 1, par3World, (int) par2EntityPlayer.posX, (int) par2EntityPlayer.posY, (int) par2EntityPlayer.posZ);
				break;
		}
		return true;
	}
	
	@Override
	public String getUnlocalizedName(ItemStack stack)
	{
		switch (stack.getItemDamage())
		{
			case 1:
				return "tx.item.chest";
			case 2:
				return "tx.item.furnace";
			case 3:
				return "tx.item.chestLarge";
			case 4:
				return "tx.item.enderChestP";
		}
		return "tx.item.crafting";
	}
	
	@Override
	public void registerIcons(IconRegister par1IconRegister)
	{
		this.itemIcon = par1IconRegister.registerIcon(CoreRefernece.CoreID + ":" + this.getUnlocalizedName());
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void getSubItems(int par1, CreativeTabs par2CreativeTabs, List par3List)
	{
		par3List.add(new ItemStack(par1, 1, 0));
		par3List.add(new ItemStack(par1, 1, 1));
		par3List.add(new ItemStack(par1, 1, 2));
		par3List.add(new ItemStack(par1, 1, 3));
		par3List.add(new ItemStack(par1, 1, 4));
	}
}