package com.texasjake95.core.Items;

import net.minecraftforge.common.ISpecialArmor;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumArmorMaterial;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;

public class NoFallBoots extends ItemArmor implements ISpecialArmor {
	
	public NoFallBoots(int par1, EnumArmorMaterial par2EnumArmorMaterial, int par3, int par4)
	{
		super(par1, par2EnumArmorMaterial, par3, par4);
	}
	
	@Override
	public void damageArmor(EntityLivingBase entity, ItemStack stack, DamageSource source, int damage, int slot)
	{
		if (source == DamageSource.fall)
		{
			return;
		}
	}
	
	@Override
	public int getArmorDisplay(EntityPlayer player, ItemStack armor, int slot)
	{
		if (slot == 0)
		{
			return 3;
		}
		return 0;
	}
	
	@Override
	public ArmorProperties getProperties(EntityLivingBase player, ItemStack armor, DamageSource source, double damage, int slot)
	{
		if (slot == 0 && source == DamageSource.fall)
		{
			return new ArmorProperties(10, 1D, Integer.MAX_VALUE);
		}
		return new ArmorProperties(1, damage, 10);
	}
}
