package com.texasjake95.core.lib;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.MathHelper;

public class Extra {
	
	/**
	 * int 0 SOUTH int 1 WEST int 2 NORTH int 3 EAST
	 */
	public static double direction(EntityPlayer var0)
	{
		return MathHelper.floor_double(var0.rotationYaw * 4.0F / 360.0F + 0.5D);
	}
}
