package com.texasjake95.core.lib;

public class MathHelper {
	
	public static int minInteger(int a, int b)
	{
		return a > b ? b : a;
	}
}
