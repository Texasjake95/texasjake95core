package com.texasjake95.core.api.item.interfaces;

public interface ICraftDamage {
	
	/**
	 * Used on Items to prevent the damage from the ItemStack to be used in a
	 * Shapeless Damage Recipe
	 * 
	 * Metadata Sensitive
	 */
	public boolean useDamage(int meta);
}
