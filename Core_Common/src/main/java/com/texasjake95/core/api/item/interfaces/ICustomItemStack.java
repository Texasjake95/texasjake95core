package com.texasjake95.core.api.item.interfaces;

import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;

public interface ICustomItemStack {
	
	boolean canHarvestBlock(ItemStack customStack, Block par1Block);
}
