package com.texasjake95.core;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;

public class TXTank extends FluidTank {
	
	public TXTank(int capacity)
	{
		super(capacity);
	}
	
	public TXTank(FluidStack stack, int capacity)
	{
		super(stack, capacity);
	}
	
	public TXTank(Fluid fluid, int amount, int capacity)
	{
		super(fluid, amount, capacity);
	}
	
	public void writeToPacket(DataOutputStream dos)
	{
		try
		{
			if (this.fluid != null)
			{
				dos.writeBoolean(true);
				dos.writeInt(this.fluid.fluidID);
				dos.writeInt(this.fluid.amount);
			}
			else
			{
				dos.writeBoolean(false);
			}
			dos.writeInt(this.capacity);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public void readFromPacket(DataInputStream dis)
	{
		try
		{
			if (dis.readBoolean())
				this.setFluid(new FluidStack(dis.readInt(), dis.readInt()));
			this.setCapacity(dis.readInt());
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
