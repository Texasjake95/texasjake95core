package com.texasjake95.core.network.packet;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.texasjake95.core.tile.TileEntityCore;

import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.network.Player;

import net.minecraft.network.INetworkManager;

public class PacketTileEntityUpdate extends PacketTX {
	
	TileEntityCore tile;
	DataInputStream data;
	int x, y, z;
	
	public PacketTileEntityUpdate(PacketTypeHandler packetType, TileEntityCore tile, String channelName)
	{
		this(packetType, channelName);
		this.tile = tile;
	}
	
	public PacketTileEntityUpdate(TileEntityCore tile, String channelName)
	{
		this(PacketTypeHandler.Tile, tile, channelName);
	}
	
	public PacketTileEntityUpdate(PacketTypeHandler packetType, String channelName)
	{
		super(packetType, true, channelName);
	}
	
	public PacketTileEntityUpdate(String channelName)
	{
		this(PacketTypeHandler.Tile, channelName);
	}
	
	public PacketTileEntityUpdate()
	{
		this(PacketTypeHandler.Tile, "");
	}
	
	@Override
	public void writeData(DataOutputStream data) throws IOException
	{
		this.tile.writeToPacket(data, this.packetType);
	}
	
	@Override
	public void readData(DataInputStream data) throws IOException
	{
		this.x = data.readInt();
		this.y = data.readInt();
		this.z = data.readInt();
		this.data = data;
	}
	
	@Override
	public void execute(INetworkManager manager, Player player)
	{
		TileEntityCore tile = (TileEntityCore) FMLClientHandler.instance().getClient().theWorld.getBlockTileEntity(x, y, z);
		tile.xCoord = x;
		tile.yCoord = y;
		tile.zCoord = z;
		try
		{
			tile.readFromPacket(data, this.packetType);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
