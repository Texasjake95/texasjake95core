package com.texasjake95.core.network.packet;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import cpw.mods.fml.common.network.Player;

import net.minecraft.network.INetworkManager;

public class PacketTX {
	
	protected boolean isChunkDataPacket;
	protected PacketTypeHandler packetType;
	protected String channelName;
	
	public PacketTX(PacketTypeHandler packetType, boolean isChunkDataPacket, String channelName)
	{
		this.packetType = packetType;
		this.isChunkDataPacket = isChunkDataPacket;
		this.channelName = channelName;
	}
	
	public void execute(INetworkManager network, Player player)
	{
	}
	
	public byte[] populate()
	{
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(bos);
		try
		{
			dos.writeByte(this.packetType.ordinal());
			this.writeData(dos);
		}
		catch (IOException e)
		{
			e.printStackTrace(System.err);
		}
		return bos.toByteArray();
	}
	
	public void readData(DataInputStream data) throws IOException
	{
	}
	
	public void readPopulate(DataInputStream data)
	{
		try
		{
			this.readData(data);
		}
		catch (IOException e)
		{
			e.printStackTrace(System.err);
		}
	}
	
	public void setKey(int key)
	{
	}
	
	public void writeData(DataOutputStream dos) throws IOException
	{
	}
}
