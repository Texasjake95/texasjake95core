package com.texasjake95.core.network.packet;

import com.texasjake95.core.tile.TileEntityGrinder;

public class PacketGrinderUpdate extends PacketTileEntityUpdate {
	
	public PacketGrinderUpdate()
	{
		super(PacketTypeHandler.Grinder, "Texasjake95Core");
	}
	
	public PacketGrinderUpdate(TileEntityGrinder tile)
	{
		super(PacketTypeHandler.Grinder, tile, "Texasjake95Core");
	}
}
