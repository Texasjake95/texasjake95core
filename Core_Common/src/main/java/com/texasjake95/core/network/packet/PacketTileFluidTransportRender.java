package com.texasjake95.core.network.packet;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.texasjake95.core.tile.TileEntityCore;
import com.texasjake95.core.tile.TileEntityFluidTransport;

import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.network.Player;

import net.minecraft.network.INetworkManager;

public class PacketTileFluidTransportRender extends PacketTX {
	
	TileEntityFluidTransport fluid;
	private DataInputStream data;
	int x, y, z;
	
	public PacketTileFluidTransportRender(TileEntityFluidTransport tile, String channelName)
	{
		super(PacketTypeHandler.FluidTransportRender, true, channelName);
		this.fluid = tile;
	}
	
	public PacketTileFluidTransportRender()
	{
		super(PacketTypeHandler.FluidTransportRender, true, "");
	}
	
	@Override
	public void writeData(DataOutputStream data) throws IOException
	{
		this.fluid.writeToPacket(data, this.packetType);
	}
	
	@Override
	public void readData(DataInputStream data) throws IOException
	{
		this.x = data.readInt();
		this.y = data.readInt();
		this.z = data.readInt();
		this.data = data;
	}
	
	@Override
	public void execute(INetworkManager manager, Player player)
	{
		TileEntityCore tile = (TileEntityCore) FMLClientHandler.instance().getClient().theWorld.getBlockTileEntity(x, y, z);
		if (tile != null)
		{
			tile.xCoord = x;
			tile.yCoord = y;
			tile.zCoord = z;
			try
			{
				tile.readFromPacket(data, this.packetType);
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
