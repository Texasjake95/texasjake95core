package com.texasjake95.core.client.gui;

import com.texasjake95.core.inventory.ContainerGrinder;
import com.texasjake95.core.tile.TileEntityGrinder;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.EntityPlayer;

public class GuiGrinder extends GuiContainer {
	
	public GuiGrinder(EntityPlayer player, TileEntityGrinder grinder)
	{
		super(new ContainerGrinder(player.inventory, grinder));
	}
	
	@Override
	protected void drawGuiContainerBackgroundLayer(float f, int i, int j)
	{
		// TODO Auto-generated method stub
	}
}
