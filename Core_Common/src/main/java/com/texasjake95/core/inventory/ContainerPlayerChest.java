package com.texasjake95.core.inventory;

import com.texasjake95.core.tile.TileEntityPlayerChest;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class ContainerPlayerChest extends Container {
	
	private IInventory chest;
	private int numRows;
	
	public ContainerPlayerChest(IInventory playerInv, TileEntityPlayerChest chest)
	{
		this.chest = chest;
		this.numRows = 5;
		System.out.print(chest.getCustomName());
		chest.openChest();
		int i = (this.numRows - 4) * 18;
		int j;
		int k;
		for (k = 0; k < 4; k++)
		{
			this.addSlotToContainer(new CustomSlotArmor(chest, 39 - k, 8 + k * 18, 18, k));
		}
		for (j = 0; j < this.numRows - 1; ++j)
		{
			for (k = 0; k < 9; ++k)
			{
				this.addSlotToContainer(new Slot(chest, k + j * 9, 8 + k * 18, 18 + (j + 1) * 18));
			}
		}
		for (j = 0; j < 3; ++j)
		{
			for (k = 0; k < 9; ++k)
			{
				this.addSlotToContainer(new Slot(playerInv, k + j * 9 + 9, 8 + k * 18, 103 + j * 18 + i));
			}
		}
		for (j = 0; j < 9; ++j)
		{
			this.addSlotToContainer(new Slot(playerInv, j, 8 + j * 18, 161 + i));
		}
	}
	
	public boolean canInteractWith(EntityPlayer par1EntityPlayer)
	{
		return this.chest.isUseableByPlayer(par1EntityPlayer);
	}
	
	public ItemStack transferStackInSlot(EntityPlayer par1EntityPlayer, int par2)
	{
		ItemStack itemstack = null;
		Slot slot = (Slot) this.inventorySlots.get(par2);
		if (slot != null && slot.getHasStack())
		{
			ItemStack itemstack1 = slot.getStack();
			itemstack = itemstack1.copy();
			if (par2 < this.numRows * 9)
			{
				if (!this.mergeItemStack(itemstack1, 36, this.inventorySlots.size(), true))
				{
					return null;
				}
			}
			else if (!this.mergeItemStack(itemstack1, 0, 36, false))
			{
				return null;
			}
			if (itemstack1.stackSize == 0)
			{
				slot.putStack((ItemStack) null);
			}
			else
			{
				slot.onSlotChanged();
			}
		}
		return itemstack;
	}
	
	/**
	 * Called when the container is closed.
	 */
	public void onContainerClosed(EntityPlayer par1EntityPlayer)
	{
		super.onContainerClosed(par1EntityPlayer);
		this.chest.closeChest();
	}
	
	/**
	 * Return this chest container's lower chest inventory.
	 */
	public IInventory getLowerChestInventory()
	{
		return this.chest;
	}
}
