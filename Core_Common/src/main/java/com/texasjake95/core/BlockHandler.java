package com.texasjake95.core;

import java.lang.reflect.InvocationTargetException;
import java.util.LinkedList;

import com.texasjake95.base.config.ConfigWriterTemplate;

import net.minecraft.block.Block;
import net.minecraft.item.ItemBlock;

public class BlockHandler implements IBlockHandler {
	
	LinkedList<Class<?>> parameterTypes = new LinkedList<Class<?>>();
	LinkedList<Object> parameters = new LinkedList<Object>();
	private final Class<? extends Block> blockClass;
	private String blockName;
	private final Class<? extends ItemBlock> itemBlockClass;
	private final ConfigWriterTemplate configw;
	private int blockID;
	private int priority;
	private String modName = null;
	
	public BlockHandler(String name, Class<? extends Block> blockClass, Class<? extends ItemBlock> itemBlockClass, ConfigWriterTemplate configw, Class<?>[] parameterTypes, Object[] parameters)
	{
		this.blockName = name;
		this.blockClass = blockClass;
		this.itemBlockClass = itemBlockClass;
		this.configw = configw;
		this.parameterTypes.add(int.class);
		for (Class<?> parameterType : parameterTypes)
			this.parameterTypes.add(parameterType);
		for (Object parameter : parameters)
			this.parameters.add(parameter);
		this.configw.config().getCatagory("IDs").addSubCatagoryWithComment("Blocks", "All Blocks can be turned off by setting id to -1");
	}
	
	public BlockHandler(String name, Class<? extends Block> blockClass, ConfigWriterTemplate configw)
	{
		this(name, blockClass, ItemBlock.class, configw, new Class<?>[] {}, new Object[] {});
	}
	
	@Override
	public String blockName()
	{
		return blockName;
	}
	
	@Override
	public boolean shouldInit()
	{
		this.blockID = this.configw.getBlockID(this.blockName, -1);
		this.parameters.addFirst(this.blockID);
		return this.blockID != -1;
	}
	
	@Override
	public Block initBlock()
	{
		Class<?>[] types = this.parameterTypes.toArray(new Class<?>[0]);
		Object[] params = this.parameters.toArray(new Object[0]);
		try
		{
			return this.blockClass.getConstructor(types).newInstance(params);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public Class<? extends ItemBlock> getItemBlockClass()
	{
		return this.itemBlockClass;
	}
	
	@Override
	public int getPriority()
	{
		return this.priority;
	}
	
	@Override
	public IBlockHandler setPriority(int priority)
	{
		this.priority = priority;
		return this;
	}
	
	@Override
	public IBlockHandler setModName(String modName)
	{
		this.modName = modName;
		return this;
	}
	
	@Override
	public String getModName()
	{
		return this.modName;
	}
}
