package com.texasjake95.core;

import net.minecraft.block.Block;
import net.minecraft.item.Item;

public interface IItemRegistry {
	
	public void addItemHandler(IItemHandler handler);
	
	public void addBlockHandler(IBlockHandler handler);
	
	public Item getItem(String name);
	
	public Block getBlock(String name);
	
	public boolean isBlockLoaded(String name);
	
	public boolean isItemLoaded(String name);
}
