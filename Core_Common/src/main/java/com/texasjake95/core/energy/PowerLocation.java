package com.texasjake95.core.energy;

public enum PowerLocation
{
	/**
	 * This is if the power is being drained/added by a machine
	 */
	MACHINE,
	/**
	 * This is if the power is being drained/added by an item in the player's
	 * inventory
	 */
	INVENTORY,
	/**
	 * This is if the power is being drained/added by an armor in the player's
	 * inventory
	 */
	ARMOR,
	/**
	 * This is if the power is being drained/added (usually drain) by a itself
	 */
	INTERNAL;
}
