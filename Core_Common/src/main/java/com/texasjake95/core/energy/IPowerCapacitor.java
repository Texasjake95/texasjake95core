package com.texasjake95.core.energy;

/**
 * An interface to make a power capacitor
 * 
 * NOTE: It is much like the fluid system but for power.
 * 
 * @author Texasjake95
 * 
 */
public interface IPowerCapacitor {
	
	/**
	 * @return Current amount of power in the capacitor.
	 */
	int getPower();
	
	/**
	 * @return Capacity of this capacitor.
	 */
	int getCapacity();
	
	/**
	 * 
	 * @param power
	 *            the amount of power attempting to fill the capacitor.
	 * @param doFill
	 *            If false, the fill will only be simulated.
	 * @return Amount of power that was accepted by the capacitor.
	 */
	int fill(int power, boolean doFill);
	
	/**
	 * 
	 * @param power
	 *            Maximum amount of power to be removed from the capacitor.
	 * @param doFill
	 *            If false, the fill will only be simulated.
	 * @return Amount of power that was removed from the capacitor.
	 */
	int drain(int power, boolean doDrain);
	
	int maxTransferRate();
	
	int minTransferRate();
	
	void setPower(int power);
	
	void setCapacity(int capacity);
	
	void setMaxTransferRate(int transferRate);
	
	void setMinTransferRate(int transferRate);
}
