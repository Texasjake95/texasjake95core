package com.texasjake95.core.energy;

import java.util.List;

import com.texasjake95.base.helpers.NBTHelper;
import com.texasjake95.core.Texasjake95Core;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class PowerHelper {
	
	public static final int[] itemDrainMinRate = new int[] { 1, 11, 21, 31, 1 };
	public static final int[] itemDrainMaxRate = new int[] { 19, 29, 39, 50, 50 };
	public static final int[] itemFillMinRate = new int[] { 1, 11, 21, 31, 1 };
	public static final int[] itemFillMaxRate = new int[] { 19, 29, 39, 50, 50 };
	private static final int itemTier1 = 10000;
	private static final int itemTier2 = itemTier1 * 2;
	private static final int itemTier3 = itemTier2 * 2;
	private static final int itemTier4 = itemTier3 * 2;
	private static final int itemTier5 = itemTier4 * 2;
	public static final int[] itemCapacity = new int[] { itemTier1, itemTier2, itemTier3, itemTier4, itemTier5 };
	
	public static int getPower(ItemStack stack)
	{
		if (stack == null || !(stack.getItem() instanceof IPoweredItem))
			return 0;
		if (stack.getItem() instanceof ICustomPoweredItem)
			return ((ICustomPoweredItem) stack.getItem()).getPower(stack);
		return NBTHelper.getInt(stack, "power");
	}
	
	public static int addPower(IPoweredItem poweredItem, ItemStack stack, int add, boolean shouldFill, PowerLocation inMachine)
	{
		int power;
		int trueAdd = add > poweredItem.maxFill(stack, inMachine) ? poweredItem.maxFill(stack, inMachine) : add;
		trueAdd = trueAdd < poweredItem.minFill(stack, inMachine) ? 0 : trueAdd;
		if (poweredItem == null || stack == null)
			return 0;
		if (poweredItem instanceof ICustomPoweredItem)
		{
			return ((ICustomPoweredItem) poweredItem).fill(stack, trueAdd, shouldFill);
		}
		else
		{
			power = getPower(stack);
			if (power + trueAdd > poweredItem.getCapacity(stack))
			{
				trueAdd = poweredItem.getCapacity(stack) - power;
				power = poweredItem.getCapacity(stack);
			}
			else
			{
				power += trueAdd;
			}
			if (shouldFill)
			{
				NBTHelper.setInteger(stack, "power", power);
				setItemDamage(stack);
			}
			return trueAdd;
		}
	}
	
	public static int removePower(IPoweredItem poweredItem, ItemStack stack, int remove, boolean shouldDrain, PowerLocation inMachine)
	{
		int power;
		int trueRemove = remove > poweredItem.maxDrain(stack, inMachine) ? poweredItem.maxDrain(stack, inMachine) : remove;
		trueRemove = trueRemove < poweredItem.minDrain(stack, inMachine) ? 0 : trueRemove;
		if (poweredItem == null || stack == null || !poweredItem.canProvidePower(stack, inMachine))
			return 0;
		if (poweredItem instanceof ICustomPoweredItem)
		{
			return ((ICustomPoweredItem) poweredItem).drain(stack, trueRemove, shouldDrain);
		}
		else
		{
			power = getPower(stack);
			if (power - trueRemove < 0)
			{
				trueRemove = power;
				power = 0;
			}
			else
			{
				power -= trueRemove;
				if (shouldDrain)
				{
					NBTHelper.setInteger(stack, "power", power);
					setItemDamage(stack);
				}
			}
			return trueRemove;
		}
	}
	
	public static void setPower(ItemStack stack, int power)
	{
		if (stack == null || !(stack.getItem() instanceof IPoweredItem))
			return;
		if (stack.getItem() instanceof ICustomPoweredItem)
			((ICustomPoweredItem) stack.getItem()).setPower(stack, power);
		else
		{
			NBTHelper.setInteger(stack, "power", power);
			setItemDamage(stack);
		}
	}
	
	private static void setItemDamage(ItemStack stack)
	{
		double powerPer = 1 - getPowerPer(stack);
		int damage = ((int) (powerPer * stack.getMaxDamage()));
		stack.setItemDamage(damage);
	}
	
	public static double getPowerPer(ItemStack stack)
	{
		int power = getPower(stack);
		int capacity = ((IPoweredItem) stack.getItem()).getCapacity(stack);
		double powerPer = (double) power / (double) capacity;
		return powerPer;
	}
	
	public static boolean isAbleToExchangePower(IPoweredItem poweredItem, ItemStack stack, IPowerHandler handler, boolean drain, PowerLocation isInMachine)
	{
		int maxTransferRate = handler.getCapacitor().getMaxTransferRate();
		int minTransferRate = handler.getCapacitor().getMinTransferRate();
		if (!canTransferEnergy(handler, !drain))
			return false;
		int maxItemRate = drain ? poweredItem.maxDrain(stack, isInMachine) : poweredItem.maxFill(stack, isInMachine);
		int minItemRate = drain ? poweredItem.minDrain(stack, isInMachine) : poweredItem.minFill(stack, isInMachine);
		if (!canTransferEnergy(poweredItem, stack, drain, isInMachine))
			return false;
		for (int i = minTransferRate; i <= maxTransferRate; i++)
			for (int j = minItemRate; j <= maxItemRate; j++)
				if (i == j)
					return true;
		if (Texasjake95Core.isTesting)
			System.out.println("Ranges do not match");
		return false;
	}
	
	public static boolean isAbleToExchangePower(IPowerHandler from, IPowerHandler to)
	{
		int maxFromRate = from.getCapacitor().getMaxTransferRate();
		int minFromRate = from.getCapacitor().getMinTransferRate();
		if (!canTransferEnergy(from, true))
			return false;
		int maxToRate = to.getCapacitor().getMaxTransferRate();
		int minToRate = to.getCapacitor().getMinTransferRate();
		if (!canTransferEnergy(to, false))
			return false;
		for (int i = minFromRate; i <= maxFromRate; i++)
			for (int j = minToRate; j <= maxToRate; j++)
				if (i == j)
					return true;
		if (Texasjake95Core.isTesting)
			System.out.println("Ranges do not match");
		return false;
	}
	
	public static boolean isAbleToExchangePower(IPoweredItem from, ItemStack fromStack, IPoweredItem to, ItemStack toStack, PowerLocation isInMachine)
	{
		int maxFromRate = from.maxDrain(fromStack, isInMachine);
		int minFromRate = from.minDrain(fromStack, isInMachine);
		if (!canTransferEnergy(from, fromStack, true, isInMachine))
			return false;
		int maxToRate = to.maxFill(toStack, isInMachine);
		int minToRate = to.minFill(toStack, isInMachine);
		if (!canTransferEnergy(to, toStack, false, isInMachine))
			return false;
		for (int i = minFromRate; i <= maxFromRate; i++)
			for (int j = minToRate; j <= maxToRate; j++)
				if (i == j)
					return true;
		if (Texasjake95Core.isTesting)
			System.out.println("Ranges do not match");
		return false;
	}
	
	public static boolean canTransferEnergy(IPowerHandler handler, boolean drain)
	{
		return handler.getCapacitor().getMinTransferRate() < (!drain ? handler.getCapacitor().getCapacity() - handler.getCapacitor().getPower() : handler.getCapacitor().getPower());
	}
	
	public static boolean canTransferEnergy(IPoweredItem item, ItemStack stack, boolean drain, PowerLocation isInMachine)
	{
		int minDrain = item.minDrain(stack, isInMachine);
		int minFill = item.minFill(stack, isInMachine);
		int power = getPower(stack);
		int capacity = item.getCapacity(stack);
		return drain ? minDrain < power : minFill < capacity - power;
	}
	
	public static void addFullAndEmpty(Item item, int par1, CreativeTabs par2CreativeTabs, List par3List)
	{
		if (item instanceof IPoweredItem)
		{
			ItemStack full = getFull(item);
			ItemStack empty = getEmpty(item);
			par3List.add(empty);
			par3List.add(full);
		}
	}
	
	public static ItemStack getFull(Item item)
	{
		ItemStack full = new ItemStack(item.itemID, 0, 0);
		setPower(full, ((IPoweredItem) item).getCapacity(full));
		return full;
	}
	
	public static ItemStack getEmpty(Item item)
	{
		ItemStack empty = new ItemStack(item.itemID, 0, 0);
		setPower(empty, 0);
		return empty;
	}
}
