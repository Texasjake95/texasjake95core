package com.texasjake95.core.energy;

public class PowerCapacitorInfo {
	
	private final IPowerCapacitor capacitor;
	
	public PowerCapacitorInfo(IPowerCapacitor capacitor)
	{
		this.capacitor = capacitor;
	}
	
	public int getPower()
	{
		return capacitor.getPower();
	}
	
	/**
	 * @return Capacity of this capacitor.
	 */
	public int getCapacity()
	{
		return capacitor.getCapacity();
	}
	
	public int getMaxTransferRate()
	{
		return capacitor.maxTransferRate();
	}
	
	public int getMinTransferRate()
	{
		return capacitor.minTransferRate();
	}
}
