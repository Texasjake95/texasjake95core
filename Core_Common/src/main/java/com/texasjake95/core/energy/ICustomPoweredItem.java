package com.texasjake95.core.energy;

import net.minecraft.item.ItemStack;

/**
 * 
 * Use this instead of{@link IPoweredItem} to have custom control of the power
 * in the item
 * 
 * @author Texasjake95
 * 
 */
public interface ICustomPoweredItem extends IPoweredItem {
	
	/**
	 * 
	 * @param container
	 *            ItemStack which is the power capacitor.
	 * @return amount of power in the capacitor.
	 */
	int getPower(ItemStack container);
	
	/**
	 * This allows the item to handle its own power
	 * 
	 * @param container
	 *            ItemStack which is the power capacitor.
	 * @param power
	 *            the amount of power attempting to fill the capacitor.
	 * @param doFill
	 *            If false, the fill will only be simulated.
	 * @return Amount of power that was (or would have been, if simulated)
	 *         filled into the capacitor.
	 */
	int fill(ItemStack container, int power, boolean doFill);
	
	/**
	 * This allows the item to handle its own power
	 * 
	 * @param container
	 *            ItemStack which is the power capacitor.
	 * @param power
	 *            Maximum amount of power to be removed from the capacitor.
	 * @param doFill
	 *            If false, the drain will only be simulated.
	 * @return Amount of power that was (or would have been, if simulated)
	 *         drained from the capacitor.
	 */
	int drain(ItemStack container, int power, boolean doDrain);
	
	/**
	 * This lets outside sources set the power of the item
	 */
	void setPower(ItemStack stack, int power);
}
