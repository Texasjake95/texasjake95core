package com.texasjake95.core.tile;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.texasjake95.core.energy.IPowerCapacitor;
import com.texasjake95.core.energy.PowerCapacitorInfo;

import net.minecraft.nbt.NBTTagCompound;

public class PowerStorage implements IPowerCapacitor {
	
	private int power = 0;
	private int capacity = 0;
	private int maxTransferRate = 1;
	private int minTransferRate = 1;
	private PowerCapacitorInfo info;
	
	public PowerStorage(int capacity)
	{
		this(capacity, capacity);
	}
	
	public PowerStorage(int capacity, int transferRate)
	{
		this(capacity, transferRate, transferRate);
	}
	
	public PowerStorage(int capacity, int maxTransferRate, int minTransferRate)
	{
		this.capacity = capacity;
		this.maxTransferRate = maxTransferRate;
		this.minTransferRate = minTransferRate;
		this.info = new PowerCapacitorInfo(this);
	}
	
	@Override
	public int getPower()
	{
		return power;
	}
	
	@Override
	public int getCapacity()
	{
		return capacity;
	}
	
	@Override
	public int fill(int power, boolean doFill)
	{
		int fill = this.power + power <= this.capacity ? power : this.capacity - this.power;
		fill = this.minTransferRate > fill ? 0 : this.maxTransferRate < fill ? this.maxTransferRate : fill;
		if (doFill)
			this.power += fill;
		return fill;
	}
	
	@Override
	public int drain(int power, boolean doDrain)
	{
		int drain = this.power - power >= 0 ? power : this.power;
		drain = this.minTransferRate > drain ? 0 : this.maxTransferRate < drain ? this.maxTransferRate : drain;
		if (doDrain)
			this.power -= drain;
		return drain;
	}
	
	@Override
	public int maxTransferRate()
	{
		return maxTransferRate;
	}
	
	@Override
	public int minTransferRate()
	{
		return minTransferRate;
	}
	
	@Override
	public void setPower(int power)
	{
		this.power = power;
	}
	
	@Override
	public void setCapacity(int capacity)
	{
		this.capacity = capacity;
	}
	
	@Override
	public void setMaxTransferRate(int maxTransferRate)
	{
		this.maxTransferRate = maxTransferRate;
	}
	
	@Override
	public void setMinTransferRate(int minTransferRate)
	{
		this.minTransferRate = minTransferRate;
	}
	
	public PowerCapacitorInfo getInfo()
	{
		return info;
	}
	
	public void writeToPacket(DataOutputStream dos) throws IOException
	{
		dos.writeInt(power);
		dos.writeInt(capacity);
		dos.writeInt(maxTransferRate);
		dos.writeInt(minTransferRate);
	}
	
	public void readFromPacket(DataInputStream dis) throws IOException
	{
		power = dis.readInt();
		capacity = dis.readInt();
		maxTransferRate = dis.readInt();
		minTransferRate = dis.readInt();
	}
	
	public PowerStorage readFromNBT(NBTTagCompound nbt)
	{
		power = nbt.getInteger("power");
		capacity = nbt.getInteger("capacity");
		maxTransferRate = nbt.getInteger("maxTransferRate");
		minTransferRate = nbt.getInteger("minTransferRate");
		return this;
	}
	
	public NBTTagCompound writeToNBT(NBTTagCompound nbt)
	{
		nbt.setInteger("power", power);
		nbt.setInteger("capacity", capacity);
		nbt.setInteger("maxTransferRate", maxTransferRate);
		nbt.setInteger("minTransferRate", minTransferRate);
		return nbt;
	}
}
