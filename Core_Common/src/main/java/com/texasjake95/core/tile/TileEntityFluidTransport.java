package com.texasjake95.core.tile;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.texasjake95.commons.util.CustomArrayList;
import com.texasjake95.core.TXSmartTank;
import com.texasjake95.core.TXSmartTank.TankState;
import com.texasjake95.core.Texasjake95Core;
import com.texasjake95.core.network.packet.PacketTileEntityUpdate;
import com.texasjake95.core.network.packet.PacketTileFluidTransportRender;
import com.texasjake95.core.network.packet.PacketTypeHandler;

import cpw.mods.fml.common.network.PacketDispatcher;

import net.minecraftforge.common.ForgeDirection;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTankInfo;
import net.minecraftforge.fluids.IFluidHandler;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.packet.Packet;
import net.minecraft.tileentity.TileEntity;

public class TileEntityFluidTransport extends TileEntityCore implements IFluidHandler {
	
	protected TXSmartTank[] internalTanks = new TXSmartTank[7];
	private IFluidHandler[] tanks = new IFluidHandler[6];
	private static final CustomArrayList<String> pipeRenders = new CustomArrayList<String>().addAll(new String[] { "PipeBottom", "PipeTop", "PipeBack", "PipeFront", "PipeLeft", "PipeRight" });
	private static final CustomArrayList<String> tankRenders = new CustomArrayList<String>().addAll(new String[] { "Bottom", "Top", "Back", "Front", "Left", "Right" });
	private static final String CORNER = "Corner";
	private static final String PIPESTRAIGHT = "PipeStraight";
	private static ForgeDirection[] directions = new ForgeDirection[] { ForgeDirection.DOWN, ForgeDirection.NORTH, ForgeDirection.EAST, ForgeDirection.SOUTH, ForgeDirection.WEST, ForgeDirection.UP };
	public int flowRate = 100;
	private int centerTankCap = this.flowRate * 18;
	private boolean isType[] = new boolean[6];
	public int inputTimer[] = new int[6];
	public int outputTimer[] = new int[6];
	private ConnectType[] connectType = new ConnectType[6];
	
	public TileEntityFluidTransport()
	{
		super();
		for (ForgeDirection d : directions)
		{
			internalTanks[d.ordinal()] = new TXSmartTank(this.flowRate);
			if (d != ForgeDirection.UNKNOWN)
				connectType[d.ordinal()] = ConnectType.NONE;
		}
		internalTanks[ForgeDirection.UNKNOWN.ordinal()] = new TXSmartTank(centerTankCap);
	}
	
	public void onBlockUpdate()
	{
		for (ForgeDirection direction : directions)
		{
			TileEntity tile = this.worldObj.getBlockTileEntity(this.xCoord + direction.offsetX, this.yCoord + direction.offsetY, this.zCoord + direction.offsetZ);
			boolean isAir = this.worldObj.isAirBlock(this.xCoord + direction.offsetX, this.yCoord + direction.offsetY, this.zCoord + direction.offsetZ);
			if (tile != null && !isAir)
				if (tile instanceof IFluidHandler)
				{
					this.tanks[direction.ordinal()] = (IFluidHandler) tile;
					if (tile instanceof TileEntityFluidTransport)
					{
						this.isType[direction.ordinal()] = true;
					}
					else
					{
						this.isType[direction.ordinal()] = false;
					}
				}
				else
				{
					this.tanks[direction.ordinal()] = null;
					this.isType[direction.ordinal()] = false;
				}
			else
			{
				this.tanks[direction.ordinal()] = null;
				this.isType[direction.ordinal()] = false;
			}
		}
	}
	
	public void updateRenders()
	{
		for (ForgeDirection direction : directions)
		{
			TileEntity tile = this.worldObj.getBlockTileEntity(this.xCoord + direction.offsetX, this.yCoord + direction.offsetY, this.zCoord + direction.offsetZ);
			boolean isAir = this.worldObj.isAirBlock(this.xCoord + direction.offsetX, this.yCoord + direction.offsetY, this.zCoord + direction.offsetZ);
			if (tile != null && !isAir)
				if (tile instanceof IFluidHandler)
					if (tile instanceof TileEntityFluidTransport)
						this.connectType[direction.ordinal()] = ConnectType.PIPE;
					else
						this.connectType[direction.ordinal()] = ConnectType.TANK;
				else
					this.connectType[direction.ordinal()] = ConnectType.NONE;
			else
				this.connectType[direction.ordinal()] = ConnectType.NONE;
		}
		PacketDispatcher.sendPacketToAllPlayers(PacketTypeHandler.populatePacket(new PacketTileFluidTransportRender(this, "Texasjake95Core")));
	}
	
	@Override
	public void updateEntity()
	{
		if (!this.isSameBlock(Texasjake95Core.fluidTransport.blockID))
		{
			this.invalidate();
			this.worldObj.markTileEntityForDespawn(this);
			return;
		}
		this.onBlockUpdate();
		moveToCenter();
		for (int i = 0; i < 6; i++)
		{
			if (inputTimer[i] > 0)
				inputTimer[i] -= 1;
			if (outputTimer[i] > 0)
				outputTimer[i] -= 1;
		}
		int validOutputs = this.countValidOutputs();
		if (!this.isPowered())
			this.pushFluids(validOutputs, ForgeDirection.UNKNOWN);
		else
		{
			this.pullFluid();
			this.pushFluids(validOutputs, this.facing);
		}
		for (int i = 0; i < 6; i++)
		{
			ForgeDirection d = ForgeDirection.getOrientation(i);
			if (this.getTank(d).getState() == TankState.INPUT)
				if (this.inputTimer[i] == 0)
					this.getTank(d).setState(TankState.NONE);
			if (this.getTank(d).getState() == TankState.OUTPUT)
				if (this.outputTimer[i] == 0)
					this.getTank(d).setState(TankState.NONE);
		}
	}
	
	private boolean isPowered()
	{
		return this.worldObj.isBlockIndirectlyGettingPowered(this.xCoord, this.yCoord, this.zCoord);
	}
	
	private void pullFluid()
	{
		TileEntity tile = this.worldObj.getBlockTileEntity(this.xCoord + this.facing.offsetX, this.yCoord + this.facing.offsetY, this.zCoord + this.facing.offsetZ);
		if (tile != null)
			if (tile instanceof IFluidHandler && !this.isType[this.facing.ordinal()])
			{
				IFluidHandler tank = (IFluidHandler) tile;
				FluidStack drainStack = tank.drain(this.facing.getOpposite(), this.flowRate * 3, false);
				if (drainStack != null)
					if (this.canFill(this.facing, drainStack.getFluid()))
					{
						int fill = this.fill(this.facing, tank.drain(this.facing.getOpposite(), this.flowRate, false), true);
						tank.drain(this.facing.getOpposite(), fill, true);
					}
			}
	}
	
	private void pushFluids(int validOutputs, ForgeDirection d)
	{
		for (ForgeDirection direction : directions)
		{
			if (direction == d)
			{
				continue;
			}
			if (this.getCenterTank().getFluid() != null && this.getCenterTank().getFluid().getFluid() != null)
				if (this.canDrain(direction, this.getCenterTank().getFluid().getFluid()))
				{
					TXSmartTank tank = this.getTank(direction);
					moveToTank(direction);
					IFluidHandler tileTank = tanks[direction.ordinal()];
					if (tileTank != null && tileTank.canFill(direction.getOpposite(), tank.getFluid().getFluid()))
						if (this.isType[direction.ordinal()])
						{
							for (int i = 0; i < 3; i++)
							{
								moveToTank(direction);
								FluidStack stack = tank.drain(this.flowRate, false);
								int drain = 0;
								if (stack != null)
									drain = tileTank.fill(direction.getOpposite(), stack, true);
								tank.drain(drain, true);
							}
						}
						else
						{
							FluidStack stack = tank.drain(this.flowRate / validOutputs, false);
							int drain = 0;
							if (stack != null)
								drain = tileTank.fill(direction.getOpposite(), stack, true);
							tank.drain(drain, true);
						}
					moveToCenter();
				}
		}
	}
	
	private int countValidOutputs()
	{
		int validOutputs = 0;
		for (ForgeDirection d : directions)
		{
			if (this.getTank(d).getState() == TankState.OUTPUT || this.getTank(d).getState() == TankState.NONE)
				if (this.tanks[d.ordinal()] != null && !this.isType[d.ordinal()])
					validOutputs++;
		}
		return validOutputs;
	}
	
	private void moveToCenter()
	{
		TXSmartTank center = this.getCenterTank();
		for (ForgeDirection d : directions)
		{
			TXSmartTank tank = this.getTank(d);
			if (tank.getFluid() == null)
				continue;
			FluidStack drain = tank.drain(tank.getCapacity(), false);
			if (center.getFluidAmount() + drain.amount >= center.getCapacity())
				drain.amount = center.getCapacity() - center.getFluidAmount();
			drain = tank.drain(drain.amount, true);
			center.fill(drain, true);
		}
	}
	
	@Override
	public void readFromNBT(NBTTagCompound tag)
	{
		super.readFromNBT(tag);
		this.flowRate = tag.getInteger("flowRate");
		NBTTagCompound connectTypes = tag.getCompoundTag("ConnectTypes");
		for (ForgeDirection d : ForgeDirection.values())
		{
			NBTTagCompound tag2 = tag.getCompoundTag("Tank:" + d.name());
			TXSmartTank tank = this.internalTanks[d.ordinal()];
			tank.readFromNBT(tag2);
			if (d != ForgeDirection.UNKNOWN)
			{
				this.connectType[d.ordinal()] = ConnectType.getConnectType(connectTypes.getByte("ConnectType:" + d.name()));
			}
		}
	}
	
	@Override
	public void writeToNBT(NBTTagCompound tag)
	{
		super.writeToNBT(tag);
		tag.setInteger("flowRate", this.flowRate);
		NBTTagCompound connectTypes = new NBTTagCompound();
		for (ForgeDirection d : ForgeDirection.values())
		{
			TXSmartTank tank = this.internalTanks[d.ordinal()];
			NBTTagCompound tag2 = new NBTTagCompound();
			tank.writeToNBT(tag2);
			tag.setCompoundTag("Tank:" + d.name(), tag2);
			if (d != ForgeDirection.UNKNOWN)
			{
				connectTypes.setByte("ConnectType:" + d.name(), (byte) this.connectType[d.ordinal()].ordinal());
			}
		}
		tag.setCompoundTag("ConnectTypes", connectTypes);
	}
	
	/* IFluidHandler */
	@Override
	public int fill(ForgeDirection from, FluidStack resource, boolean doFill)
	{
		int fill = 0;
		TXSmartTank tank = this.getTank(from);
		tank.setState(TankState.INPUT);
		this.inputTimer[from.ordinal()] += 1;
		while (resource.amount > 0)
		{
			int fake = tank.fill(resource, false);
			fill += tank.fill(resource, doFill);
			moveToCenter();
			resource.amount -= fake;
			if (tank.getCapacity() == tank.getFluidAmount())
			{
				return fill;
			}
		}
		return fill;
	}
	
	@Override
	public FluidStack drain(ForgeDirection from, FluidStack resource, boolean doDrain)
	{
		moveToTank(from);
		if (resource == null || !resource.isFluidEqual(this.getTank(from).getFluid()))
		{
			return null;
		}
		this.getTank(from).setState(TankState.OUTPUT);
		this.outputTimer[from.ordinal()] += 1;
		FluidStack drain = this.getTank(from).drain(resource.amount, doDrain);
		return drain;
	}
	
	@Override
	public FluidStack drain(ForgeDirection from, int maxDrain, boolean doDrain)
	{
		moveToTank(from);
		this.getTank(from).setState(TankState.OUTPUT);
		this.outputTimer[from.ordinal()] += 1;
		FluidStack drain = this.getTank(from).drain(maxDrain, doDrain);
		return drain;
	}
	
	private void moveToTank(ForgeDirection from)
	{
		TXSmartTank center = this.getCenterTank();
		TXSmartTank tank = this.getTank(from);
		if (from == ForgeDirection.UNKNOWN)
			return;
		FluidStack drain = center.drain(tank.getCapacity(), false);
		int realDrain = tank.fill(drain, true);
		center.drain(realDrain, true);
	}
	
	@Override
	public boolean canFill(ForgeDirection from, Fluid fluid)
	{
		if (fluid == null)
		{
		}
		if (this.getCenterTank().getFluid() == null)
			return true;
		if (this.getTank(from).getState() == TankState.OUTPUT)
			return false;
		if ((this.getCenterTank().getFluid() != null && this.getCenterTank().getFluid().getFluid().getID() != fluid.getID()))
			return false;
		moveToCenter();
		boolean result = this.getCenterTank().getFluidAmount() < this.getCenterTank().getCapacity();
		return result;
	}
	
	@Override
	public boolean canDrain(ForgeDirection from, Fluid fluid)
	{
		if (fluid == null || this.getCenterTank().getFluid() == null || this.getCenterTank().getFluid().getFluid() == null || this.getCenterTank().getFluid().getFluid().getID() != fluid.getID())
			return false;
		if (this.getTank(from).getState() == TankState.INPUT)
			return false;
		moveToTank(from);
		boolean result = this.getTank(from).getFluidAmount() > 0;
		moveToCenter();
		return result;
	}
	
	@Override
	public void writeToPacket(DataOutputStream dos, PacketTypeHandler packetType) throws IOException
	{
		if (dos != null)
		{
			switch (packetType)
			{
				case Tile:
				{
					super.writeToPacket(dos, packetType);
					dos.writeInt(this.flowRate);
					for (ForgeDirection d : directions)
					{
						if (d != ForgeDirection.UNKNOWN)
						{
							dos.writeByte(this.connectType[d.ordinal()].ordinal());
						}
					}
					for (TXSmartTank tank : this.internalTanks)
					{
						tank.writeToPacket(dos);
					}
					return;
				}
				case FluidTransportRender:
				{
					try
					{
						dos.writeInt(this.xCoord);
						dos.writeInt(this.yCoord);
						dos.writeInt(this.zCoord);
						for (ForgeDirection d : directions)
						{
							if (d != ForgeDirection.UNKNOWN)
							{
								dos.writeByte(this.connectType[d.ordinal()].ordinal());
							}
						}
					}
					catch (IOException e)
					{
						e.printStackTrace();
					}
					return;
				}
				default:
					return;
			}
		}
	}
	
	public void readFromPacket(DataInputStream dis, PacketTypeHandler packetType) throws IOException
	{
		if (dis != null)
		{
			switch (packetType)
			{
				case Tile:
				{
					super.readFromPacket(dis, packetType);
					this.flowRate = dis.readInt();
					for (ForgeDirection d : directions)
					{
						if (d != ForgeDirection.UNKNOWN)
						{
							this.connectType[d.ordinal()] = ConnectType.getConnectType(dis.readByte());
						}
					}
					for (TXSmartTank tank : this.internalTanks)
					{
						tank.readFromPacket(dis);
					}
					return;
				}
				case FluidTransportRender:
				{
					for (ForgeDirection d : directions)
					{
						if (d != ForgeDirection.UNKNOWN)
						{
							this.connectType[d.ordinal()] = ConnectType.getConnectType(dis.readByte());
						}
					}
					return;
				}
				default:
					return;
			}
		}
	}
	
	@Override
	public FluidTankInfo[] getTankInfo(ForgeDirection from)
	{
		return new FluidTankInfo[] { this.internalTanks[from.ordinal()].getInfo() };
	}
	
	private TXSmartTank getCenterTank()
	{
		return this.internalTanks[ForgeDirection.UNKNOWN.ordinal()];
	}
	
	private TXSmartTank getTank(ForgeDirection d)
	{
		return this.internalTanks[d.ordinal()];
	}
	
	@Override
	public Packet getDescriptionPacket()
	{
		return PacketTypeHandler.populatePacket(new PacketTileEntityUpdate(this, "Texasjake95Core"));
	}
	
	public String[] getRenderNames()
	{
		CustomArrayList<String> tankNames = new CustomArrayList<String>();
		CustomArrayList<String> pipeNames = new CustomArrayList<String>();
		CustomArrayList<String> names = new CustomArrayList<String>();
		for (ForgeDirection d : directions)
		{
			ConnectType type = this.connectType[d.ordinal()];
			if (type == ConnectType.PIPE)
			{
				pipeNames.add(pipeRenders.getList().get(d.ordinal()));
				names.add(pipeRenders.getList().get(d.ordinal()));
			}
			else if (type == ConnectType.TANK)
			{
				tankNames.add(tankRenders.getList().get(d.ordinal()));
				pipeNames.add(pipeRenders.getList().get(d.ordinal()));
				names.add(tankRenders.getList().get(d.ordinal()));
				names.add(pipeRenders.getList().get(d.ordinal()));
			}
			else
			{
			}
		}
		if (pipeNames.getList().size() != 2)
		{
			names.add(CORNER);
		}
		else
		{
			String pipeName = pipeNames.getList().get(0);
			int index = pipeRenders.getList().indexOf(pipeName);
			ForgeDirection d = ForgeDirection.getOrientation(index).getOpposite();
			String oppositeRender = pipeRenders.getList().get(d.ordinal());
			if (pipeNames.contains(oppositeRender))
			{
				names.add(PIPESTRAIGHT);
			}
			else
			{
				names.add(CORNER);
			}
		}
		return names.toArray(new String[0]);
	}
	
	private static enum ConnectType
	{
		PIPE, TANK, NONE;
		
		public static ConnectType getConnectType(int id)
		{
			if (id > values().length)
				id = values().length - 1;
			return values()[id];
		}
	}
}
