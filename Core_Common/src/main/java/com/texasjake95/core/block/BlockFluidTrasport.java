package com.texasjake95.core.block;

import com.texasjake95.base.helpers.PlayerHelper;
import com.texasjake95.core.tile.TileEntityFluidTransport;

import net.minecraftforge.common.ForgeDirection;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class BlockFluidTrasport extends BlockContainer {
	
	public BlockFluidTrasport(int par1)
	{
		super(par1, Material.iron);
		this.setBlockBounds(0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f);
	}
	
	@Override
	public TileEntity createNewTileEntity(World world)
	{
		return new TileEntityFluidTransport();
	}
	
	@Override
	public void onNeighborBlockChange(World par1World, int par2, int par3, int par4, int par5)
	{
		if (!par1World.isRemote)
		{
			TileEntity tile = par1World.getBlockTileEntity(par2, par3, par4);
			if (tile instanceof TileEntityFluidTransport)
			{
				((TileEntityFluidTransport) tile).onBlockUpdate();
				((TileEntityFluidTransport) tile).updateRenders();
			}
		}
	}
	
	@Override
	public void onBlockPlacedBy(World par1World, int par2, int par3, int par4, EntityLivingBase par5EntityLivingBase, ItemStack par6ItemStack)
	{
		if (!par1World.isRemote)
		{
			TileEntity tile = par1World.getBlockTileEntity(par2, par3, par4);
			if (tile instanceof TileEntityFluidTransport)
			{
				if (par5EntityLivingBase instanceof EntityPlayer)
					((TileEntityFluidTransport) tile).setFacing(PlayerHelper.getPlayerFacing((EntityPlayer) par5EntityLivingBase));
				System.out.println("Placing block facing" + ((TileEntityFluidTransport) tile).getFacing());
				((TileEntityFluidTransport) tile).onBlockUpdate();
				((TileEntityFluidTransport) tile).updateRenders();
			}
		}
	}
	
	@Override
	public int onBlockPlaced(World par1World, int par2, int par3, int par4, int par5, float par6, float par7, float par8, int par9)
	{
		if (!par1World.isRemote)
		{
			TileEntity tile = par1World.getBlockTileEntity(par2, par3, par4);
			if (tile instanceof TileEntityFluidTransport)
			{
				((TileEntityFluidTransport) tile).onBlockUpdate();
				((TileEntityFluidTransport) tile).updateRenders();
			}
		}
		return par9;
	}
	
	@Override
	public boolean renderAsNormalBlock()
	{
		return false;
	}
	
	@Override
	public int getRenderType()
	{
		return RenderIndex.fluidTransort;
	}
	
	@Override
	public boolean isOpaqueCube()
	{
		return false;
	}
	
	@Override
	public boolean rotateBlock(World world, int x, int y, int z, ForgeDirection axis)
	{
		TileEntity tile = world.getBlockTileEntity(x, y, z);
		if (tile instanceof TileEntityFluidTransport)
		{
			return ((TileEntityFluidTransport) tile).rotateBlock(axis);
		}
		return false;
	}
}
