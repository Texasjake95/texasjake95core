package com.texasjake95.core.addon;

import java.util.LinkedList;
import java.util.logging.Level;

import com.texasjake95.base.helpers.LoggerHelper;
import com.texasjake95.core.addon.buildcraft.CoreBuildcraftAddon;
import com.texasjake95.core.addon.ee3.CoreEE3Addon;
import com.texasjake95.core.addon.nei.NEICoreConfig;

public class AddonHandler {
	
	private static AddonHandler instance;
	private LinkedList<IAddonCore<?>> addons = new LinkedList<IAddonCore<?>>();
	
	public AddonHandler()
	{
		addons.add(CoreEE3Addon.getInstance());
		addons.add(NEICoreConfig.getInstance());
		addons.add(CoreBuildcraftAddon.getInstance());
	}
	
	public void preLoadAddons()
	{
		for (IAddonCore<?> addon : addons)
		{
			if (addon.isModLoaded())
			{
				LoggerHelper.getCoreLogger(String.format("Preloading %s addons", addon.modName()), Level.INFO);
				addon.preLoad();
			}
		}
	}
	
	public void loadAddons()
	{
		for (IAddonCore<?> addon : addons)
		{
			if (addon.isModLoaded())
			{
				LoggerHelper.getCoreLogger(String.format("Loading %s addons", addon.modName()), Level.INFO);
				addon.load();
			}
		}
	}
	
	public void postLoadAddons()
	{
		for (IAddonCore<?> addon : addons)
		{
			if (addon.isModLoaded())
			{
				LoggerHelper.getCoreLogger(String.format("Postloading %s addons", addon.modName()), Level.INFO);
				addon.postLoad();
			}
		}
	}
	
	public void onServerLoad()
	{
		for (IAddonCore<?> addon : addons)
		{
			if (addon.isModLoaded())
			{
				LoggerHelper.getCoreLogger(String.format("Loading %s addons with Server", addon.modName()), Level.INFO);
				addon.onServerLoad();
			}
		}
	}
	
	public void onServerLoaded()
	{
		for (IAddonCore<?> addon : addons)
		{
			if (addon.isModLoaded())
			{
				LoggerHelper.getCoreLogger(String.format("Loading %s addons after Server load", addon.modName()), Level.INFO);
				addon.onServerLoaded();
			}
		}
	}
	
	public static AddonHandler getInstance()
	{
		if (instance == null)
			instance = new AddonHandler();
		return instance;
	}
}
