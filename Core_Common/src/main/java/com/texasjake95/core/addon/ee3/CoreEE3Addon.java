package com.texasjake95.core.addon.ee3;

import java.util.LinkedList;

import com.texasjake95.core.addon.IAddonCore;
import com.texasjake95.core.config.CoreConfig;

import cpw.mods.fml.common.Loader;

public class CoreEE3Addon implements IAddonCore<IEE3> {
	
	protected LinkedList<IEE3> EE3AddOns = new LinkedList<IEE3>();
	private static CoreEE3Addon instance;
	
	public CoreEE3Addon()
	{
	}
	
	@Override
	public void preLoad()
	{
		for (IEE3 handler : EE3AddOns)
		{
			handler.activate(Boolean.valueOf(CoreConfig.getInstance().configw.config().getSubCatagory(new String[] { "Addons" }, "EE3_Addons").addProperty("Activate " + handler.ModName() + " EE3 Addon", true).value));
			if (handler.getActivate())
			{
				handler.preLoad();
			}
		}
	}
	
	@Override
	public void load()
	{
		for (IEE3 handler : EE3AddOns)
		{
			if (handler.getActivate())
			{
				handler.load();
			}
		}
	}
	
	@Override
	public void postLoad()
	{
		for (IEE3 handler : EE3AddOns)
		{
			if (handler.getActivate())
			{
				handler.postLoad();
			}
		}
	}
	
	@Override
	public boolean isModLoaded()
	{
		return Loader.isModLoaded("EE3");
	}
	
	public static IAddonCore<IEE3> getInstance()
	{
		if (instance == null)
			instance = new CoreEE3Addon();
		return instance;
	}
	
	@Override
	public String modName()
	{
		return "Equivalent Exchange 3";
	}
	
	@Override
	public void onServerLoad()
	{
		for (IEE3 handler : EE3AddOns)
		{
			if (handler.getActivate())
			{
				handler.onServerLoad();
			}
		}
	}
	
	@Override
	public void onServerLoaded()
	{
		for (IEE3 handler : EE3AddOns)
		{
			if (handler.getActivate())
			{
				handler.onServerLoaded();
			}
		}
	}
	
	public void addAddon(IEE3 addon)
	{
		this.EE3AddOns.add(addon);
	}
}
