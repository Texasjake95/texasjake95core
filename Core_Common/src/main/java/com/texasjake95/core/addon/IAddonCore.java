package com.texasjake95.core.addon;

public interface IAddonCore<T extends IBaseAddon> {
	
	public void preLoad();
	
	public void load();
	
	public void postLoad();
	
	public void onServerLoad();
	
	public void onServerLoaded();
	
	public boolean isModLoaded();
	
	public String modName();
	
	public void addAddon(T addon);
}
