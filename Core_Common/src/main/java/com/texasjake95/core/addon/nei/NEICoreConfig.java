package com.texasjake95.core.addon.nei;

import java.util.LinkedList;

import codechicken.nei.api.API;
import codechicken.nei.api.IConfigureNEI;
import codechicken.nei.recipe.DefaultOverlayHandler;

import com.texasjake95.core.Texasjake95Core;
import com.texasjake95.core.addon.IAddonCore;
import com.texasjake95.core.client.gui.GuiPortableCrafting;
import com.texasjake95.core.config.CoreConfig;

import cpw.mods.fml.common.Loader;
import cpw.mods.fml.common.Mod;

public class NEICoreConfig implements IAddonCore<INEI>, IConfigureNEI {
	
	private static LinkedList<INEI> NEIAddOns = new LinkedList<INEI>();
	private static NEICoreConfig instance;
	
	@Override
	public String getName()
	{
		return "Texasjake95 NEI Config";
	}
	
	@Override
	public String getVersion()
	{
		return Texasjake95Core.class.getAnnotation(Mod.class).version();
	}
	
	@Override
	public void loadConfig()
	{
		for (INEI handler : NEIAddOns)
		{
			if (handler.getActivate())
			{
				handler.init();
			}
		}
		API.registerRecipeHandler(new ShapelessDamageRecipeHandler());
		API.registerUsageHandler(new ShapelessDamageRecipeHandler());
		API.registerGuiOverlay(GuiPortableCrafting.class, "crafting");
		API.registerGuiOverlayHandler(GuiPortableCrafting.class, new DefaultOverlayHandler(), "crafting");
	}
	
	@Override
	public void preLoad()
	{
		for (INEI handler : NEIAddOns)
		{
			handler.activate(Boolean.valueOf(CoreConfig.getInstance().configw.config().getSubCatagory(new String[] { "Addons" }, "NEI_Addons").addProperty("Activate " + handler.ModName() + " NEI Addon", true).value));
		}
	}
	
	@Override
	public void load()
	{
	}
	
	@Override
	public void postLoad()
	{
	}
	
	@Override
	public boolean isModLoaded()
	{
		return Loader.isModLoaded("NotEnoughItems");
	}
	
	public static IAddonCore<INEI> getInstance()
	{
		if (instance == null)
			instance = new NEICoreConfig();
		return instance;
	}
	
	@Override
	public String modName()
	{
		return "Not Enough Items";
	}
	
	@Override
	public void onServerLoad()
	{
	}
	
	@Override
	public void onServerLoaded()
	{
	}
	
	@Override
	public void addAddon(INEI addon)
	{
		NEIAddOns.add(addon);
		System.out.println(addon.ModName() + " added an NEI addon");
	}
}
