package com.texasjake95.core.addon.buildcraft;

import java.util.LinkedList;

import com.texasjake95.core.addon.IAddonCore;
import com.texasjake95.core.addon.ee3.CoreEE3Addon;
import com.texasjake95.core.config.CoreConfig;

import cpw.mods.fml.common.Loader;

public class CoreBuildcraftAddon implements IAddonCore<IBuildcraft> {
	
	protected LinkedList<IBuildcraft> BuildcraftAddOns = new LinkedList<IBuildcraft>();
	private static CoreBuildcraftAddon instance;
	
	public CoreBuildcraftAddon()
	{
		CoreEE3Addon.getInstance().addAddon(new Buildcraft_EE3());
	}
	
	public void preLoad()
	{
		for (IBuildcraft handler : BuildcraftAddOns)
		{
			handler.activate(Boolean.valueOf(CoreConfig.getInstance().configw.config().getSubCatagory(new String[] { "Addons" }, "Buildcraft_Addons").addProperty("Activate " + handler.ModName() + " Buildcraft Addon", true).value));
			if (handler.getActivate())
			{
				handler.preLoad();
			}
		}
	}
	
	@Override
	public void load()
	{
		for (IBuildcraft handler : BuildcraftAddOns)
		{
			if (handler.getActivate())
			{
				handler.load();
			}
		}
	}
	
	@Override
	public void postLoad()
	{
		for (IBuildcraft handler : BuildcraftAddOns)
		{
			if (handler.getActivate())
			{
				handler.postLoad();
			}
		}
	}
	
	@Override
	public void onServerLoad()
	{
		for (IBuildcraft handler : BuildcraftAddOns)
		{
			if (handler.getActivate())
			{
				handler.onServerLoad();
			}
		}
	}
	
	@Override
	public void onServerLoaded()
	{
		for (IBuildcraft handler : BuildcraftAddOns)
		{
			if (handler.getActivate())
			{
				handler.onServerLoaded();
			}
		}
	}
	
	@Override
	public boolean isModLoaded()
	{
		return Loader.isModLoaded("BuildCraft|Core");
	}
	
	@Override
	public String modName()
	{
		return "Buildcraft";
	}
	
	public static IAddonCore<IBuildcraft> getInstance()
	{
		if (instance == null)
			instance = new CoreBuildcraftAddon();
		return instance;
	}
	
	public void addAddon(IBuildcraft addon)
	{
		this.BuildcraftAddOns.add(addon);
	}
}
