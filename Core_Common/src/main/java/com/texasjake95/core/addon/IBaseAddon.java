package com.texasjake95.core.addon;

import com.texasjake95.base.helpers.LoggerHelper;

public interface IBaseAddon {
	
	public LoggerHelper SH = new LoggerHelper();
	
	public void activate(boolean b);
	
	public boolean getActivate();
	
	public String ModName();
	
	public void preLoad();
	
	public void load();
	
	public void postLoad();
	
	public void onServerLoad();
	
	public void onServerLoaded();
}
