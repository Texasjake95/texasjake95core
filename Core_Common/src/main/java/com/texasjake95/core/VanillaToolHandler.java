package com.texasjake95.core;

import net.minecraftforge.common.ForgeHooks;

import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;

public class VanillaToolHandler implements IToolHandler {
	
	@Override
	public boolean canHarvest(int blockID, int blockMeta, ItemStack stack)
	{
		return ForgeHooks.canToolHarvestBlock(Block.blocksList[blockID], blockMeta, stack) || stack.getItem().canHarvestBlock(Block.blocksList[blockID], stack) || Block.blocksList[blockID].blockMaterial.isToolNotRequired();
	}
	
	@Override
	public boolean canAutoSwtichTo(ItemStack stack)
	{
		return true;
	}
	
	@Override
	public boolean isDamageable(ItemStack stack)
	{
		return stack.isItemStackDamageable();
	}
	
	@Override
	public double getDurability(ItemStack stack)
	{
		double per = isDamageable(stack) ? (double) stack.getItemDamage() / (double) stack.getMaxDamage() : 0.0F;
		return 1.0D - per;
	}
}
