package com.texasjake95.core.proxy;

import org.lwjgl.opengl.GL11;

import com.texasjake95.core.tile.TileEntityFluidTransport;

import cpw.mods.fml.client.FMLClientHandler;

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;

public class TileEntityFluidTransportRenderer extends TileEntitySpecialRenderer {
	
	private ModelFluidTransport modelFluidTransport = new ModelFluidTransport();
	
	public TileEntityFluidTransportRenderer()
	{
	}
	
	@Override
	public void renderTileEntityAt(TileEntity tileEntity, double x, double y, double z, float tick)
	{
		if (tileEntity instanceof TileEntityFluidTransport)
		{
			GL11.glPushMatrix();
			GL11.glDisable(GL11.GL_LIGHTING);
			// Scale, Translate, Rotate
			GL11.glScalef(1.0F, 1.0F, 1.0F);
			GL11.glTranslatef((float) x, (float) y, (float) z);
			// Bind texture
			FMLClientHandler.instance().getClient().renderEngine.bindTexture(new ResourceLocation("texasjake95core", "/textures/model/FluidTransport.png"));
			// Render
			modelFluidTransport.renderOnly(((TileEntityFluidTransport) tileEntity).getRenderNames());
			GL11.glEnable(GL11.GL_LIGHTING);
			GL11.glPopMatrix();
		}
	}
}
