package com.texasjake95.core.proxy;

import net.minecraftforge.client.IItemRenderer;

import net.minecraft.item.ItemStack;

public abstract class ItemRenderCore implements IItemRenderer {
	
	protected ModelCore model;
	
	public ItemRenderCore(ModelCore model)
	{
		this.model = model;
	}
	
	@Override
	public abstract boolean handleRenderType(ItemStack item, ItemRenderType type);
	
	@Override
	public abstract boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item, ItemRendererHelper helper);
	
	@Override
	public abstract void renderItem(ItemRenderType type, ItemStack item, Object... data);
}
