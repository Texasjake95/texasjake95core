package com.texasjake95.core.proxy;

import net.minecraftforge.client.model.AdvancedModelLoader;
import net.minecraftforge.client.model.IModelCustom;

public class ModelCore {
	
	protected IModelCustom model;
	
	public ModelCore(String objFile)
	{
		this.model = AdvancedModelLoader.loadModel(objFile);
	}
	
	public void renderAll()
	{
		model.renderAll();
	}
	
	public void renderAllExcept(String... excludedGroupNames)
	{
		model.renderAllExcept(excludedGroupNames);
	}
	
	public void renderPart(String partName)
	{
		model.renderPart(partName);
	}
	
	public void renderOnly(String... groupNames)
	{
		model.renderOnly(groupNames);
	}
}
