package com.texasjake95.core.proxy;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;

public class CommonProxy {
	
	public EntityPlayer getPlayer(String name)
	{
		EntityPlayer player = MinecraftServer.getServer().getConfigurationManager().getPlayerForUsername(name);
		if (player == null)
			System.out.println("Player is null:" + name + "on Server");
		return player;
	}
	
	public void registerEntityRenders()
	{
	}
	
	public void registerTileEntities()
	{
	}
	
	public void registerEventHandlers()
	{
	}
	
	public void registerTickHanders(Object mod)
	{
	}
}
