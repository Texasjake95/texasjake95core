package com.texasjake95.core.proxy;

import com.texasjake95.core.Texasjake95Core;

import net.minecraftforge.client.event.TextureStitchEvent;
import net.minecraftforge.event.ForgeSubscribe;

public class ClientEventHandler {
	
	@ForgeSubscribe
	public void registerTextures(TextureStitchEvent.Pre event)
	{
		Texasjake95Core.fluidIconStill = event.map.registerIcon("texasjake95core:oil_still");
		Texasjake95Core.fluidIconFlow = event.map.registerIcon("texasjake95core:oil_flow");
		Texasjake95Core.fluidTest.setIcons(Texasjake95Core.fluidIconStill, Texasjake95Core.fluidIconFlow);
	}
}
