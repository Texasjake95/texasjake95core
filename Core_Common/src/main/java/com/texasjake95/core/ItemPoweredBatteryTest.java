package com.texasjake95.core;

import java.util.List;

import com.texasjake95.core.energy.IPoweredItem;
import com.texasjake95.core.energy.PowerHelper;
import com.texasjake95.core.energy.PowerLocation;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ItemPoweredBatteryTest extends Item implements IPoweredItem {
	
	private final int tier;
	
	public ItemPoweredBatteryTest(int id, int tier)
	{
		super(id);
		this.tier = tier;
		this.setMaxStackSize(1);
		this.setHasSubtypes(true);
		this.setUnlocalizedName("BAT" + this.tier);
	}
	
	public int getMaxDamage(ItemStack stack)
	{
		return 10000;
	}
	
	@Override
	public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
	{
		par3EntityPlayer.setItemInUse(par1ItemStack, this.getMaxItemUseDuration(par1ItemStack));
		return par1ItemStack;
	}
	
	public int getMaxItemUseDuration(ItemStack par1ItemStack)
	{
		return 72000;
	}
	
	@Override
	public void onUsingItemTick(ItemStack stack, EntityPlayer player, int count)
	{
		if (PowerHelper.getPower(stack) != 0 && PowerHelper.getPower(stack) >= this.minDrain(stack, PowerLocation.INVENTORY))
			for (ItemStack invStack : player.inventory.mainInventory)
			{
				if (invStack != null)
					if (invStack.getItem() instanceof IPoweredItem)
					{
						IPoweredItem item = (IPoweredItem) invStack.getItem();
						System.out.println("Item Found");
						if (!item.canProvidePower(invStack, PowerLocation.INVENTORY))
						{
							System.out.println("\tValid Item Found");
							if (PowerHelper.isAbleToExchangePower(this, stack, item, invStack, PowerLocation.INVENTORY))
							{
								int drain = PowerHelper.removePower(this, stack, item.maxDrain(invStack, PowerLocation.INVENTORY), false, PowerLocation.INVENTORY);
								drain = PowerHelper.addPower(item, invStack, drain, false, PowerLocation.INVENTORY);
								System.out.println("\t\tDraining: " + drain);
								if (drain != 0 && drain <= PowerHelper.getPower(stack) && drain >= this.minDrain(stack, PowerLocation.INVENTORY) && drain <= this.maxDrain(stack, PowerLocation.INVENTORY))
								{
									System.out.println("\t\t\tReally draining: " + drain);
									PowerHelper.addPower(item, invStack, drain, true, PowerLocation.INVENTORY);
									PowerHelper.removePower(this, stack, drain, true, PowerLocation.INVENTORY);
								}
							}
						}
					}
			}
		player.stopUsingItem();
	}
	
	@Override
	public int maxDrain(ItemStack stack, PowerLocation inMachine)
	{
		return PowerHelper.itemDrainMaxRate[this.tier];
	}
	
	@Override
	public int minDrain(ItemStack stack, PowerLocation inMachine)
	{
		return PowerHelper.itemDrainMinRate[this.tier];
	}
	
	@Override
	public int maxFill(ItemStack stack, PowerLocation inMachine)
	{
		return PowerHelper.itemFillMaxRate[this.tier];
	}
	
	@Override
	public int minFill(ItemStack stack, PowerLocation inMachine)
	{
		return PowerHelper.itemFillMinRate[this.tier];
	}
	
	@Override
	public int getCapacity(ItemStack stack)
	{
		return PowerHelper.itemCapacity[this.tier];
	}
	
	@Override
	public boolean canProvidePower(ItemStack itemstack, PowerLocation inMachine)
	{
		return true;
	}
	
	@Override
	public void getSubItems(int par1, CreativeTabs par2CreativeTabs, List par3List)
	{
		PowerHelper.addFullAndEmpty(this, par1, par2CreativeTabs, par3List);
	}
	
	@Override
	public boolean useDefaultToolTip(ItemStack itemStack)
	{
		return true;
	}
}
