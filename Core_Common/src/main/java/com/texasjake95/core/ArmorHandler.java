package com.texasjake95.core;

import com.texasjake95.base.helpers.NBTHelper;

import net.minecraftforge.common.ISpecialArmor.ArmorProperties;
import net.minecraftforge.oredict.ShapedOreRecipe;

import net.minecraft.item.EnumArmorMaterial;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.nbt.NBTTagCompound;

public class ArmorHandler {
	
	private final EnumArmorMaterial material;
	private final String armorLoc;
	private final int type;
	private final String prefix;
	private final String iconLoc;
	private final int itemId;
	private final int itemMEta;
	private String oreName = null;
	
	public ArmorHandler(EnumArmorMaterial material, String iconLoc, String armorLoc, int type, String prefix, int itemID, int itemMeta, String oreName)
	{
		this.material = material;
		this.iconLoc = iconLoc;
		this.armorLoc = armorLoc;
		this.type = type;
		this.prefix = prefix;
		this.itemId = itemID;
		this.itemMEta = itemMeta;
		this.oreName = oreName;
	}
	
	public EnumArmorMaterial getMaterial()
	{
		return this.material;
	}
	
	public String getIconLoc()
	{
		return this.iconLoc;// + "texasjake95core:test";
	}
	
	public String getArmorLoc()
	{
		return this.armorLoc;
	}
	
	public int getType()
	{
		return this.type;
	}
	
	public boolean hasIcon()
	{
		return this.iconLoc != null;
	}
	
	private String getSuffix()
	{
		switch (this.type)
		{
			case 1:
				return "Chest";
			case 2:
				return "Legs";
			case 3:
				return "Boots";
			default:
				return "Helmet";
		}
	}
	
	public NBTTagCompound getDefaultCompound()
	{
		NBTTagCompound nbt = new NBTTagCompound();
		nbt.setInteger("Damage", 0);
		nbt.setString("name", this.getMapKey());
		return nbt;
	}
	
	public String getUnLocName()
	{
		return String.format("txItem.%s_%s", this.prefix, this.getSuffix());
	}
	
	public ArmorProperties getProperties(ItemStack armor)
	{
		return new ArmorProperties(0, this.material.getDamageReductionAmount(this.type) / 25D, this.material.getDurability(this.type) + 1 - NBTHelper.getInt(armor, "damage"));
	}
	
	public String getMapKey()
	{
		return this.prefix + this.getSuffix();
	}
	
	public ShapedOreRecipe createRecipe()
	{
		ItemStack output = new ItemStack(CoreItemHandler.getInstace().getItem("Generic Armor"));
		output.setTagCompound(this.getDefaultCompound());
		switch (this.getType())
		{
			case 0:
				return new ShapedOreRecipe(output.copy(), new String[] { "xxx", "x x" }, 'x', oreName != null ? oreName : new ItemStack(itemId, 1, itemMEta));
			case 1:
				return new ShapedOreRecipe(output.copy(), new String[] { "x x", "xxx", "xxx" }, 'x', oreName != null ? oreName : new ItemStack(itemId, 1, itemMEta));
			case 2:
				return new ShapedOreRecipe(output.copy(), new String[] { "xxx", "x x", "x x" }, 'x', oreName != null ? oreName : new ItemStack(itemId, 1, itemMEta));
			case 3:
				return new ShapedOreRecipe(output.copy(), new String[] { "x x", "x x" }, 'x', oreName != null ? oreName : new ItemStack(itemId, 1, itemMEta));
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public static void initRecipes()
	{
		for (ArmorHandler handler : ItemGenericArmor.armorHandlers.values())
		{
			if (handler.createRecipe() != null)
				CraftingManager.getInstance().getRecipeList().add(handler.createRecipe());
		}
	}
}
