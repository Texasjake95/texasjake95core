package com.texasjake95.core;

import java.util.List;
import java.util.TreeMap;

import com.texasjake95.base.helpers.NBTHelper;

import net.minecraftforge.common.ISpecialArmor;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumArmorMaterial;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;
import net.minecraft.util.Icon;
import net.minecraft.world.World;

public class ItemGenericArmor extends ItemArmor implements ISpecialArmor {
	
	public static final TreeMap<String, ArmorHandler> armorHandlers = new TreeMap<String, ArmorHandler>();
	private static final TreeMap<String, Icon> icons = new TreeMap<String, Icon>();
	
	public ItemGenericArmor(int par1)
	{
		super(par1, EnumArmorMaterial.IRON, Item.helmetDiamond.renderIndex, 0);
		this.setMaxStackSize(1);
		this.setTextureName("NUUUULLLLL");
	}
	
	public boolean isDamageable()
	{
		return true;
	}
	
	@Override
	public int getMaxDamage(ItemStack stack)
	{
		return 100;
	}
	
	public String getUnlocalizedName(ItemStack par1ItemStack)
	{
		return hasHandler(par1ItemStack) ? getHandler(par1ItemStack).getUnLocName() : "";
	}
	
	@Override
	public ArmorProperties getProperties(EntityLivingBase player, ItemStack armor, DamageSource source, double damage, int slot)
	{
		return getHandler(armor).getProperties(armor);
	}
	
	@Override
	public int getArmorDisplay(EntityPlayer player, ItemStack armor, int slot)
	{
		return hasHandler(armor) ? getHandler(armor).getMaterial().getDamageReductionAmount(getHandler(armor).getType()) : 0;
	}
	
	@Override
	public void damageArmor(EntityLivingBase entity, ItemStack stack, DamageSource source, int damage, int slot)
	{
		int itemDamage = NBTHelper.getInt(stack, "Damage");
		itemDamage += damage;
		if (getHandler(stack).getMaterial().getDurability(getHandler(stack).getType()) <= itemDamage)
			stack.stackSize = 0;
		NBTHelper.setInteger(stack, "Damage", itemDamage);
		stack.setItemDamage((int) ((((float) itemDamage / (float) this.getTrueMaxDamage(stack))) * 100));
	}
	
	@Override
	public Icon getIconIndex(ItemStack stack)
	{
		return icons.containsKey(NBTHelper.getString(stack, "name")) ? icons.get(NBTHelper.getString(stack, "name")) : this.itemIcon;
	}
	
	@Override
	public String getArmorTexture(ItemStack stack, Entity entity, int slot, int layer)
	{
		return hasHandler(stack) ? getHandler(stack).getArmorLoc() : null;
	}
	
	public boolean isValidArmor(ItemStack stack, int armorType, Entity entity)
	{
		if (hasHandler(stack))
		{
			return getHandler(stack).getType() == armorType;
		}
		return false;
	}
	
	public int getTrueMaxDamage(ItemStack stack)
	{
		return hasHandler(stack) ? getHandler(stack).getMaterial().getDurability(getHandler(stack).getType()) : 0;
	}
	
	@Override
	public void registerIcons(IconRegister par1IconRegister)
	{
		super.registerIcons(par1IconRegister);
		for (ArmorHandler armorHandler : armorHandlers.values())
			if (armorHandler.hasIcon())
				icons.put(armorHandler.getMapKey(), par1IconRegister.registerIcon(armorHandler.getIconLoc()));
	}
	
	@Override
	public boolean getIsRepairable(ItemStack par1ItemStack, ItemStack par2ItemStack)
	{
		return hasHandler(par1ItemStack) ? getHandler(par1ItemStack).getMaterial().getArmorCraftingMaterial() == par2ItemStack.itemID ? true : false : false;
	}
	
	public static void addArmor(EnumArmorMaterial material, String iconLoc, String armorLoc, int type, String prefix, int itemID, int itemMeta, String oreName)
	{
		ArmorHandler handler = new ArmorHandler(material, iconLoc, armorLoc, type, prefix, itemID, itemMeta, oreName);
		armorHandlers.put(handler.getMapKey(), handler);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void getSubItems(int par1, CreativeTabs par2CreativeTabs, List par3List)
	{
		for (String name : armorHandlers.keySet())
		{
			ItemStack stack = new ItemStack(this);
			stack.setTagCompound(getHandler(name).getDefaultCompound());
			par3List.add(stack);
		}
	}
	
	private ArmorHandler getHandler(String name)
	{
		return armorHandlers.get(name);
	}
	
	public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
	{
		int i = getArmorSlot(par1ItemStack) - 1;
		ItemStack itemstack1 = par3EntityPlayer.getCurrentArmor(i);
		if (itemstack1 == null)
		{
			par3EntityPlayer.setCurrentItemOrArmor(i + 1, par1ItemStack.copy());
			par1ItemStack.stackSize = 0;
		}
		return par1ItemStack;
	}
	
	private boolean hasHandler(String name)
	{
		return armorHandlers.containsKey(name);
	}
	
	private boolean hasHandler(ItemStack stack)
	{
		return hasHandler(NBTHelper.getString(stack, "name"));
	}
	
	private ArmorHandler getHandler(ItemStack stack)
	{
		return getHandler(NBTHelper.getString(stack, "name"));
	}
	
	private int getArmorSlot(ItemStack armor)
	{
		switch (getHandler(armor).getType())
		{
			case 0:
				return 4;
			case 1:
				return 3;
			case 2:
				return 2;
			case 3:
				return 1;
			default:
				return 4;
		}
	}
}
