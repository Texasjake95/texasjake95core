package com.texasjake95.core;

import java.util.HashMap;

import com.texasjake95.base.config.ConfigWriterTemplate;

public class CoreConfigHandler {
	
	private static CoreConfigHandler instance;
	private final HashMap<String, ConfigWriterTemplate> configMap = new HashMap<String, ConfigWriterTemplate>();
	
	public static CoreConfigHandler getInstance()
	{
		if (instance == null)
			instance = new CoreConfigHandler();
		return instance;
	}
	
	public void addConfig(ConfigWriterTemplate configWriterTemplate)
	{
		this.configMap.put(configWriterTemplate.modName(), configWriterTemplate);
	}
	
	/**
	 * Can return null
	 */
	public ConfigWriterTemplate getConfig(String modID)
	{
		ConfigWriterTemplate cw = configMap.get(modID);
		return cw;
	}
}
