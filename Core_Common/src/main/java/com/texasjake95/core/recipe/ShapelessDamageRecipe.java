package com.texasjake95.core.recipe;

import java.util.ArrayList;
import java.util.Iterator;

import com.texasjake95.core.api.item.interfaces.ICraftDamage;

import net.minecraft.block.Block;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.world.World;

public class ShapelessDamageRecipe implements IRecipe {
	
	/** Is a List of ItemStack that composes the recipe. */
	public final ArrayList<ItemStack> recipeItems = new ArrayList<ItemStack>();
	/** Is the ItemStack that you get when craft the recipe. */
	private final ItemStack recipeOutput;
	
	public ShapelessDamageRecipe(ItemStack par1ItemStack, Object... recipe)
	{
		ArrayList<ItemStack> inputFinal = new ArrayList<ItemStack>();
		Object[] inputs = recipe;
		int inputLength = recipe.length;
		for (int i = 0; i < inputLength; ++i)
		{
			Object input = inputs[i];
			if (input instanceof ItemStack)
			{
				inputFinal.add(((ItemStack) input).copy());
			}
			else if (input instanceof Item)
			{
				inputFinal.add(new ItemStack((Item) input));
			}
			else
			{
				if (!(input instanceof Block))
				{
					throw new RuntimeException("Invalid shapeless damage recipe!");
				}
				inputFinal.add(new ItemStack((Block) input));
			}
		}
		recipeItems.addAll(inputFinal);
		recipeOutput = par1ItemStack;
	}
	
	/**
	 * Returns an Item that is the result of this recipe
	 */
	@Override
	public ItemStack getCraftingResult(InventoryCrafting par1InventoryCrafting)
	{
		ArrayList<ItemStack> var2 = new ArrayList<ItemStack>(this.recipeItems);
		int damage = 0;
		for (int var3 = 0; var3 < 3; ++var3)
		{
			for (int var4 = 0; var4 < 3; ++var4)
			{
				ItemStack var5 = par1InventoryCrafting.getStackInRowAndColumn(var4, var3);
				if (var5 != null)
				{
					boolean var6 = false;
					Iterator<ItemStack> var7 = var2.iterator();
					while (var7.hasNext())
					{
						ItemStack var8 = var7.next();
						if (var5.itemID == var8.itemID)
						{
							var6 = true;
							if (var5.getItem() instanceof ICraftDamage)
							{
								if (((ICraftDamage) var5.getItem()).useDamage(var5.getItemDamage()) == true)
								{
									damage = damage + var5.getItemDamage();
									var2.remove(var8);
									break;
								}
								else
								{
									var2.remove(var8);
									break;
								}
							}
							else
							{
								damage = damage + var5.getItemDamage();
								var2.remove(var8);
							}
							break;
						}
					}
					if (!var6)
					{
						return null;
					}
				}
			}
		}
		ItemStack output = new ItemStack(this.recipeOutput.copy().getItem(), this.recipeOutput.copy().stackSize, damage);
		return output.copy();
	}
	
	@Override
	public ItemStack getRecipeOutput()
	{
		return this.recipeOutput;
	}
	
	/**
	 * Returns the size of the recipe area
	 */
	@Override
	public int getRecipeSize()
	{
		return this.recipeItems.size();
	}
	
	@Override
	public boolean matches(InventoryCrafting var1, World var2)
	{
		ArrayList<ItemStack> list = new ArrayList<ItemStack>(this.recipeItems);
		for (int var3 = 0; var3 < 3; ++var3)
		{
			for (int var4 = 0; var4 < 3; ++var4)
			{
				ItemStack var5 = var1.getStackInRowAndColumn(var4, var3);
				if (var5 != null)
				{
					boolean var6 = false;
					Iterator<ItemStack> var7 = list.iterator();
					while (var7.hasNext())
					{
						ItemStack var8 = var7.next();
						if (var5.itemID == var8.itemID)
						{
							var6 = true;
							list.remove(var8);
							break;
						}
					}
					if (!var6)
					{
						return false;
					}
				}
			}
		}
		return list.isEmpty();
	}
}
