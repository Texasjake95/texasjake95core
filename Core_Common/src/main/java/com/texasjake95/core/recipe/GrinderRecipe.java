package com.texasjake95.core.recipe;

import java.util.ArrayList;

import net.minecraftforge.oredict.OreDictionary;

import net.minecraft.item.ItemStack;

public class GrinderRecipe {
	
	private static int nextID = 0;
	private static ArrayList<GrinderRecipe> recipeMap = new ArrayList<GrinderRecipe>();
	private final int id;
	private ItemStack input;
	private ItemStack output;
	
	public GrinderRecipe(ItemStack input, ItemStack output, int id)
	{
		this.input = input;
		this.output = output;
		this.id = id;
	}
	
	public ItemStack getOutput(ItemStack input)
	{
		return doesItemStackMatch(input, true) ? this.output : null;
	}
	
	public int getInputDecrement()
	{
		return this.input.stackSize;
	}
	
	public boolean doesItemStackMatch(ItemStack stack, boolean sizeMatters)
	{
		if (input.itemID != stack.itemID || (input.getItemDamage() != stack.getItemDamage() && input.getItemDamage() != OreDictionary.WILDCARD_VALUE))
			return false;
		return sizeMatters ? stack.stackSize >= this.input.stackSize : true;
	}
	
	public static void addRecipe(ItemStack input, ItemStack output)
	{
		recipeMap.add(new GrinderRecipe(input, output, nextID));
		nextID++;
	}
	
	public static GrinderRecipe getRecipe(ItemStack input)
	{
		for (GrinderRecipe recipe : recipeMap)
		{
			if (recipe.doesItemStackMatch(input, false))
				return recipe;
		}
		return null;
	}
	
	public static GrinderRecipe getRecipe(int id)
	{
		return recipeMap.get(id);
	}
	
	public static int getRecipeID(ItemStack itemStack)
	{
		return getRecipe(itemStack).id;
	}
}
