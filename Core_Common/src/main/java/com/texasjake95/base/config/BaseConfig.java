package com.texasjake95.base.config;

import com.texasjake95.commons.file.config.ConfigFile;

public abstract class BaseConfig {
	
	public ConfigFile config;
	public ConfigWriterTemplate configw;
	public boolean retroGen = false;
	private boolean hasWorldGen = false;
	
	public abstract String modName();
	
	public BaseConfig(ConfigWriterTemplate configw, boolean hasWorldGen)
	{
		this.configw = configw;
		config = configw.config();
		this.hasWorldGen = hasWorldGen;
	}
	
	public void initProps()
	{
		config.load();
		if (this.hasWorldGen)
			retroGen = Boolean.parseBoolean(config.getCatagory("WorldGen").addPropertyWithComment("Retro Gen", false, "Set this to true for the world to retroGen on World load").value);
		config.save();
	}
}
