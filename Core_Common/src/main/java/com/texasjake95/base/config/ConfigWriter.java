package com.texasjake95.base.config;

import java.io.File;
import java.util.Map;
import java.util.logging.Level;

import com.texasjake95.base.helpers.LoggerHelper;
import com.texasjake95.commons.file.config.ConfigFile;
import com.texasjake95.commons.file.config.ConfigProperty;

import net.minecraftforge.common.Configuration;

import net.minecraft.block.Block;
import net.minecraft.item.Item;

public class ConfigWriter {
	
	LoggerHelper SH;
	
	public boolean autoAssign(ConfigFile config, String string)
	{
		config.load();
		config.addPropertyWithComment(ConfigFile.GENERAL, "AutoAssign", true, "Set this to true for the Block and Item IDs to be auto assigned");
		ConfigProperty prop = config.getProperty(ConfigFile.GENERAL, "AutoAssign");
		boolean a = prop.getBoolean(true);
		config.save();
		if (a == true)
		{
			LoggerHelper.getMODLogger(string, "Auto Assign = true", Level.INFO);
		}
		return a;
	}
	
	/**
	 * Adds a new Boolean Config line
	 * 
	 * @param Config
	 *            Name
	 * @param config
	 * @param default value
	 */
	public boolean BooleanConfig(String BooleanName, ConfigFile config, boolean def)
	{
		config.load();
		ConfigProperty prop = config.addProperty(Configuration.CATEGORY_GENERAL, BooleanName, def);
		boolean a = prop.getBoolean(def);
		config.save();
		return a;
	}
	
	/**
	 * Adds a new Config line Used for Booleans and is able to put in new
	 * categories
	 * 
	 * @param Config
	 *            Name
	 * @param config
	 * @param default value
	 * @param cat
	 *            0 = block 1 = item 2 = general 3 = new category
	 */
	public boolean BooleanConfig(String BooleanName, ConfigFile config, boolean def, int cat, String Category)
	{
		config.load();
		String SetCategory = null;
		switch (cat)
		{
			case 0:
				SetCategory = Configuration.CATEGORY_BLOCK;
			case 1:
				SetCategory = Configuration.CATEGORY_ITEM;
			case 2:
				SetCategory = Configuration.CATEGORY_GENERAL;
			case 3:
				SetCategory = Category;
		}
		ConfigProperty prop = config.addProperty(SetCategory, BooleanName, def);
		boolean a = prop.getBoolean(def);
		config.save();
		return a;
	}
	
	public ConfigFile CreateConfig(String confiDir, String modID)
	{
		File file = new File(confiDir + "/texasjake95/" + modID + ".cfg");
		return new ConfigFile(file);
	}
	
	public void endProps(ConfigFile config)
	{
		config.load();
		config.getProperty(ConfigFile.GENERAL, "AutoAssign").set("false");
		config.save();
	}
	
	public int getBlockID(String BlockName, ConfigFile config, int def, boolean autoAssign, Map<String, Integer> register)
	{
		config.load();
		ConfigProperty prop = config.getSubCatagory(new String[] { "IDs" }, "Blocks").addProperty(BlockName, -1);
		int id = Integer.valueOf(prop.value);
		if (id == -1 && !autoAssign)
			return -1;
		if (id == -1)
			id = 0;
		if (Item.itemsList[id] != null && Block.blocksList[id] != null || id == 0)
		{
			if (!autoAssign)
			{
				throw new IllegalArgumentException("Slot " + id + " is already occupied by " + Block.blocksList[id] + "/" + Item.itemsList[id] + " when adding " + BlockName + " please set Auto Assign equal to true or edit the config file");
			}
			if (autoAssign)
			{
				id = -1;
				for (int k = 1; k < Block.blocksList.length; k++)
				{
					if (Item.itemsList[k] == null && Block.blocksList[k] == null)
					{
						id = k;
						break;
					}
				}
				if (id == -1)
				{
					throw new IllegalArgumentException("Too Many Blocks please unistall some mods or unistall this one");
				}
				prop.set(String.valueOf(id));
			}
		}
		config.save();
		register.put(BlockName, id);
		return id;
	}
	
	public int getCreativeTabBlockID(String BlockName, ConfigFile config, boolean autoAssign)
	{
		config.load();
		ConfigProperty prop = config.addProperty(Configuration.CATEGORY_BLOCK, BlockName, 0);
		int id = Integer.valueOf(prop.value);
		if (id == -1 && !autoAssign)
			return -1;
		if (id == -1)
			id = 1;
		if (Item.itemsList[id] != null && Block.blocksList[id] != null || id == 0)
		{
			if (!autoAssign)
			{
				throw new IllegalArgumentException("Slot " + id + " is already occupied by " + Block.blocksList[id] + "/" + Item.itemsList[id] + " when adding " + BlockName + " please set Auto Assign equal to true or edit the config file");
			}
			if (autoAssign)
			{
				id = -1;
				for (int k = 1; k < Block.blocksList.length; k++)
				{
					if (Item.itemsList[k] == null && Block.blocksList[k] == null)
					{
						id = k;
						break;
					}
				}
				if (id == -1)
				{
					throw new IllegalArgumentException("Too Many Blocks please unistall some mods or unistall this one");
				}
				prop.set(String.valueOf(id));
			}
		}
		config.save();
		return id;
	}
	
	public int getCreativeTabItemID(String ItemName, ConfigFile config, boolean autoAssign)
	{
		config.load();
		ConfigProperty prop = config.addProperty(Configuration.CATEGORY_ITEM, ItemName, 0);
		int id = Integer.valueOf(prop.value);
		if (id == -1 && !autoAssign)
			return -1;
		if (id == -1)
			id = 1;
		if (Item.itemsList[id] != null || id == 0)
		{
			if (!autoAssign)
			{
				if (id < Block.blocksList.length)
				{
					throw new IllegalArgumentException("Slot " + id + " is already occupied by " + Block.blocksList[id] + "/" + Item.itemsList[id] + " when adding " + ItemName + " please set Auto Assign equal to true or edit the config file");
				}
				else
				{
					throw new IllegalArgumentException("Slot " + id + " is already occupied by " + Item.itemsList[id] + " when adding " + ItemName + " please set Auto Assign equal to true");
				}
			}
			if (autoAssign)
			{
				id = -1;
				for (int k = Block.blocksList.length; k < Item.itemsList.length; k++)
				{
					if (k < Block.blocksList.length)
					{
						if (Item.itemsList[k] == null && Block.blocksList[k] == null)
						{
							id = k;
							break;
						}
					}
					else
					{
						if (Item.itemsList[k] == null)
						{
							id = k;
							break;
						}
					}
				}
				if (id == -1)
				{
					throw new IllegalArgumentException("Too Many Items please unistall some mods or unistall this one");
				}
				prop.set(String.valueOf(id));
			}
		}
		config.save();
		return id;
	}
	
	public int getItemID(String ItemName, ConfigFile config, int def, boolean autoAssign, Map<String, Integer> register)
	{
		config.load();
		ConfigProperty prop = config.getSubCatagory(new String[] { "IDs" }, "Items").addProperty(ItemName, -1);
		int id = Integer.valueOf(prop.value);
		if (id == -1 && !autoAssign)
			return -1;
		if (id == -1)
			id = 0;
		if (Item.itemsList[id] != null || id == 0)
		{
			if (!autoAssign)
			{
				if (id < Block.blocksList.length && Block.blocksList[id] != null && Item.itemsList[id] != null)
				{
					throw new IllegalArgumentException("Slot " + id + " is already occupied by " + Block.blocksList[id] + "/" + Item.itemsList[id] + " when adding " + ItemName + " please set Auto Assign equal to true or edit the config file");
				}
				else if (Item.itemsList[id] != null)
				{
					throw new IllegalArgumentException("Slot " + id + " is already occupied by " + Item.itemsList[id] + " when adding " + ItemName + " please set Auto Assign equal to true");
				}
			}
			if (autoAssign)
			{
				id = -1;
				for (int k = Block.blocksList.length; k < Item.itemsList.length; k++)
				{
					if (k < Block.blocksList.length)
					{
						if (Item.itemsList[k] == null && Block.blocksList[k] == null)
						{
							id = k;
							break;
						}
					}
					else
					{
						if (Item.itemsList[k] == null)
						{
							id = k;
							break;
						}
					}
				}
				if (id == -1)
				{
					throw new IllegalArgumentException("Too Many Items please unistall some mods or unistall this one");
				}
				prop.set(String.valueOf(id));
			}
		}
		config.save();
		register.put(ItemName, id);
		return id - 256;
	}
}
