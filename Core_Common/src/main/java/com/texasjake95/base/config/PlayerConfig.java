package com.texasjake95.base.config;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map.Entry;

import cpw.mods.fml.common.FMLCommonHandler;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;

public class PlayerConfig {
	
	public static HashMap<String, PlayerConfig> playerSaves = new HashMap<String, PlayerConfig>();
	
	public static void addConfig(EntityPlayer player)
	{
		if (playerSaves.containsKey(player.username) == false)
		{
			new PlayerConfig(player);
		}
	}
	
	public static PlayerConfig getPlayerConfig(EntityPlayer player)
	{
		if (playerSaves.containsKey(player.username) == false)
		{
			new PlayerConfig(player);
		}
		return playerSaves.get(player.username);
	}
	
	public static PlayerConfig getPlayerConfig(String username2)
	{
		if (playerSaves.containsKey(username2) == false)
		{
			new PlayerConfig(username2);
		}
		return playerSaves.get(username2);
	}
	
	public static File getPlayerFile(EntityPlayer player)
	{
		return new File(WorldConfig.getWorldBaseSaveLocation(player.worldObj), "texasjake95/players/" + player.username + ".dat");
	}
	
	public static void removeConfig(EntityPlayer player)
	{
		getPlayerConfig(player).save();
		if (playerSaves.containsKey(player.username) == true)
		{
			playerSaves.remove(player.username);
		}
	}
	
	private boolean isDirty;
	private NBTTagCompound saveCompound;
	private File saveFile;
	private String username;
	
	public PlayerConfig(EntityPlayer player)
	{
		this.username = player.username;
		this.saveFile = getPlayerFile(player);
		if (!this.saveFile.getParentFile().exists())
		{
			this.saveFile.getParentFile().mkdirs();
		}
		playerSaves.put(this.username, this);
		this.load();
	}
	
	public PlayerConfig(String username)
	{
		this.username = username;
		this.saveFile = this.getPlayerFile(username);
		if (!this.saveFile.getParentFile().exists())
		{
			this.saveFile.getParentFile().mkdirs();
		}
		playerSaves.put(username, this);
		this.load();
	}
	
	public boolean getBoolean(String keyName)
	{
		if (!this.saveCompound.hasKey(keyName))
		{
			this.setBoolean(keyName, false);
		}
		return this.saveCompound.getBoolean(keyName);
	}
	
	public byte getByte(String keyName)
	{
		if (!this.saveCompound.hasKey(keyName))
		{
			this.setByte(keyName, (byte) 0);
		}
		return this.saveCompound.getByte(keyName);
	}
	
	public double getDouble(String keyName)
	{
		if (!this.saveCompound.hasKey(keyName))
		{
			this.setDouble(keyName, 0);
		}
		return this.saveCompound.getDouble(keyName);
	}
	
	public float getFloat(String keyName)
	{
		if (!this.saveCompound.hasKey(keyName))
		{
			this.setFloat(keyName, 0);
		}
		return this.saveCompound.getFloat(keyName);
	}
	
	public int getInteger(String keyName)
	{
		if (!this.saveCompound.hasKey(keyName))
		{
			this.setInteger(keyName, 0);
		}
		return this.saveCompound.getInteger(keyName);
	}
	
	public long getLong(String keyName)
	{
		if (!this.saveCompound.hasKey(keyName))
		{
			this.setLong(keyName, 0);
		}
		return this.saveCompound.getLong(keyName);
	}
	
	private File getPlayerFile(String username2)
	{
		return new File(WorldConfig.getWorldBaseSaveLocation(FMLCommonHandler.instance().getMinecraftServerInstance().worldServerForDimension(0)), "texasjake95/players/" + username2 + ".dat");
	}
	
	public short getShort(String keyName)
	{
		if (!this.saveCompound.hasKey(keyName))
		{
			this.setShort(keyName, (short) 0);
		}
		return this.saveCompound.getShort(keyName);
	}
	
	public String getString(String keyName)
	{
		if (!this.saveCompound.hasKey(keyName))
		{
			this.setString(keyName, "");
		}
		return this.saveCompound.getString(keyName);
	}
	
	public String getUsername()
	{
		return this.username;
	}
	
	private void load()
	{
		this.saveCompound = new NBTTagCompound();
		try
		{
			if (!this.saveFile.exists())
			{
				this.saveFile.createNewFile();
			}
			if (this.saveFile.length() > 0)
			{
				DataInputStream din = new DataInputStream(new FileInputStream(this.saveFile));
				this.saveCompound = (NBTTagCompound) NBTBase.readNamedTag(din);
				din.close();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			this.load();
		}
	}
	
	public void save()
	{
		if (!this.isDirty)
		{
			return;
		}
		try
		{
			DataOutputStream dout = new DataOutputStream(new FileOutputStream(this.saveFile));
			NBTBase.writeNamedTag(this.saveCompound, dout);
			dout.close();
			this.isDirty = false;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			this.save();
		}
	}
	
	public void setBoolean(String keyName, boolean keyValue)
	{
		this.saveCompound.setBoolean(keyName, keyValue);
		this.setDirty();
	}
	
	public void setByte(String keyName, byte keyValue)
	{
		this.saveCompound.setByte(keyName, keyValue);
		this.setDirty();
	}
	
	public void setDirty()
	{
		this.isDirty = true;
	}
	
	public void setDouble(String keyName, double keyValue)
	{
		this.saveCompound.setDouble(keyName, keyValue);
		this.setDirty();
	}
	
	public void setFloat(String keyName, float keyValue)
	{
		this.saveCompound.setFloat(keyName, keyValue);
		this.setDirty();
	}
	
	public void setInteger(String keyName, int keyValue)
	{
		this.saveCompound.setInteger(keyName, keyValue);
		this.setDirty();
	}
	
	public void setLong(String keyName, long keyValue)
	{
		this.saveCompound.setLong(keyName, keyValue);
		this.setDirty();
	}
	
	public void setShort(String keyName, short keyValue)
	{
		this.saveCompound.setShort(keyName, keyValue);
		this.setDirty();
	}
	
	public void setString(String keyName, String keyValue)
	{
		this.saveCompound.setString(keyName, keyValue);
		this.setDirty();
	}
	
	public static void saveAll(boolean shouldRemove)
	{
		for (Entry<String, PlayerConfig> entry : playerSaves.entrySet())
		{
			entry.getValue().save();
			if (shouldRemove)
				playerSaves.remove(entry.getKey());
		}
	}
}
