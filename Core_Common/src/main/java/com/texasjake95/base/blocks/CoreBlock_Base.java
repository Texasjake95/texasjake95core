package com.texasjake95.base.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;

public abstract class CoreBlock_Base extends Block {
	
	public CoreBlock_Base(int par1, Material par3Material)
	{
		super(par1, par3Material);
	}
	
	@Override
	public abstract void registerIcons(IconRegister par1IconRegister);
}
