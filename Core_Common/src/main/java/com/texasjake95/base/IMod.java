package com.texasjake95.base;

import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;

public interface IMod {
	
	/***
	 * This is code that is executed prior to your mod being initialized into of
	 * Minecraft Examples of code that could be run here;
	 * 
	 * Initializing your items/blocks (you must do this here) Setting up your
	 * own custom logger for writing log data to Loading your language
	 * translations for your mod (if your mod has translations for other
	 * languages) Registering your mod's key bindings and sounds
	 * 
	 * @param event
	 *            The Forge ModLoader pre-initialization event
	 */
	@EventHandler
	public void preload(FMLPreInitializationEvent event);
	
	/***
	 * This is code that is executed when your mod is being initialized in
	 * Minecraft Examples of code that could be run here;
	 * 
	 * Registering your GUI Handler Registering your different event listeners
	 * Registering your different tile entities Adding in any recipes you have
	 * 
	 * @param event
	 *            The Forge ModLoader initialization event
	 */
	@EventHandler
	public void load(FMLInitializationEvent event);
	
	/***
	 * This is code that is executed after all mods are initialized in Minecraft
	 * This is a good place to execute code that interacts with other mods (ie,
	 * loads an addon module of your mod if you find a particular mod).
	 * 
	 * @param event
	 *            The Forge ModLoader post-initialization event
	 */
	@EventHandler
	public void modsLoaded(FMLPostInitializationEvent event);
	
	public String modID();
}
