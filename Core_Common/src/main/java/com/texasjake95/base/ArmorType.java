package com.texasjake95.base;

public enum ArmorType
{
	helmet(0), plate(1), legs(2), boots(3);
	
	private int slot;
	
	ArmorType(int slot)
	{
		this.slot = slot;
	}
	
	public int getSlot()
	{
		return this.slot;
	}
}
