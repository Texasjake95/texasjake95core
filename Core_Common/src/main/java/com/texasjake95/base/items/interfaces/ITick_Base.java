package com.texasjake95.base.items.interfaces;

import net.minecraft.entity.player.EntityPlayer;

public interface ITick_Base {
	
	public void doTickEvent(EntityPlayer thePlayer);
}
