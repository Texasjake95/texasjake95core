package com.texasjake95.base.items;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.EnumToolMaterial;
import net.minecraft.item.ItemTool;

public abstract class CoreItemTool_Base extends ItemTool {
	
	public CoreItemTool_Base(int ID, float damageMod, EnumToolMaterial toolMaterial, Block[] blocksEffectiveAgainst)
	{
		super(ID, damageMod, toolMaterial, blocksEffectiveAgainst);
	}
	
	@Override
	public abstract void registerIcons(IconRegister par1IconRegister);
}
