package com.texasjake95.base.items;

import com.texasjake95.base.items.interfaces.IPickaxe;

import net.minecraft.item.EnumToolMaterial;
import net.minecraft.item.ItemPickaxe;

public abstract class CoreItemPickaxe_Base extends ItemPickaxe implements IPickaxe {
	
	public CoreItemPickaxe_Base(int par1, int texture, EnumToolMaterial par2EnumToolMaterial)
	{
		super(par1, par2EnumToolMaterial);
	}
	
	@Override
	public int power()
	{
		return this.toolMaterial.getHarvestLevel();
	}
}
