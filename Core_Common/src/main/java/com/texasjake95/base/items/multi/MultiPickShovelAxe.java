package com.texasjake95.base.items.multi;

import com.texasjake95.base.items.CoreItemTool_Base;
import com.texasjake95.base.items.interfaces.IAxe;
import com.texasjake95.base.items.interfaces.IPickaxe;
import com.texasjake95.base.items.interfaces.IShovel;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.EnumToolMaterial;
import net.minecraft.item.ItemStack;

public abstract class MultiPickShovelAxe extends CoreItemTool_Base implements IPickaxe, IAxe, IShovel {
	
	public static final Block[] blocksEffectiveAgainst = new Block[] { Block.planks, Block.bookShelf, Block.wood, Block.chest, Block.stoneDoubleSlab, Block.stoneSingleSlab, Block.pumpkin, Block.pumpkinLantern, Block.grass, Block.dirt, Block.sand, Block.gravel, Block.snow, Block.blockSnow, Block.blockClay, Block.tilledField, Block.slowSand, Block.mycelium, Block.cobblestone, Block.stoneDoubleSlab, Block.stoneSingleSlab, Block.stone, Block.sandStone, Block.cobblestoneMossy, Block.oreIron, Block.blockIron, Block.oreCoal, Block.blockGold, Block.oreGold, Block.oreDiamond, Block.blockDiamond, Block.ice, Block.netherrack, Block.oreLapis, Block.blockLapis, Block.oreRedstone, Block.oreRedstoneGlowing, Block.rail, Block.railDetector, Block.railPowered };
	
	public MultiPickShovelAxe(int par1, EnumToolMaterial par3EnumToolMaterial)
	{
		super(par1, 3, par3EnumToolMaterial, blocksEffectiveAgainst);
		this.toolMaterial = par3EnumToolMaterial;
		this.maxStackSize = 1;
		this.setMaxDamage(par3EnumToolMaterial.getMaxUses());
	}
	
	@Override
	public boolean canHarvestBlock(Block par1Block)
	{
		return par1Block == Block.blockSnow ? this.toolMaterial.getHarvestLevel() >= 0 : par1Block == Block.obsidian ? this.toolMaterial.getHarvestLevel() == 3 : par1Block != Block.blockDiamond && par1Block != Block.oreDiamond ? par1Block == Block.oreEmerald ? this.toolMaterial.getHarvestLevel() >= 2 : par1Block != Block.blockGold && par1Block != Block.oreGold ? par1Block != Block.blockIron && par1Block != Block.oreIron ? par1Block != Block.blockLapis && par1Block != Block.oreLapis ? par1Block != Block.oreRedstone && par1Block != Block.oreRedstoneGlowing ? par1Block.blockMaterial == Material.rock ? true : par1Block.blockMaterial == Material.iron : this.toolMaterial.getHarvestLevel() >= 2 : this.toolMaterial.getHarvestLevel() >= 1 : this.toolMaterial.getHarvestLevel() >= 1 : this.toolMaterial.getHarvestLevel() >= 2 : this.toolMaterial.getHarvestLevel() >= 2;
	}
	
	/**
	 * Returns the strength of the stack against a given block. 1.0F base,
	 * (Quality+1)*2 if correct blocktype, 1.5F if sword
	 */
	@Override
	public float getStrVsBlock(ItemStack par1ItemStack, Block par2Block)
	{
		return par2Block != null && (par2Block.blockMaterial == Material.iron || par2Block.blockMaterial == Material.rock || par2Block.blockMaterial == Material.wood) ? this.efficiencyOnProperMaterial : super.getStrVsBlock(par1ItemStack, par2Block);
	}
	
	@Override
	public int power()
	{
		return this.toolMaterial.getHarvestLevel();
	}
}
