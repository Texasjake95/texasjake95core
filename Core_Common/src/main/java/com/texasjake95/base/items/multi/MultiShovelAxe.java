package com.texasjake95.base.items.multi;

import com.texasjake95.base.items.CoreItemTool_Base;
import com.texasjake95.base.items.interfaces.IAxe;
import com.texasjake95.base.items.interfaces.IShovel;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.EnumToolMaterial;
import net.minecraft.item.ItemStack;

public abstract class MultiShovelAxe extends CoreItemTool_Base implements IShovel, IAxe {
	
	public static final Block[] blocksEffectiveAgainst = new Block[] { Block.planks, Block.bookShelf, Block.wood, Block.chest, Block.stoneDoubleSlab, Block.stoneSingleSlab, Block.pumpkin, Block.pumpkinLantern, Block.grass, Block.dirt, Block.sand, Block.gravel, Block.snow, Block.blockSnow, Block.blockClay, Block.tilledField, Block.slowSand, Block.mycelium };
	
	public MultiShovelAxe(int par1, EnumToolMaterial par2EnumToolMaterial)
	{
		super(par1, 3, par2EnumToolMaterial, blocksEffectiveAgainst);
	}
	
	/**
	 * Returns if the item (tool) can harvest results from the block type.
	 */
	@Override
	public boolean canHarvestBlock(Block par1Block)
	{
		return par1Block == Block.snow ? true : par1Block == Block.blockSnow;
	}
	
	@Override
	public float getStrVsBlock(ItemStack par1ItemStack, Block par2Block)
	{
		return par2Block != null && par2Block.blockMaterial == Material.wood ? this.efficiencyOnProperMaterial : super.getStrVsBlock(par1ItemStack, par2Block);
	}
	
	@Override
	public int power()
	{
		return this.toolMaterial.getHarvestLevel();
	}
}
