package com.texasjake95.base.items.interfaces;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

public interface IKeyBound_Base {
	
	public void doKeyBindingAction(EntityPlayer thePlayer, ItemStack currentItem, String key);
}
