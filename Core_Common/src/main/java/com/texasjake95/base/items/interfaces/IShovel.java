package com.texasjake95.base.items.interfaces;

public interface IShovel {
	
	public int power();
}
