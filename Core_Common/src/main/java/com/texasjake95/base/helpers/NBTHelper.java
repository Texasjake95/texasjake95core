package com.texasjake95.base.helpers;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

/**
 * NBTHelper
 * 
 * Helper methods for manipulating NBT data on ItemStacks
 * 
 * @author pahimar
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 * 
 */
public class NBTHelper {
	
	public static NBTTagCompound getCompound(ItemStack stack)
	{
		initNBTTagCompound(stack);
		return stack.stackTagCompound;
	}
	
	// boolean
	public static boolean getBoolean(ItemStack itemStack, String keyName)
	{
		initNBTTagCompound(itemStack);
		if (!itemStack.stackTagCompound.hasKey(keyName))
		{
			setBoolean(itemStack, keyName, false);
		}
		return itemStack.stackTagCompound.getBoolean(keyName);
	}
	
	// byte
	public static byte getByte(ItemStack itemStack, String keyName)
	{
		initNBTTagCompound(itemStack);
		if (!itemStack.stackTagCompound.hasKey(keyName))
		{
			setByte(itemStack, keyName, (byte) 0);
		}
		return itemStack.stackTagCompound.getByte(keyName);
	}
	
	// double
	public static double getDouble(ItemStack itemStack, String keyName)
	{
		initNBTTagCompound(itemStack);
		if (!itemStack.stackTagCompound.hasKey(keyName))
		{
			setDouble(itemStack, keyName, 0);
		}
		return itemStack.stackTagCompound.getDouble(keyName);
	}
	
	// float
	public static float getFloat(ItemStack itemStack, String keyName)
	{
		initNBTTagCompound(itemStack);
		if (!itemStack.stackTagCompound.hasKey(keyName))
		{
			setFloat(itemStack, keyName, 0);
		}
		return itemStack.stackTagCompound.getFloat(keyName);
	}
	
	// int
	public static int getInt(ItemStack itemStack, String keyName)
	{
		initNBTTagCompound(itemStack);
		if (!itemStack.stackTagCompound.hasKey(keyName))
		{
			setInteger(itemStack, keyName, 0);
		}
		return itemStack.stackTagCompound.getInteger(keyName);
	}
	
	// long
	public static long getLong(ItemStack itemStack, String keyName)
	{
		initNBTTagCompound(itemStack);
		if (!itemStack.stackTagCompound.hasKey(keyName))
		{
			setLong(itemStack, keyName, 0);
		}
		return itemStack.stackTagCompound.getLong(keyName);
	}
	
	// short
	public static short getShort(ItemStack itemStack, String keyName)
	{
		initNBTTagCompound(itemStack);
		if (!itemStack.stackTagCompound.hasKey(keyName))
		{
			setShort(itemStack, keyName, (short) 0);
		}
		return itemStack.stackTagCompound.getShort(keyName);
	}
	
	// String
	public static String getString(ItemStack itemStack, String keyName)
	{
		initNBTTagCompound(itemStack);
		if (!itemStack.stackTagCompound.hasKey(keyName))
		{
			setString(itemStack, keyName, "");
		}
		return itemStack.stackTagCompound.getString(keyName);
	}
	
	public static NBTTagCompound getNBTTags(ItemStack itemStack, String keyName)
	{
		initNBTTagCompound(itemStack);
		return itemStack.stackTagCompound.getCompoundTag(keyName);
	}
	
	public static void setNBTTags(ItemStack itemStack, NBTTagCompound tags, String keyName)
	{
		initNBTTagCompound(itemStack);
		itemStack.stackTagCompound.setCompoundTag(keyName, tags);
	}
	
	/**
	 * Initializes the NBT Tag Compound for the given ItemStack if it is null
	 * 
	 * @param itemStack
	 *            The ItemStack for which its NBT Tag Compound is being checked
	 *            for initialization
	 */
	private static void initNBTTagCompound(ItemStack itemStack)
	{
		if (itemStack.stackTagCompound == null)
		{
			itemStack.setTagCompound(new NBTTagCompound());
		}
	}
	
	public static void setBoolean(ItemStack itemStack, String keyName, boolean keyValue)
	{
		initNBTTagCompound(itemStack);
		itemStack.stackTagCompound.setBoolean(keyName, keyValue);
	}
	
	public static void setByte(ItemStack itemStack, String keyName, byte keyValue)
	{
		initNBTTagCompound(itemStack);
		itemStack.stackTagCompound.setByte(keyName, keyValue);
	}
	
	public static void setDouble(ItemStack itemStack, String keyName, double keyValue)
	{
		initNBTTagCompound(itemStack);
		itemStack.stackTagCompound.setDouble(keyName, keyValue);
	}
	
	public static void setFloat(ItemStack itemStack, String keyName, float keyValue)
	{
		initNBTTagCompound(itemStack);
		itemStack.stackTagCompound.setFloat(keyName, keyValue);
	}
	
	public static void setInteger(ItemStack itemStack, String keyName, int keyValue)
	{
		initNBTTagCompound(itemStack);
		itemStack.stackTagCompound.setInteger(keyName, keyValue);
	}
	
	public static void setLong(ItemStack itemStack, String keyName, long keyValue)
	{
		initNBTTagCompound(itemStack);
		itemStack.stackTagCompound.setLong(keyName, keyValue);
	}
	
	public static void setShort(ItemStack itemStack, String keyName, short keyValue)
	{
		initNBTTagCompound(itemStack);
		itemStack.stackTagCompound.setShort(keyName, keyValue);
	}
	
	public static void setString(ItemStack itemStack, String keyName, String keyValue)
	{
		initNBTTagCompound(itemStack);
		itemStack.stackTagCompound.setString(keyName, keyValue);
	}
	
	// boolean
	public static boolean getBoolean(NBTTagCompound tags, String keyName)
	{
		if (!tags.hasKey(keyName))
		{
			setBoolean(tags, keyName, false);
		}
		return tags.getBoolean(keyName);
	}
	
	// byte
	public static byte getByte(NBTTagCompound tags, String keyName)
	{
		if (!tags.hasKey(keyName))
		{
			setByte(tags, keyName, (byte) 0);
		}
		return tags.getByte(keyName);
	}
	
	// double
	public static double getDouble(NBTTagCompound tags, String keyName)
	{
		if (!tags.hasKey(keyName))
		{
			setDouble(tags, keyName, 0);
		}
		return tags.getDouble(keyName);
	}
	
	// float
	public static float getFloat(NBTTagCompound tags, String keyName)
	{
		if (!tags.hasKey(keyName))
		{
			setFloat(tags, keyName, 0);
		}
		return tags.getFloat(keyName);
	}
	
	// int
	public static int getInt(NBTTagCompound tags, String keyName)
	{
		if (!tags.hasKey(keyName))
		{
			setInteger(tags, keyName, 0);
		}
		return tags.getInteger(keyName);
	}
	
	// long
	public static long getLong(NBTTagCompound tags, String keyName)
	{
		if (!tags.hasKey(keyName))
		{
			setLong(tags, keyName, 0);
		}
		return tags.getLong(keyName);
	}
	
	// short
	public static short getShort(NBTTagCompound tags, String keyName)
	{
		if (!tags.hasKey(keyName))
		{
			setShort(tags, keyName, (short) 0);
		}
		return tags.getShort(keyName);
	}
	
	// String
	public static String getString(NBTTagCompound tags, String keyName)
	{
		if (!tags.hasKey(keyName))
		{
			setString(tags, keyName, "");
		}
		return tags.getString(keyName);
	}
	
	public static NBTTagCompound getNBTTags(NBTTagCompound tags, String keyName)
	{
		return tags.getCompoundTag(keyName);
	}
	
	public static void setNBTTags(NBTTagCompound tags, NBTTagCompound tags1, String keyName)
	{
		tags.setCompoundTag(keyName, tags1);
	}
	
	public static void setBoolean(NBTTagCompound tags, String keyName, boolean keyValue)
	{
		tags.setBoolean(keyName, keyValue);
	}
	
	public static void setByte(NBTTagCompound tags, String keyName, byte keyValue)
	{
		tags.setByte(keyName, keyValue);
	}
	
	public static void setDouble(NBTTagCompound tags, String keyName, double keyValue)
	{
		tags.setDouble(keyName, keyValue);
	}
	
	public static void setFloat(NBTTagCompound tags, String keyName, float keyValue)
	{
		tags.setFloat(keyName, keyValue);
	}
	
	public static void setInteger(NBTTagCompound tags, String keyName, int keyValue)
	{
		tags.setInteger(keyName, keyValue);
	}
	
	public static void setLong(NBTTagCompound tags, String keyName, long keyValue)
	{
		tags.setLong(keyName, keyValue);
	}
	
	public static void setShort(NBTTagCompound tags, String keyName, short keyValue)
	{
		tags.setShort(keyName, keyValue);
	}
	
	public static void setString(NBTTagCompound tags, String keyName, String keyValue)
	{
		tags.setString(keyName, keyValue);
	}
}