package com.texasjake95.base.helpers;

import java.util.logging.Level;
import java.util.logging.Logger;

import cpw.mods.fml.common.FMLLog;

public class LoggerHelper {
	
	private static final String CORE = "Texasjake95Core";
	private static Logger TXJAKELogger = Logger.getLogger("Texasjake95");
	
	private static String CORELoggerMessage()
	{
		return "[" + CORE + "] ";
	}
	
	public static void getCoreLogger(String Message, Level level)
	{
		TXJAKELogger.log(level, CORELoggerMessage() + Message);
	}
	
	public static void getMODLogger(String ModName, String Message, Level level)
	{
		TXJAKELogger.log(level, MODLoggerMessage(ModName) + Message);
	}
	
	public static void initLogger()
	{
		TXJAKELogger.setParent(FMLLog.getLogger());
	}
	
	private static String MODLoggerMessage(String ModName)
	{
		return "[" + ModName + "] ";
	}
}
