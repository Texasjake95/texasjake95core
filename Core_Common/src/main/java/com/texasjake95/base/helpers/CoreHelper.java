package com.texasjake95.base.helpers;

import java.util.Map;

import com.google.common.collect.Lists;
import com.texasjake95.api.RegisteredBlocks;
import com.texasjake95.api.RegisteredItems;
import com.texasjake95.base.ArmorType;
import com.texasjake95.base.items.interfaces.IAxe;
import com.texasjake95.base.items.interfaces.IPickaxe;
import com.texasjake95.base.items.interfaces.IShovel;
import com.texasjake95.base.items.interfaces.ITool;
import com.texasjake95.commons.util.CustomArrayList;
import com.texasjake95.core.WorldGeneratorHandler;
import com.texasjake95.core.recipe.ShapelessDamageRecipe;

import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.IWorldGenerator;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.oredict.ShapedOreRecipe;
import net.minecraftforge.oredict.ShapelessOreRecipe;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemAxe;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemPickaxe;
import net.minecraft.item.ItemSpade;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.DamageSource;

public class CoreHelper {
	
	public static class ItemRegister {
		
		public static void DEBUG()
		{
			RegisteredBlocks.printBlockTags();
			RegisteredItems.printItemTags();
		}
		
		public static void RegisterAll(String ModID, Map<String, Integer> ItemRegister, Map<String, Integer> BlockRegister)
		{
			if (ItemRegister != null)
			{
				for (String Names : ItemRegister.keySet())
				{
					int a = ItemRegister.get(Names);
					RegisterItem(ModID, Names, Item.itemsList[a]);
				}
			}
			if (BlockRegister != null)
			{
				for (String Names : BlockRegister.keySet())
				{
					int a = BlockRegister.get(Names);
					RegisterBlock(ModID, Names, Block.blocksList[a]);
				}
			}
		}
		
		public static void RegisterBlock(String ModID, String BlockName, Block block)
		{
			RegisteredBlocks.registerBlock(ModID, BlockName, block);
		}
		
		public static void RegisterItem(String ModID, String ItemName, Item item)
		{
			RegisteredItems.registerItem(ModID, ItemName, item);
		}
	}
	
	public static class RecipeHelper {
		
		/**
		 * Add a Forge ShapedOre Recipe
		 * 
		 * @param itemstack
		 * @param recipe
		 */
		@SuppressWarnings("unchecked")
		public static void addForgeOre(ItemStack itemstack, Object... recipe)
		{
			CraftingManager.getInstance().getRecipeList().add(new ShapedOreRecipe(itemstack, recipe));
		}
		
		/**
		 * Add a Forge ShapelessOre Recipe
		 * 
		 * @param itemstack
		 * @param recipe
		 */
		@SuppressWarnings("unchecked")
		public static void addForgeOreShapeless(ItemStack itemstack, Object... recipe)
		{
			CraftingManager.getInstance().getRecipeList().add(new ShapelessOreRecipe(itemstack, recipe));
		}
		
		/**
		 * Add a Meta Sensitive Smelting Recipe
		 * 
		 * @param itemID
		 * @param meta
		 * @param itemstack
		 */
		public static void addMetaSmelt(int itemID, int meta, ItemStack itemstack)
		{
			FurnaceRecipes.smelting().addSmelting(itemID, itemstack, meta);
		}
		
		/**
		 * Add a Shaped Recipe
		 * 
		 * @param output
		 * @param params
		 */
		public static void addRecipe(ItemStack output, Object... params)
		{
			CraftingManager.getInstance().addRecipe(output, params);
		}
		
		/**
		 * Add a Shapeless Recipe that is Damage Sensitive
		 * 
		 * @param output
		 * @param params
		 */
		@SuppressWarnings("unchecked")
		public static void addShapelessDamage(ItemStack output, Object... recipe)
		{
			CustomArrayList<Object> recipe2 = new CustomArrayList<Object>();
			if (output == null)
				return;
			for (Object o : recipe)
			{
				if (o == null)
					return;
				if (o instanceof ComItmStk)
					recipe2.add(((ComItmStk) o).getItemStack());
				else
					recipe2.add(o);
			}
			CraftingManager.getInstance().getRecipeList().add(new ShapelessDamageRecipe(output, recipe2.toArray(new Object[0])));
			// FMLInterModComms.sendMessage(Reference.MOD_ID,
			// InterModComms.ADD_RECIPE, NBTHelper.encodeRecipeAsNBT(output,
			// Arrays.asList(recipe2.toArray(new Object[0]))));
		}
		
		/**
		 * Add a Shapeless Recipe
		 * 
		 * @param output
		 * @param params
		 */
		public static void addShapelessRecipe(ItemStack output, Object... params)
		{
			CraftingManager.getInstance().addShapelessRecipe(output, params);
		}
		
		public static void addShapelessDamage(ComItmStk comItmStk, Object... recipe)
		{
			if (comItmStk.getID() == -1)
				return;
			for (Object o : recipe)
				if (o instanceof ComItmStk)
					if (((ComItmStk) o).getID() == -1)
						return;
			addShapelessDamage(comItmStk.getItemStack(), recipe);
		}
	}
	
	public static class RegisterHelper {
		
		public static void registerBlock(Block block)
		{
			registerBlock(block, ItemBlock.class, block.getUnlocalizedName());
		}
		
		public static void registerWorldGen(String modName, IWorldGenerator worldGen)
		{
			GameRegistry.registerWorldGenerator(worldGen);
			WorldGeneratorHandler.addWorldGen(modName, worldGen);
		}
		
		public static void registerBlock(Block block, Class<? extends ItemBlock> itemclass, String name)
		{
			GameRegistry.registerBlock(block, itemclass, name);
		}
		
		public static void registerOre(String string, ItemStack itemstack)
		{
			OreDictionary.registerOre(string, itemstack);
		}
		
		public static void registerTool(Item item)
		{
			if (item != null)
			{
				if (item instanceof ITool)
				{
					for (String toolClass : ((ITool) item).getToolClasses())
					{
						System.out.println(String.format("Registering Item Id %s with hardness of %s for %s", item.itemID, ((ITool) item).getPower(toolClass), toolClass));
						MinecraftForge.setToolClass(item, toolClass, ((ITool) item).getPower(toolClass));
					}
				}
				if (item instanceof IPickaxe)
				{
					MinecraftForge.setToolClass(item, "pickaxe", ((IPickaxe) item).power());
				}
				if (item instanceof IAxe)
				{
					MinecraftForge.setToolClass(item, "axe", ((IAxe) item).power());
				}
				if (item instanceof IShovel)
				{
					MinecraftForge.setToolClass(item, "shovel", ((IShovel) item).power());
				}
			}
		}
		
		public static void registerTool(Object item, int power)
		{
			if (item instanceof ItemPickaxe || item instanceof IPickaxe)
			{
				MinecraftForge.setToolClass((Item) item, "pickaxe", power);
			}
			if (item instanceof ItemAxe || item instanceof IAxe)
			{
				MinecraftForge.setToolClass((Item) item, "axe", power);
			}
			if (item instanceof ItemSpade || item instanceof IShovel)
			{
				MinecraftForge.setToolClass((Item) item, "shovel", power);
			}
		}
	}
	
	public static boolean canArmorStop(DamageSource source, DamageSource[] immune)
	{
		return Lists.newArrayList(immune).contains(source);
	}
	
	public static ItemStack getEntityArmor(EntityLivingBase entity, ArmorType type)
	{
		if (entity instanceof EntityPlayer)
		{
			return ((EntityPlayer) entity).inventory.armorItemInSlot(type.getSlot());
		}
		else
		{
			return entity.getCurrentItemOrArmor(type.getSlot() + 1);
		}
	}
	
	public static ItemStack getEntityCurrentItem(EntityLivingBase entity)
	{
		if (entity instanceof EntityPlayer)
		{
			return ((EntityPlayer) entity).inventory.getCurrentItem();
		}
		else
		{
			return entity.getCurrentItemOrArmor(0);
		}
	}
	
	@SideOnly(Side.CLIENT)
	private static boolean is_client()
	{
		return true;
	}
	
	@SideOnly(Side.SERVER)
	private static boolean is_server()
	{
		return true;
	}
	
	public static boolean isClient()
	{
		try
		{
			return is_client();
		}
		catch (NoSuchMethodError e)
		{
			return false;
		}
	}
	
	public static boolean isServer()
	{
		try
		{
			return is_server();
		}
		catch (NoSuchMethodError e)
		{
			return false;
		}
	}
	
	public static Minecraft mc()
	{
		if (isClient())
		{
			return FMLClientHandler.instance().getClient();
		}
		return null;
	}
	
	public static MinecraftServer mcs()
	{
		if (isServer())
		{
			return FMLCommonHandler.instance().getMinecraftServerInstance();
		}
		return null;
	}
}
