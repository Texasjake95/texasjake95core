package com.texasjake95.base.helpers;

import java.util.ArrayList;
import java.util.Random;

import com.texasjake95.core.Texasjake95Core;

import net.minecraftforge.common.ForgeDirection;
import net.minecraftforge.oredict.OreDictionary;

import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.gen.feature.WorldGenerator;

public class TXWorldGenerator extends WorldGenerator {
	
	private int minableBlockId;
	private int minableMetaId;
	private int numberOfBlocks;
	private int blockIDReplace;
	private int blockMetaReplace;
	private int chance;
	private int spread;
	private boolean useOreDict = false;
	
	public TXWorldGenerator(int blockId, int meta, int numberofBlocks, int blockIDReplace, int chance)
	{
		this(blockId, meta, numberofBlocks, blockIDReplace, 0, chance, 4);
	}
	
	public TXWorldGenerator(int blockId, int meta, int numberofBlocks, int blockIDReplace, int chance, int spread)
	{
		this(blockId, meta, numberofBlocks, blockIDReplace, 0, chance, spread);
	}
	
	public TXWorldGenerator(int blockId, int meta, int numberofBlocks, int blockIDReplace, int blockMetaReplace, int chance, int spread)
	{
		super(true);
		this.minableBlockId = blockId;
		this.minableMetaId = meta;
		this.numberOfBlocks = numberofBlocks;
		this.blockIDReplace = blockIDReplace;
		this.blockMetaReplace = blockMetaReplace;
		this.chance = chance;
		this.spread = spread;
	}
	
	@Override
	public boolean generate(World world, Random random, int x, int y, int z)
	{
		int chunkX = x >> 4;
		int chunkZ = z >> 4;
		int chunkxB = Math.abs(x - chunkX * 16);
		int yUpper = 256 - y;
		int chunkzB = Math.abs(z - chunkZ * 16);
		int xSpreadUpper = this.getUpperSpread(chunkxB);
		int xSpreadLower = this.getLowerSpread(chunkxB);
		int ySpreadUpper = yUpper >= this.spread ? this.spread : yUpper;
		int ySpreadLower = y >= this.spread ? this.spread : y;
		int zSpreadUpper = this.getUpperSpread(chunkzB);
		int zSpreadLower = this.getLowerSpread(chunkzB);
		if (Texasjake95Core.isTesting)
		{
			System.out.println("X Center = " + chunkxB);
			System.out.println("Z Center = " + chunkzB);
			System.out.println("Before check: X Spread Upper = " + xSpreadUpper);
			System.out.println("Before check: X Spread Lower = " + xSpreadLower);
			System.out.println("Before check: Z Spread Upper = " + zSpreadUpper);
			System.out.println("Before check: Z Spread Lower = " + zSpreadLower);
		}
		if (xSpreadUpper + chunkxB > 15)
			xSpreadUpper = 15 - chunkxB;
		if (zSpreadUpper + chunkzB > 15)
			zSpreadUpper = 15 - chunkzB;
		if (Texasjake95Core.isTesting)
		{
			System.out.println("After check: X Spread Upper = " + xSpreadUpper);
			System.out.println("After check: X Spread Lower = " + xSpreadLower);
			System.out.println("After check: Z Spread Upper = " + zSpreadUpper);
			System.out.println("After check: Z Spread Lower = " + zSpreadLower);
		}
		if (!(this.chance <= 0) && random.nextInt(this.chance) == 0)
		{
			for (int _ = 0; _ < this.numberOfBlocks; _++)
			{
				int newX = this.getAppropriateCoord(x, xSpreadUpper, xSpreadLower, random);
				int newY = this.getAppropriateCoord(y, ySpreadUpper, ySpreadLower, random);
				int newZ = this.getAppropriateCoord(z, zSpreadUpper, zSpreadLower, random);
				if (this.checkForValidSpot(world, newX, newY, newZ))
				{
					if (Texasjake95Core.isTesting)
					{
						System.out.println(String.format("Attemping to place block at %s, %s, %s chunk located at %s, %s", x, y, z, x >> 4, z >> 4));
						System.out.println("Chunk exists = " + world.doChunksNearChunkExist(x, y, z, 0));
					}
					this.setBlockAndMetadata(world, newX, newY, newZ, this.minableBlockId, this.minableMetaId);
				}
			}
		}
		return true;
	}
	
	private int getUpperSpread(int chunkCoord)
	{
		if (chunkCoord >= this.spread)
			return this.spread;
		return chunkCoord;
	}
	
	private int getLowerSpread(int chunkCoord)
	{
		if ((chunkCoord - this.spread) >= 0)
			return this.spread;
		return chunkCoord;
	}
	
	private int getAppropriateCoord(int coord, int spreadUpper, int spreadLower, Random random)
	{
		int change;
		if (spreadUpper > 0)
		{
			change = random.nextInt(spreadUpper);
		}
		else
		{
			change = -random.nextInt(this.spread);
		}
		if (spreadLower > 0)
		{
			change -= random.nextInt(spreadLower);
		}
		else
		{
			change += random.nextInt(this.spread);
		}
		if (Texasjake95Core.isTesting)
			System.out.println("Change is " + change);
		int newCoord = coord + change;
		if (Texasjake95Core.isTesting)
			System.out.println("New Coord is " + newCoord);
		return newCoord;
	}
	
	public boolean retroGenerate(Chunk chunk, Random random, int y)
	{
		int chunkxB = random.nextInt(16);
		int yUpper = 256 - y;
		int chunkzB = random.nextInt(16);
		int xSpreadUpper = this.getUpperSpread(chunkxB);
		int xSpreadLower = this.getLowerSpread(chunkxB);
		int ySpreadUpper = yUpper >= this.spread ? this.spread : yUpper;
		int ySpreadLower = y >= this.spread ? this.spread : y;
		int zSpreadUpper = this.getUpperSpread(chunkzB);
		int zSpreadLower = this.getLowerSpread(chunkzB);
		if (Texasjake95Core.isTesting)
		{
			System.out.println("X Center = " + chunkxB);
			System.out.println("Z Center = " + chunkzB);
			System.out.println("Before check: X Spread Upper = " + xSpreadUpper);
			System.out.println("Before check: X Spread Lower = " + xSpreadLower);
			System.out.println("Before check: Z Spread Upper = " + zSpreadUpper);
			System.out.println("Before check: Z Spread Lower = " + zSpreadLower);
		}
		if (xSpreadUpper + chunkxB > 15)
			xSpreadUpper = 15 - chunkxB;
		if (zSpreadUpper + chunkzB > 15)
			zSpreadUpper = 15 - chunkzB;
		if (xSpreadLower + chunkxB < 0)
			xSpreadUpper = chunkxB;
		if (zSpreadLower + chunkzB <0)
			xSpreadUpper = chunkzB;
		if (Texasjake95Core.isTesting)
		{
			System.out.println("After check: X Spread Upper = " + xSpreadUpper);
			System.out.println("After check: X Spread Lower = " + xSpreadLower);
			System.out.println("After check: Z Spread Upper = " + zSpreadUpper);
			System.out.println("After check: Z Spread Lower = " + zSpreadLower);
		}
		if (!(this.chance <= 0) && random.nextInt(this.chance) == 0)
		{
			for (int _ = 0; _ < this.numberOfBlocks; _++)
			{
				int newX = this.getAppropriateCoord(chunkxB, xSpreadUpper, xSpreadLower, random);
				int newY = this.getAppropriateCoord(y, ySpreadUpper, ySpreadLower, random);
				int newZ = this.getAppropriateCoord(chunkzB, zSpreadUpper, zSpreadLower, random);
				if (Texasjake95Core.isTesting)
					System.out.println(String.format("Attemping to place block at %s, %s, %s chunk located at %s, %s", newX, newY, newZ, chunk.xPosition, chunk.zPosition));
				if (this.checkForValidSpot(chunk, newX, newY, newZ))
				{
					chunk.setBlockIDWithMetadata(newX, newY, newZ, this.minableBlockId, this.minableMetaId);
					// world.setBlock(newX, newY, newZ, this.minableBlockId,
					// this.minableMetaId, 2);
				}
				else if (Texasjake95Core.isTesting)
					System.out.println(String.format("Failed due to invalid coords: %s, %s, %s", newX, newY, newZ));
			}
		}
		return true;
	}
	
	private boolean checkForValidSpot(World world, int x, int y, int z)
	{
		// if (!world.checkChunksExist(x, y, z, x, y, z))
		// return false;
		if (Texasjake95Core.isTesting || Texasjake95Core.AlwaysSet)
			return true;
		if (this.isBlockAir(world, x, y, z))
			return checkForBlock(world, x, y, z);
		return this.isBlock(world, x, y, z);
	}
	
	private boolean checkForValidSpot(Chunk chunk, int x, int y, int z)
	{
		// if (!world.checkChunksExist(x, y, z, x, y, z))
		// return false;
		if (x < 0 || z < 0 || x > 15 || z > 15)
			return false;
		if (Texasjake95Core.isTesting)
			return true;
		if (this.isBlockAir(chunk, x, y, z))
			return checkForBlock(chunk, x, y, z);
		return this.isBlock(chunk, x, y, z);
	}
	
	private boolean checkForBlock(World world, int x, int y, int z)
	{
		for (ForgeDirection d : ForgeDirection.VALID_DIRECTIONS)
			if (isBlock(world, x + d.offsetX, y + d.offsetY, z + d.offsetZ))
				return true;
			else if (isReplaceBlock(world, x + d.offsetX, y + d.offsetY, z + d.offsetZ))
				return true;
		return false;
	}
	
	private boolean checkForBlock(Chunk chunk, int x, int y, int z)
	{
		for (ForgeDirection d : ForgeDirection.VALID_DIRECTIONS)
			if (isBlock(chunk, x + d.offsetX, y + d.offsetY, z + d.offsetZ))
				return true;
			else if (isReplaceBlock(chunk, x + d.offsetX, y + d.offsetY, z + d.offsetZ))
				return true;
		return false;
	}
	
	private boolean isReplaceBlock(World world, int x, int y, int z)
	{
		int id = world.getBlockId(x, y, z);
		if (id == Block.bedrock.blockID)
			return false;
		int meta = world.getBlockMetadata(x, y, z);
		int oreID = OreDictionary.getOreID(new ItemStack(this.minableBlockId, 1, this.minableMetaId));
		boolean isOre = false;
		if (oreID > -1)
		{
			ArrayList<ItemStack> ores = OreDictionary.getOres(oreID);
			if (this.useOreDict)
				for (ItemStack stack : ores)
				{
					if (stack.itemID == id && (stack.getItemDamage() == meta || stack.getItemDamage() == OreDictionary.WILDCARD_VALUE))
					{
						isOre = true;
						break;
					}
				}
		}
		return (id == this.minableBlockId && (meta == this.minableMetaId || this.minableMetaId == OreDictionary.WILDCARD_VALUE) || isOre);
	}
	
	private boolean isReplaceBlock(Chunk chunk, int x, int y, int z)
	{
		int id = chunk.getBlockID(x, y, z);
		if (id == Block.bedrock.blockID)
			return false;
		int meta = chunk.getBlockMetadata(x, y, z);
		int oreID = OreDictionary.getOreID(new ItemStack(this.minableBlockId, 1, this.minableMetaId));
		boolean isOre = false;
		if (oreID > -1)
		{
			ArrayList<ItemStack> ores = OreDictionary.getOres(oreID);
			if (this.useOreDict)
				for (ItemStack stack : ores)
				{
					if (stack.itemID == id && (stack.getItemDamage() == meta || stack.getItemDamage() == OreDictionary.WILDCARD_VALUE))
					{
						isOre = true;
						break;
					}
				}
		}
		return (id == this.minableBlockId && (meta == this.minableMetaId || this.minableMetaId == OreDictionary.WILDCARD_VALUE) || isOre);
	}
	
	private boolean isBlock(Chunk chunk, int x, int y, int z)
	{
		int id = chunk.getBlockID(x, y, z);
		if (id == Block.bedrock.blockID)
			return false;
		int meta = chunk.getBlockMetadata(x, y, z);
		int oreID = OreDictionary.getOreID(new ItemStack(this.blockIDReplace, 1, this.blockMetaReplace));
		boolean isOre = false;
		if (oreID > -1)
		{
			ArrayList<ItemStack> ores = OreDictionary.getOres(oreID);
			if (this.useOreDict)
				for (ItemStack stack : ores)
				{
					if (stack.itemID == id && (stack.getItemDamage() == meta || stack.getItemDamage() == OreDictionary.WILDCARD_VALUE))
					{
						isOre = true;
						break;
					}
				}
		}
		return (id == this.blockIDReplace && (meta == this.blockMetaReplace || this.blockMetaReplace == OreDictionary.WILDCARD_VALUE) || isOre);
	}
	
	private boolean isBlock(World world, int x, int y, int z)
	{
		int id = world.getBlockId(x, y, z);
		if (id == Block.bedrock.blockID)
			return false;
		int meta = world.getBlockMetadata(x, y, z);
		int oreID = OreDictionary.getOreID(new ItemStack(this.blockIDReplace, 1, this.blockMetaReplace));
		boolean isOre = false;
		if (oreID > -1)
		{
			ArrayList<ItemStack> ores = OreDictionary.getOres(oreID);
			if (this.useOreDict)
				for (ItemStack stack : ores)
				{
					if (stack.itemID == id && (stack.getItemDamage() == meta || stack.getItemDamage() == OreDictionary.WILDCARD_VALUE))
					{
						isOre = true;
						break;
					}
				}
		}
		return (id == this.blockIDReplace && (meta == this.blockMetaReplace || this.blockMetaReplace == OreDictionary.WILDCARD_VALUE) || isOre);
	}
	
	private boolean isBlockAir(World world, int x, int y, int z)
	{
		return world.isAirBlock(x, y, z) || !world.blockExists(x, y, z);
	}
	
	public boolean isBlockAir(Chunk chunk, int x, int y, int z)
	{
		int id = chunk.getBlockID(x, y, z);
		return id == 0 || Block.blocksList[id] == null || Block.blocksList[id].isAirBlock(chunk.worldObj, x, y, z);
	}
}
