package com.texasjake95.base.helpers;

import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;

public class WorldGenHelper {
	
	public static class EndOreMeta extends TXWorldGenerator {
		
		public EndOreMeta(int blockId, int meta, int numberofBlocks, int chance, int spread)
		{
			super(blockId, meta, numberofBlocks, Block.whiteStone.blockID, chance, spread);
		}
		// private int minableBlockId;
		// private int minableMetaId;
		// private int numberOfBlocks;
		//
		// private EndOreMeta(int BlockId, int Meta, int NumberofBlocks)
		// {
		// this.minableBlockId = BlockId;
		// this.minableMetaId = Meta;
		// this.numberOfBlocks = NumberofBlocks;
		// }
		//
		// @Override
		// public boolean generate(World par1World, Random par2Random, int par3,
		// int par4, int par5)
		// {
		// float f = par2Random.nextFloat() * (float) Math.PI;
		// double d = par3 + 8 + MathHelper.sin(f) * this.numberOfBlocks / 8F;
		// double d1 = par3 + 8 - MathHelper.sin(f) * this.numberOfBlocks / 8F;
		// double d2 = par5 + 8 + MathHelper.cos(f) * this.numberOfBlocks / 8F;
		// double d3 = par5 + 8 - MathHelper.cos(f) * this.numberOfBlocks / 8F;
		// double d4 = par4 + par2Random.nextInt(3) - 2;
		// double d5 = par4 + par2Random.nextInt(3) - 2;
		// for (int i = 0; i <= this.numberOfBlocks; i++)
		// {
		// double d6 = d + (d1 - d) * i / this.numberOfBlocks;
		// double d7 = d4 + (d5 - d4) * i / this.numberOfBlocks;
		// double d8 = d2 + (d3 - d2) * i / this.numberOfBlocks;
		// double d9 = par2Random.nextDouble() * this.numberOfBlocks / 16D;
		// double d10 = (MathHelper.sin(i * (float) Math.PI /
		// this.numberOfBlocks) + 1.0F) * d9 + 1.0D;
		// double d11 = (MathHelper.sin(i * (float) Math.PI /
		// this.numberOfBlocks) + 1.0F) * d9 + 1.0D;
		// int j = MathHelper.floor_double(d6 - d10 / 2D);
		// int k = MathHelper.floor_double(d7 - d11 / 2D);
		// int l = MathHelper.floor_double(d8 - d10 / 2D);
		// int i1 = MathHelper.floor_double(d6 + d10 / 2D);
		// int j1 = MathHelper.floor_double(d7 + d11 / 2D);
		// int k1 = MathHelper.floor_double(d8 + d10 / 2D);
		// for (int l1 = j; l1 <= i1; l1++)
		// {
		// double d12 = (l1 + 0.5D - d6) / (d10 / 2D);
		// if (d12 * d12 >= 1.0D)
		// {
		// continue;
		// }
		// for (int i2 = k; i2 <= j1; i2++)
		// {
		// double d13 = (i2 + 0.5D - d7) / (d11 / 2D);
		// if (d12 * d12 + d13 * d13 >= 1.0D)
		// {
		// continue;
		// }
		// for (int j2 = l; j2 <= k1; j2++)
		// {
		// double d14 = (j2 + 0.5D - d8) / (d10 / 2D);
		// if (d12 * d12 + d13 * d13 + d14 * d14 < 1.0D &&
		// par1World.getBlockId(l1, i2, j2) == Block.whiteStone.blockID)
		// {
		// par1World.setBlock(l1, i2, j2, this.minableBlockId,
		// this.minableMetaId, 2);
		// }
		// }
		// }
		// }
		// }
		// return true;
		// }
	}
	
	public static class NetherOreMeta extends TXWorldGenerator {
		
		public NetherOreMeta(int blockId, int meta, int numberofBlocks, int chance, int spread)
		{
			super(blockId, meta, numberofBlocks, Block.netherrack.blockID, chance, spread);
		}
		// private int minableBlockId;
		// private int minableMetaId;
		// private int numberOfBlocks;
		//
		// private NetherOreMeta(int BlockId, int Meta, int NumberofBlocks)
		// {
		// this.minableBlockId = BlockId;
		// this.minableMetaId = Meta;
		// this.numberOfBlocks = NumberofBlocks;
		// }
		//
		// @Override
		// public boolean generate(World par1World, Random par2Random, int par3,
		// int par4, int par5)
		// {
		// float f = par2Random.nextFloat() * (float) Math.PI;
		// double d = par3 + 8 + MathHelper.sin(f) * this.numberOfBlocks / 8F;
		// double d1 = par3 + 8 - MathHelper.sin(f) * this.numberOfBlocks / 8F;
		// double d2 = par5 + 8 + MathHelper.cos(f) * this.numberOfBlocks / 8F;
		// double d3 = par5 + 8 - MathHelper.cos(f) * this.numberOfBlocks / 8F;
		// double d4 = par4 + par2Random.nextInt(3) - 2;
		// double d5 = par4 + par2Random.nextInt(3) - 2;
		// for (int i = 0; i <= this.numberOfBlocks; i++)
		// {
		// double d6 = d + (d1 - d) * i / this.numberOfBlocks;
		// double d7 = d4 + (d5 - d4) * i / this.numberOfBlocks;
		// double d8 = d2 + (d3 - d2) * i / this.numberOfBlocks;
		// double d9 = par2Random.nextDouble() * this.numberOfBlocks / 16D;
		// double d10 = (MathHelper.sin(i * (float) Math.PI /
		// this.numberOfBlocks) + 1.0F) * d9 + 1.0D;
		// double d11 = (MathHelper.sin(i * (float) Math.PI /
		// this.numberOfBlocks) + 1.0F) * d9 + 1.0D;
		// int j = MathHelper.floor_double(d6 - d10 / 2D);
		// int k = MathHelper.floor_double(d7 - d11 / 2D);
		// int l = MathHelper.floor_double(d8 - d10 / 2D);
		// int i1 = MathHelper.floor_double(d6 + d10 / 2D);
		// int j1 = MathHelper.floor_double(d7 + d11 / 2D);
		// int k1 = MathHelper.floor_double(d8 + d10 / 2D);
		// for (int l1 = j; l1 <= i1; l1++)
		// {
		// double d12 = (l1 + 0.5D - d6) / (d10 / 2D);
		// if (d12 * d12 >= 1.0D)
		// {
		// continue;
		// }
		// for (int i2 = k; i2 <= j1; i2++)
		// {
		// double d13 = (i2 + 0.5D - d7) / (d11 / 2D);
		// if (d12 * d12 + d13 * d13 >= 1.0D)
		// {
		// continue;
		// }
		// for (int j2 = l; j2 <= k1; j2++)
		// {
		// double d14 = (j2 + 0.5D - d8) / (d10 / 2D);
		// if (d12 * d12 + d13 * d13 + d14 * d14 < 1.0D &&
		// par1World.getBlockId(l1, i2, j2) == Block.netherrack.blockID)
		// {
		// par1World.setBlock(l1, i2, j2, this.minableBlockId,
		// this.minableMetaId, 2);
		// }
		// }
		// }
		// }
		// }
		// return true;
		// }
	}
	
	public static class OreMeta extends TXWorldGenerator {
		
		public OreMeta(int blockId, int meta, int numberofBlocks, int chance, int spread)
		{
			super(blockId, meta, numberofBlocks, Block.stone.blockID, chance, spread);
		}
		// private int minableBlockId;
		// private int minableMetaId;
		// private int numberOfBlocks;
		//
		// public OreMeta(int blockId, int meta, int numberofBlocks)
		// {
		// minableBlockId = blockId;
		// minableMetaId = meta;
		// numberOfBlocks= numberofBlocks;
		// }
		//
		// @Override
		// public boolean generate(World par1World, Random par2Random, int par3,
		// int par4, int par5)
		// {
		// float f = par2Random.nextFloat() * (float) Math.PI;
		// double d = par3 + 8 + MathHelper.sin(f) * this.numberOfBlocks / 8F;
		// double d1 = par3 + 8 - MathHelper.sin(f) * this.numberOfBlocks / 8F;
		// double d2 = par5 + 8 + MathHelper.cos(f) * this.numberOfBlocks / 8F;
		// double d3 = par5 + 8 - MathHelper.cos(f) * this.numberOfBlocks / 8F;
		// double d4 = par4 + par2Random.nextInt(3) - 2;
		// double d5 = par4 + par2Random.nextInt(3) - 2;
		// for (int i = 0; i <= this.numberOfBlocks; i++)
		// {
		// double d6 = d + (d1 - d) * i / this.numberOfBlocks;
		// double d7 = d4 + (d5 - d4) * i / this.numberOfBlocks;
		// double d8 = d2 + (d3 - d2) * i / this.numberOfBlocks;
		// double d9 = par2Random.nextDouble() * this.numberOfBlocks / 16D;
		// double d10 = (MathHelper.sin(i * (float) Math.PI /
		// this.numberOfBlocks) + 1.0F) * d9 + 1.0D;
		// double d11 = (MathHelper.sin(i * (float) Math.PI /
		// this.numberOfBlocks) + 1.0F) * d9 + 1.0D;
		// int j = MathHelper.floor_double(d6 - d10 / 2D);
		// int k = MathHelper.floor_double(d7 - d11 / 2D);
		// int l = MathHelper.floor_double(d8 - d10 / 2D);
		// int i1 = MathHelper.floor_double(d6 + d10 / 2D);
		// int j1 = MathHelper.floor_double(d7 + d11 / 2D);
		// int k1 = MathHelper.floor_double(d8 + d10 / 2D);
		// for (int l1 = j; l1 <= i1; l1++)
		// {
		// double d12 = (l1 + 0.5D - d6) / (d10 / 2D);
		// if (d12 * d12 >= 1.0D)
		// {
		// continue;
		// }
		// for (int i2 = k; i2 <= j1; i2++)
		// {
		// double d13 = (i2 + 0.5D - d7) / (d11 / 2D);
		// if (d12 * d12 + d13 * d13 >= 1.0D)
		// {
		// continue;
		// }
		// for (int j2 = l; j2 <= k1; j2++)
		// {
		// double d14 = (j2 + 0.5D - d8) / (d10 / 2D);
		// if (d12 * d12 + d13 * d13 + d14 * d14 < 1.0D &&
		// par1World.getBlockId(l1, i2, j2) == Block.stone.blockID)
		// {
		// System.out.println(String.format("Placing block at %s, %s, %s", l1,
		// i2, j2));
		// par1World.setBlock(l1, i2, j2, this.minableBlockId,
		// this.minableMetaId, 2);
		// }
		// }
		// }
		// }
		// }
		// return true;
		// }
	}
	
	public static TXWorldGenerator End(int BlockId, int NumberofBlocks, int chance, int spread)
	{
		return End(BlockId, 0, NumberofBlocks, chance, spread);
	}
	
	public static TXWorldGenerator End(int BlockId, int Meta, int NumberofBlocks, int chance, int spread)
	{
		return new EndOreMeta(BlockId, Meta, NumberofBlocks, chance, spread);
	}
	
	public static TXWorldGenerator End(ItemStack stack, int NumberofBlocks, int chance, int spread)
	{
		return End(stack.itemID, NumberofBlocks, stack.getItemDamage(), chance, spread);
	}
	
	public static TXWorldGenerator Nether(int BlockId, int NumberofBlocks, int chance, int spread)
	{
		return Nether(BlockId, 0, NumberofBlocks, chance, spread);
	}
	
	public static TXWorldGenerator Nether(int BlockId, int Meta, int NumberofBlocks, int chance, int spread)
	{
		return new NetherOreMeta(BlockId, Meta, NumberofBlocks, chance, spread);
	}
	
	public static TXWorldGenerator Nether(ItemStack stack, int NumberofBlocks, int chance, int spread)
	{
		return Nether(stack.itemID, NumberofBlocks, stack.getItemDamage(), chance, spread);
	}
	
	public static TXWorldGenerator Surface(int BlockId, int NumberofBlocks, int chance, int spread)
	{
		return Surface(BlockId, 0, NumberofBlocks, chance, spread);
	}
	
	public static TXWorldGenerator Surface(int BlockId, int Meta, int NumberofBlocks, int chance, int spread)
	{
		return new OreMeta(BlockId, Meta, NumberofBlocks, chance, spread);
	}
	
	public static TXWorldGenerator Surface(ItemStack stack, int NumberofBlocks, int chance, int spread)
	{
		return Surface(stack.itemID, NumberofBlocks, stack.getItemDamage(), chance, spread);
	}
	
	public static TXWorldGenerator CustomGen(int blockID, int meta, int numberOfBlocks, int blockToReplace, int metaToReplace, int chance, int spread)
	{
		return new TXWorldGenerator(blockID, meta, numberOfBlocks, blockToReplace, metaToReplace, chance, spread);
	}
	
	public static TXWorldGenerator getGenFromType(WorldGenType type, int blockID, int meta, int numberOfBlocks, int chance, int spread)
	{
		switch (type)
		{
			case SURFACE:
				return Surface(blockID, meta, numberOfBlocks, chance, spread);
			case NETHER:
				return Nether(blockID, meta, numberOfBlocks, chance, spread);
			case END:
				return End(blockID, meta, numberOfBlocks, chance, spread);
			default:
				return Surface(blockID, meta, numberOfBlocks, chance, spread);
		}
	}
	
	public static enum WorldGenType
	{
		SURFACE, NETHER, END;
	}
}
