package com.texasjake95.modularTool.item;

import com.texasjake95.base.helpers.NBTHelper;
import com.texasjake95.modularTool.modifiers.ModToolStats;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

public class ItemModularTool extends Item {
	
	static final String NBT_TOOL_TAG_BASE = "TX_MOD_TOOL";
	static final String NBT_TOOL_TAG_EFFBLOCK = "EFFBLOCK";
	static final String NBT_TOOL_TAG_BROKEN = "broken";
	static final String[] TOOL_TYPES = { "pickaxe", "axe", "shovel" };
	
	public ItemModularTool(int par1)
	{
		super(par1);
		this.maxStackSize = 1;
	}
	
	@Override
	public void onCreated(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
	{
		ModToolStats.initStack(par1ItemStack);
	}
	
	@Override
	public float getStrVsBlock(ItemStack par1ItemStack, Block par2Block, int meta)
	{
		ModToolStats stats = new ModToolStats(par1ItemStack);
		float result = stats.getStrVsBlock(par2Block, meta);
		return result;
	}
	
	@Override
	public boolean hitEntity(ItemStack par1ItemStack, EntityLivingBase par2EntityLiving, EntityLivingBase par3EntityLiving)
	{
		// par1ItemStack.damageItem(2, par3EntityLiving);
		// TODO Add damage methods
		return true;
	}
	
	@Override
	public boolean onBlockDestroyed(ItemStack par1ItemStack, World par2World, int par3, int par4, int par5, int par6, EntityLivingBase par7EntityLiving)
	{
		return true;
	}
	
	@Override
	public float getDamageVsEntity(Entity par1Entity, ItemStack stack)
	{
		NBTTagCompound tags = NBTHelper.getNBTTags(stack, NBT_TOOL_TAG_BASE);
		if (NBTHelper.getBoolean(tags, NBT_TOOL_TAG_BROKEN))
		{
			return 1F;
		}
		return NBTHelper.getFloat(tags, "sword.damage");
	}
	
	@SideOnly(Side.CLIENT)
	@Override
	public boolean isFull3D()
	{
		return true;
	}
	
	@Override
	public boolean getIsRepairable(ItemStack par1ItemStack, ItemStack par2ItemStack)
	{
		return false;
	}
	
	@Override
	public CreativeTabs[] getCreativeTabs()
	{
		return new CreativeTabs[] { CreativeTabs.tabTools };
	}
}
