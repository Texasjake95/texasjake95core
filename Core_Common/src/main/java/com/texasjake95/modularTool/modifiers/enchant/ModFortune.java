package com.texasjake95.modularTool.modifiers.enchant;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.item.ItemStack;

public class ModFortune extends ModEnchant {
	
	public ModFortune()
	{
		super("fortune", Enchantment.fortune.getMaxLevel(), EnumEnchantType.Tool);
	}
	
	@Override
	public boolean canHaveEnchant(ItemStack stack)
	{
		return !new ModSilkTouch().hasEnchant(stack) && super.canHaveEnchant(stack);
	}
}
