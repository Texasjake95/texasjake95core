package com.texasjake95.modularTool;

import com.texasjake95.base.IMod;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;

public class ModularTool implements IMod {
	
	@Override
	@EventHandler
	public void preload(FMLPreInitializationEvent event)
	{
		// TODO Auto-generated method stub
	}
	
	@Override
	@EventHandler
	public void load(FMLInitializationEvent event)
	{
		// TODO Auto-generated method stub
	}
	
	@Override
	@EventHandler
	public void modsLoaded(FMLPostInitializationEvent event)
	{
		// TODO Auto-generated method stub
	}
	
	@Override
	public String modID()
	{
		return this.getClass().getAnnotation(Mod.class).modid();
	}
}
